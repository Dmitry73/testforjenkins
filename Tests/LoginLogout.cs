﻿using NUnit.Framework;
using gehtsoft.tscontroller;
using gehtsoft.applicationcontroller;
using TSAutotests.Login;
using System.Configuration;
using System.Threading;
using System.Runtime.InteropServices;

namespace TSAutotests
{
    [Category("TestForConstantin")]
    [TestFixture]
    internal class TradingSettingTestSuits
    {
        private TradingStationWindow application;
        private const string QA_REAL_CONNECTION_TYPE = "QAReal";
        private const string NAME_CHOOSE_TRADING_SESSION_100KREAL = "100KREAL";
        private const string TITLE_MANAGE_SYMBOL_SUBSCRIPTION_DIALOG =
            "Manage Symbol Subscription";
        private const string CLOSE_BUTTON = "Close";
        private const string DIALOG_LOGIN_TITLE = "Login";
        private const string TS_PROCESS_NAME = "FXTSpp";
        private const string TITLE_DIALOG_WHATSNEW = "What's New";
        private const string OK_BUTTON = "OK";
        private const string ORDER_TYPE_AT_MARKET = "At Market";
        private const string TIME_IN_FORCE_GTC = "GTC";
        private const string TEST_CURRANCY_PAIR = "EUR/USD";
        private const bool OFF = false;
        private const string ENTRY_ORDER_TYPE = "Entry";


        [Test]
        public void _0_Login()
        {
            ProcessKill(TS_PROCESS_NAME);
            application = TradingStation.StartApplication();

            using (var loginDialog = new LoginDialog(application))
            {
                string login = ConfigurationManager.AppSettings["login"];
                string password = ConfigurationManager.AppSettings["password"];
                string connection = ConfigurationManager.AppSettings["connection"];

                bool testResult = loginDialog.Login(login, password, connection,
                    application);
                Assert.True(testResult, "Error in entering TS");

                if (testResult)
                {
                    if (connection.Equals(QA_REAL_CONNECTION_TYPE))
                    {
                        using (var chooseSession = new ChooseTradingSession(
                            application))
                        {
                            chooseSession.SelectNameDialog(
                                NAME_CHOOSE_TRADING_SESSION_100KREAL);
                        }
                    }
                    WaitLoading();
                }
            }

            using (var orderTab = new OrdersTab(application))
            {
                orderTab.DeleteAllOrders(application);
                Assert.AreEqual(orderTab.GetAllOrdersCount(), 0); // проверяет что в табе Order нет ордеров
            }
        }

        //[Test]
        //public void _1_OpenPosition()
        //{
        //    using (var simpleDealingRates = new SimpleDealingRatesTab(application))
        //    {
        //        simpleDealingRates.PressCurrancyPair(TEST_CURRANCY_PAIR);
        //        var menuHelper = new ApplicationMenuHelper();
        //        menuHelper.PressTradingDealingRatesMarketOrder(application);
        //        using (var createMarketOrder = new CreateMarketOrderDialog(application))
        //        {
        //            Assert.True(createMarketOrder.CreateMarketOrderMenu(application,
        //                TEST_CURRANCY_PAIR, BuySellEnum.SELL, 200, ORDER_TYPE_AT_MARKET));
        //        }
        //    }
        //}

        //[Test]
        //public void _2_ClosePosition()
        //{
        //    using (var openPositionTab = new OpenPositionsTab(application))
        //    {
        //        Assert.True(openPositionTab.ClosePositionMenu(application,
        //            TEST_CURRANCY_PAIR, ORDER_TYPE_AT_MARKET,
        //            TIME_IN_FORCE_GTC));
        //    }
        //}

        //[Test]
        //public void _3_CreateContingentOrder()
        //{
        //    int plusDay = 2;

        //    var aplicationMenu = new ApplicationMenuHelper();
        //    aplicationMenu.PressOpenCreateContingentOrder(application);
        //    using (var contingentOrder = new ContingentOrderTestDialog(application))
        //    {
        //        Assert.True(contingentOrder.CreateGTD(application, plusDay,
        //            TEST_CURRANCY_PAIR, TEST_CURRANCY_PAIR, OrderGroups.OTO));
        //    }
        //}

        //[Test]
        //public void _4_DeleteContingentOrder()
        //{
        //    using (var orderTab = new OrdersTab(application))
        //    {
        //        orderTab.DeleteAllOrders(application);
        //        Assert.AreEqual(orderTab.GetAllOrdersCount(), 0); // проверяет что в табе Order нет ордеров
        //    }
        //}

        [Test]
        public void _5_Logout()
        {
            var menu = new ApplicationMenuHelper();
            menu.PressSystemLogout(application);
            Assert.True(CheckLogout());
            ProcessKill(TS_PROCESS_NAME);
            if (application != null)
            {
                application.Dispose();
            }
        }



        private bool CheckLogout()
        {
            Waiter.WaitingCondition conditionLogout = () =>
                (application.GetDialog(DIALOG_LOGIN_TITLE)) != null;
            if (Waiter.Wait(conditionLogout))
            {
                TestContext.Out.WriteLine("\r\nLogout successfully completed");
                return true;
            }
            else
            {
                TestContext.Out.WriteLine("\r\nLogout not completed");
                return false;
            }
        }

        private void ProcessKill(string processName)
        {
            System.Diagnostics.Process[] findProcessTS =
                System.Diagnostics.Process.GetProcessesByName(processName);
            foreach (System.Diagnostics.Process processTS in findProcessTS)
            {
                processTS.Kill();
            }
        }

        private void WhatsNewDialog()
        {
            Dialog whatsNewDialog = null;
            Waiter.WaitingCondition conditionWhatsNew = () =>
                (whatsNewDialog = application.GetDialog(
                    TITLE_DIALOG_WHATSNEW)) != null;
            if (Waiter.Wait(conditionWhatsNew, 2))
            {
                whatsNewDialog[OK_BUTTON].Click();
                Waiter.WaitingCondition conditionDisappear = () =>
               (application.GetDialog(TITLE_DIALOG_WHATSNEW)) == null;
                if (Waiter.Wait(conditionDisappear)) { }
                if (whatsNewDialog != null)
                {
                    whatsNewDialog.Dispose();
                }
            }
        }

        private void WaitLoading()
        {
            WhatsNewDialog();
            application.SendKeyDown(InputKeyCode.VK_S);
            application.SendKeyUp(InputKeyCode.VK_S);
            Dialog dialogWaitingLoading = null;
            Waiter.WaitingCondition dialogExistsCondition = () =>
            (dialogWaitingLoading = application.GetDialog(
                TITLE_MANAGE_SYMBOL_SUBSCRIPTION_DIALOG)) != null;
            using (dialogWaitingLoading)
            {
                if (Waiter.Wait(dialogExistsCondition, 2))
                {
                    dialogWaitingLoading[CLOSE_BUTTON].Click();
                    Waiter.WaitingCondition dialogDisepearCondition = () =>
                    (application.GetDialog(
                        TITLE_MANAGE_SYMBOL_SUBSCRIPTION_DIALOG)) == null;
                    if (Waiter.Wait(dialogExistsCondition, 2))
                    {
                        dialogWaitingLoading.Dispose();
                    }
                    TestContext.Out.WriteLine("");
                }
                else
                {
                    WaitLoading();
                }
            }
        }
    }
}










﻿using System.Collections.Generic;

namespace TSAutotests
{
    public enum OrderGroups
    {
        OTO,
        OCO,
        OTOCO,
        Entry,
        If_Then,
        If_Then_OCO
    }

    public static class OrderGroupDictionary
    {
        public static Dictionary<OrderGroups, string> orderGroupNames = new Dictionary<OrderGroups, string>()
        {
            { OrderGroups.OTO, "OTO" },
            { OrderGroups.OCO, "OCO" },
            { OrderGroups.OTOCO, "OTOCO" },
            { OrderGroups.Entry, "Entry Orders" },
            { OrderGroups.If_Then, "If-Then" },
            { OrderGroups.If_Then_OCO, "If-Then OCO" }
        };
    }
}
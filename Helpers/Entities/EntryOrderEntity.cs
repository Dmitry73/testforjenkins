﻿using System;

namespace TSAutotests
{
    public class EntryOrderEntity
    {
        private string orderId;
        public string OrderId
        {
            get
            {
                return orderId;
            }
        }

        private string currencyPair;
        public string CurrencyPair
        {
            get
            {
                return currencyPair;
            }
        }

        private BuySellEnum buySell;
        public BuySellEnum BuySell
        {
            get
            {
                return buySell;
            }
        }

        private double exhibitionRate;
        public double ExhibitionRate
        {
            get
            {
                return exhibitionRate;
            }
        }

        private DateTime timeOfCreation;
        public DateTime TimeOfCreation
        {
            get
            {
                return timeOfCreation;
            }
        }

        private double priceBuyAtOrderCreation;
        public double PriceBuyAtOrderCreation
        {
            get
            {
                return priceBuyAtOrderCreation;
            }
        }

        private double priceSellAtOrderCreation;
        public double PriceSellAtOrderCreation
        {
            get
            {
                return priceSellAtOrderCreation;
            }
        }


        public EntryOrderEntity(string orderId, string currencyPair, BuySellEnum buySell,
            double exhibitionRate,DateTime timeOfCreation, double priceBuyAtOrderCreation,
            double priceSellAtOrderCreation)
        {
            this.orderId = orderId;
            this.currencyPair = currencyPair;
            this.buySell = buySell;
            this.exhibitionRate = exhibitionRate;
            this.timeOfCreation = timeOfCreation;
            this.priceBuyAtOrderCreation = priceBuyAtOrderCreation;
            this.priceSellAtOrderCreation = priceSellAtOrderCreation;
        }
    }
}

﻿namespace TSAutotests
{
    public class OtoOrderEntity
    {
        private EntryOrderEntity primaryOrder;
        public EntryOrderEntity PrimaryOrder
        {
            get
            {
                return primaryOrder;
            }
        }

        private EntryOrderEntity secondaryOrder;
        public EntryOrderEntity SecondaryOrder
        {
            get
            {
                return secondaryOrder;
            }
        }

        public OtoOrderEntity(EntryOrderEntity primary, EntryOrderEntity secondary)
        {
            primaryOrder = primary;
            secondaryOrder = secondary;
        }
    }
}

﻿namespace TSAutotests
{
    class OpenPositionEntity
    {
        private string ticket;
        public string Ticket
        {
            get
            {
                return ticket;
            }
        }

        private string tradeType;
        public string TradeType
        {
            get
            {
                return tradeType;
            }
        }

        private string priceOpen;
        public string PriceOpen
        {
            get
            {
                return priceOpen;
            }
        }

        private string amount;
        public string Amount
        {
            get
            {
                return amount;
            }
        }

        private int rowItem;
        public int RowItem
        {
            get
            {
                return rowItem;
            }
        }

        public OpenPositionEntity(string ticket, string tradeType, string priceOpen, string amount, int rowItem)
        {
            this.ticket = ticket;
            this.tradeType = tradeType;
            this.priceOpen = priceOpen;
            this.amount = amount;
            this.rowItem = rowItem;
        }
    }
}

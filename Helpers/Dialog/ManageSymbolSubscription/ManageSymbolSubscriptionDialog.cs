﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Collections.Generic;
using System.Threading;
using TSAutotests.MSS;

namespace TSAutotests
{
    class ManageSymbolSubscriptionDialog : IDisposable
    {
        private const string MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW =
            "Manage Symbol Subscription";
        private const string REMOVE_SYMBOL_BUTTON = "<";
        private const string ADD_SYMBOL_BUTTON = ">";
        private const string CLOSE_BUTTON = "Close";
        private const int SEARCH_EDIT_SYMBOL_LIST_ID = 45730;
        private const int SEARCH_EDIT_SUBSCRIBED_SYMBOLS_ID = 45731;
        private const string SAVE_WATCHLIST_BUTTON = "Save watchlist...";
        private const string OPEN_WATCHLIST_BUTTON = "Open watchlist...";


        private Dialog dialogMSS;
        private ListView manageSymbolView;

        public ManageSymbolSubscriptionDialog(TradingStationWindow application)
        {
            dialogMSS = Waiter.WaitDialogAppear(application, 
                MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW);
            if (dialogMSS == null)
            {
                throw new Exception("Can't find window Manage Symbol Subscription");
            }
            manageSymbolView = (dialogMSS.FindChild("SysListView32", ".*",
                true)) as ListView;
        }

        public bool RemoveNotTradingSymbol(TradingStationWindow application,
            string symbol)
        {
            using (var openPosition = new OpenPositionsTab(application))
            {
                if (openPosition.ExistanceOpenPositionOnCurrancyPair(symbol))
                {
                    throw new Exception(
                        "There is an open position on this currency pair");
                }
            }
            List<string> listOneSymbolRemove = new List<string> { symbol };
            RemoveSymbolView(listOneSymbolRemove, true);
            PressClose();
            bool result = false;
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                if (simpleDealingRates.FindOnSymbol(symbol) == -1)
                {
                    result = true;
                }
            }
            return result;
        }

        public bool OpenWatchListAndCheckCurrancyPair(TradingStationWindow application,
            List<string> contentWathlist, string nameWatchlist)
        {
            dialogMSS[OPEN_WATCHLIST_BUTTON].Click();
            using (var openWatchlist = new OpenWatchlistDialog(application))
            {
                openWatchlist.SelectWatchlist(nameWatchlist);
            }
            bool result = CheckContenetCurrancyPairInView(contentWathlist);
            PressClose();
            return result;
        }

        public bool More20SymbolAdd(TradingStationWindow application,
            List<string> currancyPairs20Symbol, List<string> symbolNotEnter20Symbol)
        {
            InstallSymbolOpenDialog(application, currancyPairs20Symbol);

            using (var treeManageSymbol = new TreeViewHelper(dialogMSS, ".*"))
            {
                TreeViewItem findItem = treeManageSymbol.FindElement(symbolNotEnter20Symbol[0]);
                while (findItem.Selected != true)
                {
                    findItem.Click(InputEventModifiers.LeftButton);
                }
            }
            Waiter.WaitingCondition condition = () => dialogMSS[ADD_SYMBOL_BUTTON].Enabled == true;
            if (Waiter.Wait(condition, 3))
            {
                dialogMSS[ADD_SYMBOL_BUTTON].Click();
            }
            bool result = false;
            using (var errorDialog = new MSSDialogError(application))
            {
                result = errorDialog.CheckEmergenceWindowMore20Symbol();
            }
            PressClose();
            return result;
        }

        public bool RemoveTradingCurrancyPair (TradingStationWindow application, string tradingCurrancyPair)
        {
            for (int i = 0; i < manageSymbolView.RowCount; i++)
            {
                string valueRow = manageSymbolView.CellValue(i, 0);
                if (tradingCurrancyPair.Equals(valueRow))
                {
                    manageSymbolView.ClickCell(i, 0, InputEventModifiers.LeftButton);
                }
            }

            Waiter.WaitingCondition condition = () => dialogMSS[REMOVE_SYMBOL_BUTTON].Enabled == true;
            if (Waiter.Wait(condition, 3))
            {
                dialogMSS[REMOVE_SYMBOL_BUTTON].Click();
            }

            bool result = false;
            using (var mssDialog = new MSSDialogError(application))
            {
                result = mssDialog.CheckEmergenceWindowSymbolOpenPosition(tradingCurrancyPair);
            }
            PressClose();
            return result;
        }

        private void InstallSymbolOpenDialog(TradingStationWindow application,
            List<string> currancyPairs)
        {
            LeaveOnlyThoseCurrenctyPairs(currancyPairs);
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressTradingDealingRatesManageSymbol(application);
            dialogMSS = Waiter.WaitDialogAppear(application, MANAGE_SYMBOL_SUBSCRIPTION_NAME_WINDOW);
            manageSymbolView = (dialogMSS.FindChild("SysListView32", ".*", true)) as ListView;
        }

        public bool CheckSearchSymbolList(string searchMoney)
        {
            bool result = false;
            InstallValueSearchSymbolListEdit(searchMoney);

            using (var treeManageSymbol = new TreeViewHelper(dialogMSS, ".*"))
            {
                result = treeManageSymbol.CompareElementTree(searchMoney);
            }
            PressClose();
            return result;
        }

        public bool NegativeSearchInSymbolList(string negativeValueForSearch)
        {
            bool result = false;
            InstallValueSearchSymbolListEdit(negativeValueForSearch);
            using (var treeManageSymbol = new TreeViewHelper(dialogMSS, ".*"))
            {
                result = treeManageSymbol.NegativSearchSymbolList();
            }
            PressClose();
            return result;
        }

        public bool CheckOpenWindow()
        {
            if (dialogMSS != null)
            {
                PressClose();
                return true;
            }
            throw new Exception("Can't find dialog window Manage Symbol Subscription");
        }

        public bool CheckAdd(List<string> currancyPairs)
        {
            bool result = false;
            AddSymbol(currancyPairs);
            foreach (string currancyPair in currancyPairs)
            {
                Waiter.WaitingCondition condition = () => result =
                    CheckSymbolInView(currancyPair) == true;
                if (!Waiter.Wait(condition, 5))
                {
                    throw new Exception("Did not add selected characters");
                }
            }
            PressClose();
            return result;
        }

        public bool CheckSearchSubscribedSymbols(string valueSearch)
        {
            InstallValueSearchSubscribedSymbolsEdit(valueSearch);
            bool result = false;
            Waiter.WaitingCondition condition = () => result =
            CheckSerachSubscribedSymolInView(valueSearch) == true;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Error in search results SubscribedSymbol");
            }
            PressClose();
            return result;
        }

        private bool CheckContenetCurrancyPairInView(List<string> currancyPairs)
        {
            foreach (string currancyPair in currancyPairs)
            {
                if (CheckSymbolInView(currancyPair) == false)
                {
                    return false;
                }
            }
            return true;
        }

        private bool CheckSerachSubscribedSymolInView(string valueSearch)
        {
            int countRowManageSymbolView = manageSymbolView.RowCount;
            for (int i = 0; i < countRowManageSymbolView; i++)
            {
                string valueRow = manageSymbolView.CellValue(i, 0);
                if (!valueRow.Contains(valueSearch))
                {
                    return false;
                }
            }

            return true;
        }

        private bool CheckSymbolInView(string valueSearch)
        {
            int countRowManageSymbolView = manageSymbolView.RowCount;
            for (int i = 0; i < countRowManageSymbolView; i++)
            {
                string valueRow = manageSymbolView.CellValue(i, 0);
                if (valueRow.Equals(valueSearch))
                {
                    return true;
                }
            }
            return false;
        }

        public bool RemoveOneSymbolAddOneSymbol20Symbols(TradingStationWindow application, List<string> symbols20,
            List<string> removeOneSymbol, List<string> addOneSimbol)
        {
            InstallSymbolOpenDialog(application, symbols20);
            RemoveSymbolView(removeOneSymbol, true);
            AddSymbol(addOneSimbol);
            bool result = false;
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                if ((simpleDealingRates.FindOnSymbol(addOneSimbol[0]) != -1) &&
                    (simpleDealingRates.ReturnRowCount()) == 20)
                {
                    result = true;
                }
            }

            PressClose();
            return result;
        }

        public void LeaveOnlyThoseCurrenctyPairs(List<string> currancyPairs)
        {
            RemoveSymbolView(currancyPairs, false);
            if (CheckContenetCurrancyPairInView(currancyPairs) == false)
            {
                AddSymbol(currancyPairs);
            }
            PressClose();
        }

        public bool AddSelectActiveSymbol(string activeSymbol)
        {
            bool result = false;
            using (var treeManageSymbol = new TreeViewHelper(dialogMSS, ".*"))
            {
                SelectItemTree(treeManageSymbol, activeSymbol);
            }

            Waiter.WaitingCondition condition = () => dialogMSS[ADD_SYMBOL_BUTTON].Enabled == true;
            if (!Waiter.Wait(condition, 1))
            {
                result = true;
            }

            PressClose();
            return result;
        }

        private void SelectItemTree (TreeViewHelper treeView, string findElement)
        {
            TreeViewItem findItem = treeView.FindElement(findElement);
            while (findItem.Selected != true)
            {
                Thread.Sleep(100);
                findItem.Click(InputEventModifiers.LeftButton);
                Thread.Sleep(100);
            }
        }

        private void RemoveSymbolView(List<string> currancyPairs, bool removeOrLeave)
        {
            for (int i = 0; i < manageSymbolView.RowCount; i++)
            {
                string valueRow = manageSymbolView.CellValue(i, 0);
                if (currancyPairs.Contains(valueRow) == removeOrLeave)
                {
                    manageSymbolView.ClickCell(i, 0, InputEventModifiers.LeftButton);
                }
            }

            Waiter.WaitingCondition condition = () => dialogMSS[REMOVE_SYMBOL_BUTTON].Enabled == true;
            if (Waiter.Wait(condition, 1))
            {
                dialogMSS[REMOVE_SYMBOL_BUTTON].Click();
            }
            if (removeOrLeave == true)
            {
                Waiter.WaitingCondition conditionDisepear = () => CheckContenetCurrancyPairInView(currancyPairs) == false;
                if (!Waiter.Wait(conditionDisepear, 2))
                {
                    throw new Exception("The currency pair has not disappeared from the list");
                }
            }
        }

        public bool RecordOrOverwriteWatchList(TradingStationWindow application,
            string nameList, string descriptionList)
        {
            dialogMSS[SAVE_WATCHLIST_BUTTON].Click();
            using (var saveWatchList = new SaveWatchlistDialog(application))
            {
                saveWatchList.RecordOverwritingWacthList(application, nameList,
                    descriptionList);
            }
            bool result = false;
            using (var treeView = new TreeViewHelper(dialogMSS, ".*"))
            {
                TreeViewItem findItem = treeView.FindElement(nameList);
                if (findItem!=null)
                {
                    result = true;
                }
            }
            PressClose();
            return result;
        }

        private void AddSymbol(List<string> currancyPairs)
        {
            using (var treeManageSymbol = new TreeViewHelper(dialogMSS, ".*"))
            {
                foreach (string currancyPair in currancyPairs)
                {
                    SelectItemTree(treeManageSymbol, currancyPair);
                }

                Waiter.WaitingCondition condition = () => dialogMSS[ADD_SYMBOL_BUTTON].Enabled == true;
                if (Waiter.Wait(condition, 2))
                {
                    dialogMSS[ADD_SYMBOL_BUTTON].Click();
                }

                Waiter.WaitingCondition conditionFilling = () =>
                    CheckContenetCurrancyPairInView(currancyPairs) == true;
                if (!Waiter.Wait(conditionFilling, 2))
                {
                    AddSymbol(currancyPairs);
                }
            }
        }

        private void PressClose()
        {
            dialogMSS[CLOSE_BUTTON].Click();
            Waiter.WaitingCondition condition = () => dialogMSS.Exists == false;
            if (!Waiter.Wait(condition))
            {
                dialogMSS[CLOSE_BUTTON].Click();
                Waiter.WaitingCondition conditionSecondChance = () => 
                    dialogMSS.Exists == false;
                if (!Waiter.Wait(conditionSecondChance))
                {
                    throw new Exception("Not close dialog window Manage Symbol Subscription");
                }
            }
        }

        private void InstallValueSearchSymbolListEdit(string valueSearch)
        {
            using (EditBox editSearch = (EditBox)dialogMSS[SEARCH_EDIT_SYMBOL_LIST_ID])
            {
                editSearch.Text = valueSearch;
                Thread.Sleep(1000); //Did not come up with adequate expectations
            }
        }

        private void InstallValueSearchSubscribedSymbolsEdit(string valueSearch)
        {
            using (EditBox editSearchSubscribedSymbol = (EditBox)dialogMSS[SEARCH_EDIT_SUBSCRIBED_SYMBOLS_ID])
            {
                editSearchSubscribedSymbol.Text = valueSearch;
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    manageSymbolView.Dispose();
                    dialogMSS.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

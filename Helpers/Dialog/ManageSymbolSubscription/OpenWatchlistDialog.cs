﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;

namespace TSAutotests.MSS
{
    class OpenWatchlistDialog : IDisposable
    {
        private Dialog dialogOpen;
        private ListView openWatchList;
        private const string TITLE_DIALOG = "Open Watchlist";
        private const string OK_BUTTON = "OK";

        public OpenWatchlistDialog(TradingStationWindow application)
        {
            dialogOpen = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
            if (dialogOpen == null)
            {
                throw new Exception("Can't find window Open Watchlist");
            }
            openWatchList = (ListView)dialogOpen.FindChild("SysListView32", ".*", true);
        }

        public bool SelectWatchlist(string nameWacthlist)
        {
            for (int i = 0; i < openWatchList.RowCount; i++)
            {
                if (openWatchList.CellValue(i, 0).Equals(nameWacthlist))
                {
                    openWatchList.ClickCell(i, 0, InputEventModifiers.LeftButton);
                    PressOK();
                    return true;
                }
            }
            throw new Exception("Not found watchlist");
        }

        private void PressOK()
        {
            dialogOpen[OK_BUTTON].Click();
            Waiter.WaitingCondition dialogDisepearCondition = () =>
               dialogOpen.Exists == false;
            if (!Waiter.Wait(dialogDisepearCondition))
            {
                throw new Exception("Not disepear dialog Open Watchlist");
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    openWatchList.Dispose();
                    dialogOpen.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}





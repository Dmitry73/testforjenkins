﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using NUnit.Framework;

namespace TSAutotests.MSS
{
    class MSSDialogError : IDisposable
    {
        private const int LABEL_FOR_CHECK_ID =  1001;
        private const string OK_BUTTON = "OK";
        private const string TITLE_DIALOG = "FXCM Trading Station Desktop";
        private const string LABEL_TEXT_MORE_20 = 
            "The maximum number of symbols you can subscribe to is 20.";
        private const string FIRST_PART_ERROR_OPEN_POSITION = 
            "You cannot disable ";
        private const string SECOND_PART_ERROR_OPEN_POSITION = 
            " while an Open Position is open for this symbol.";


        private Dialog dialogError;

        public MSSDialogError(TradingStationWindow application)
        {
            dialogError = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
            if (dialogError != null)
            {
                TestContext.Out.WriteLine("\r\nDialog error for MSS appear");
            }
        }


        public bool CheckEmergenceWindowMore20Symbol ()
        {
            bool result = false;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Equals(LABEL_TEXT_MORE_20))
            {
                TestContext.Out.WriteLine("\r\nCorrect type of error");
                result = true;
            }
            PressOK();
            return result;
        }

        public bool CheckEmergenceWindowSymbolOpenPosition (string symbol)
        {
            bool result = false;
            string lineForCompare = FIRST_PART_ERROR_OPEN_POSITION + symbol +
                SECOND_PART_ERROR_OPEN_POSITION;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Equals(lineForCompare))
            {
                TestContext.Out.WriteLine("\r\nCorrect type of error");
                result = true;
            }
            PressOK();
            return result;
        }


        private void PressOK()
        {
            dialogError[OK_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialogError.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                TestContext.Out.WriteLine("\r\nNot close window with error");
                Assert.Fail();
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nPress button OK and dialog MSS Error disapear");
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogError.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}


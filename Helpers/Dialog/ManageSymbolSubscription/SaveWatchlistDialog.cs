﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;

namespace TSAutotests.MSS
{
    class SaveWatchlistDialog : IDisposable
    {
        private Dialog dialogSave;
        private const string TITLE_DIALOG = "Save Watchlist";
        private const int NAME_EDIT_ID = 1001;
        private const int DESCRIPTION_EDIT_ID = 41020;
        private const string OK_BUTTON = "OK";
        private const string TITLE_DIALOG_REWRITE = "FXCM Trading Station Desktop";
        private const string YES_BUTTON = "Yes";

        public SaveWatchlistDialog(TradingStationWindow application)
        {
            dialogSave = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
            if (dialogSave == null)
            {
                throw new Exception("Can't find window Save Watchlist");
            }
        }

        public void RecordOverwritingWacthList (TradingStationWindow application,
            string nameList, string descriptionList)
        {
            EditBox nameListEdit = (EditBox)dialogSave[NAME_EDIT_ID];
            nameListEdit.Text = nameList;
            EditBox descriptionListEdit = (EditBox)dialogSave[DESCRIPTION_EDIT_ID];
            descriptionListEdit.Text = descriptionList;

            dialogSave[OK_BUTTON].Click();
            Dialog dialogRewrite = null;
            Waiter.WaitingCondition dialogExistsCondition = () =>
                (dialogRewrite = application.GetDialog(TITLE_DIALOG_REWRITE)) != null;
            if (Waiter.Wait(dialogExistsCondition, 4))
            {
                dialogRewrite[YES_BUTTON].Click();
                Waiter.WaitingCondition dialogDisepearCondition = () =>
                    dialogRewrite.Exists == false;
                if (!Waiter.Wait(dialogDisepearCondition))
                {
                    throw new Exception("Not disepear dialog Rewrite");
                }
            }
            nameListEdit.Dispose();
            descriptionListEdit.Dispose();
            dialogRewrite.Dispose();
        }
        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogSave.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}




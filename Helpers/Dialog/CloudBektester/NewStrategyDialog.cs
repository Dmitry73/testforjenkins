﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;
using System;

namespace TSAutotests
{
    //TEST CLASS
    class NewStrategyDialog : IDisposable
    {
        private const string TITLE_DIALOG = "New Strategy";
        private const string STRATEGIES_TITLE = "0";
        private const string ALERT_AND_SIGNALS = "1";
        private const string OK_BUTTON = "OK";
        private const int FILTER_EDIT_ID = 58236;
        private Dialog dialogNewStrategy;
        private ListView strategiesView;

        public NewStrategyDialog(TradingStationWindow application)
        {
            dialogNewStrategy = Waiter.WaitDialogAppear(application,
                TITLE_DIALOG);
            strategiesView = (ListView)dialogNewStrategy.FindChild(
                "SysListView32", STRATEGIES_TITLE, true);
        }

        public string TestRunStrategies(string findStrategy)
        {
            FilterEdit(findStrategy);
            Waiter.WaitingCondition waitSearch = () =>
                strategiesView.RowCount < 4;
            if (!Waiter.Wait(waitSearch))
            {
                throw new Exception("Not find name strategy");
            }
            strategiesView.ClickCell(0, 0, InputEventModifiers.LeftButton);
            string nameStrategy = strategiesView.CellValue(0, 0);
            PressOK();
            return nameStrategy;
        }

        protected void PressOK()
        {
            dialogNewStrategy[OK_BUTTON].Click();
            TestContext.Out.WriteLine(
                "\r\nPress button OK in dialog New Strategy");
            Waiter.WaitingCondition condition = () => 
                dialogNewStrategy.Exists == false;
            if (!Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine(
                "\r\nPress button OK second time in dialog New Strategy");
                dialogNewStrategy[OK_BUTTON].Click();
                Waiter.WaitingCondition secondChanceCondition = () => 
                dialogNewStrategy.Exists == false;
                if (!Waiter.Wait(secondChanceCondition))
                {
                    throw new Exception("Not close dialog New Strategy");
                }
                else
                {
                    TestContext.Out.WriteLine("\r\nA dialog Create Contingent Order Close");
                }
            }
            else
            {
                TestContext.Out.WriteLine("\r\nA dialog New Strategy Close");
            }
        }

        private void FilterEdit(string findStrategy)
        {
            using (EditBox filter = (EditBox)dialogNewStrategy[FILTER_EDIT_ID])
            {
                filter.Text = findStrategy;
                TestContext.Out.WriteLine(
                    "\r\nInstall filter in dilog New Strategy - " + findStrategy);
            }
        }

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogNewStrategy.Dispose();
                }
                disposedValue = true;
            }
#endif
        }

        private bool disposedValue = false;

        #endregion IDisposable
    }
}

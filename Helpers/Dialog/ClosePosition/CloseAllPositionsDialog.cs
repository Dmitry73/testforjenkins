﻿#define NO_DISPOSE

using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using NUnit.Framework;

namespace TSAutotests
{
    internal class CloseAllPositionsDialog : IDisposable
    {
        public CloseAllPositionsDialog(TradingStationWindow application)
        {
            dialogClosePosition = Waiter.WaitDialogAppear(application, 
                CLOSE_ALL_POSITIONS_DIALOG_TITLE);
        }

        public void InstallValue(string orderType, string timeInForce, 
            double marketRangeValue = 0.1)
        {
            using (ComboBox orderTypeComboBox = (ComboBox)dialogClosePosition[ORDER_TYPE_COMBO_BOX])
            {
                orderTypeComboBox.SelectItem(orderType);
            }
            using (ComboBox timeInForceComboBox = (ComboBox)dialogClosePosition[TIME_IN_FORCE_COMBO_BOX])
            {
                timeInForceComboBox.SelectItem(timeInForce);
            }
            if (orderType.Equals(MARKET_RANGE_ORDER_TYPE))
            {
                using (EditBox marketRangeValueEdit = (EditBox)dialogClosePosition[MARKET_RANGE_VALUE])
                {
                    marketRangeValueEdit.Text = Convert.ToString(marketRangeValue);
                }
            }
        }

        public void PressOK(TradingStationWindow application)
        {
            dialogClosePosition[OK_BUTTON].Click();
            Waiter.WaitingCondition condition = () => dialogClosePosition.Exists
                == false;
            if (!Waiter.Wait(condition, 5))
            {
                Dialog dialogNoLongerBeHit = application.GetDialog(FXCM_ERROR);
                if (dialogNoLongerBeHit[OK_BUTTON].Exists)
                {
                    dialogNoLongerBeHit[OK_BUTTON].Click();
                    Waiter.WaitingCondition conditionDisepear = () =>
                        dialogNoLongerBeHit.Exists == false;
                    if (!Waiter.Wait(conditionDisepear, 5))
                    {
                    }
                    PressOK(application);
                }
            }
            else
            {
                TestContext.Out.WriteLine("\r\nDialog Close All Position disappear");
            }
        }

        private const string CLOSE_ALL_POSITIONS_DIALOG_TITLE = "Close All Positions";
        private const string FXCM_ERROR = "FXCM Trading Station Desktop";
        private const string MARKET_RANGE_ORDER_TYPE = "Market Range";
        private const int MARKET_RANGE_VALUE = 49480;
        private const string OK_BUTTON = "OK";
        private const int ORDER_TYPE_COMBO_BOX = 49479;
        private const int TIME_IN_FORCE_COMBO_BOX = 47106;
        private Dialog dialogClosePosition;

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            //#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogClosePosition.Dispose();
                }
                disposedValue = true;
            }
            //#endif
        }

        private bool disposedValue = false;

        #endregion IDisposable
    }
}
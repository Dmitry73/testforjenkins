﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using NUnit.Framework;

namespace TSAutotests
{
    class CloseAllPositionsForSymbol : IDisposable
    {
        public CloseAllPositionsForSymbol(TradingStationWindow application)
        {
            dialogClosePosition = Waiter.WaitDialogAppear(application, 
                CLOSE_ALL_POSITIONS_FOR_SYMBOL_DIALOG_TITLE);
        }

        public void InstallValue(string orderType, string timeInForce, 
            string symbol)
        {
            using (ComboBox orderTypeComboBox = (ComboBox)dialogClosePosition[ORDER_TYPE_COMBO_BOX_ID])
            {
                orderTypeComboBox.SelectItem(orderType);
            }
            using (ComboBox timeInForceComboBox = (ComboBox)dialogClosePosition[TIME_IN_FORCE_COMBO_BOX_ID])
            {
                timeInForceComboBox.SelectItem(timeInForce);
            }
            using (ComboBox symbolComboBox = (ComboBox)dialogClosePosition[SYMBOL_COMBO_BOX_ID])
            {
                symbolComboBox.SelectItem(symbol);
            }
        }

        public void PressOK(TradingStationWindow application)
        {
            dialogClosePosition[OK_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialogClosePosition.Exists 
                == false;
            if (!Waiter.Wait(condition, 5))
            {
                Dialog dialogNoLongerBeHit = application.GetDialog(FXCM_ERROR);
                if (dialogNoLongerBeHit[OK_BUTTON].Exists)
                {
                    dialogNoLongerBeHit[OK_BUTTON].Click();
                    Waiter.WaitingCondition conditionDisepear = () =>
                        dialogNoLongerBeHit.Exists == false;
                    if (!Waiter.Wait(conditionDisepear, 5))
                    {

                    }
                    PressOK(application);
                }
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nDialog Close Position for symbol disappeared");
            }
        }

        private const string CLOSE_ALL_POSITIONS_FOR_SYMBOL_DIALOG_TITLE =
            "Close All Positions for Symbol";
        private const string FXCM_ERROR = "FXCM Trading Station Desktop";
        private const string OK_BUTTON = "OK";
        private const int ORDER_TYPE_COMBO_BOX_ID = 49476;
        private const int SYMBOL_COMBO_BOX_ID = 49451;
        private const int TIME_IN_FORCE_COMBO_BOX_ID = 47106;
        private Dialog dialogClosePosition;

        #region IDisposable 
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogClosePosition.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        private bool disposedValue = false;
        #endregion
    }
}


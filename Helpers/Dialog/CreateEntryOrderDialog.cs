﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests
{

    class CreateEntryOrderDialog : IDisposable
    {
        private Dialog dialog;

        private const int SYMBOL_COMBOBOX_ID = 5011;
        private const int AMOUNT_K_COMBOBOX_ID = 1001;
        private const int ACCOUNT_ID = 5010;
        private const int RATE_EDIT_ID = 5017;
        private const int RATE_STOP_EDIT_ID = 5020;
        private const int RATE_LIMIT_EDIT_ID = 5021;
        private const int ORDER_TYPE_COMBOBOX_ID = 49370;
        private const int RANGE_ENTRY_EDIT_BOX_ID = 49371;
        private const int TIME_IN_FORCE_COMBOBOX_ID = 47106;
        private const int TRAILING_COMBOBOX_ID = 49530;
        private const int IN_PIP_CHECK_BOX_ID = 49403;
        private const int TIME_IN_FORCE_GTD_DATE_TIME_ID = 55055;
        private const string SELL_RADIOBUTTON = "Sell";
        private const string BUY_RADIOBUTTON = "Buy";
        private const string ADVANCED_BUTTON = "Advanced>>";
        private const string OK_BUTTON = "OK";
        private const string STOP_CHECKED = "Stop:";
        private const string LIMIT_CHECKED = "Limit:";
        private const string FXCM_ERROR = "FXCM Trading Station Desktop";
        private const bool IN_PIP_CHECK_ON = true;
        private const bool IN_PIP_CHECK_OFF = false;
        private const string ORDER_TYPE_ENTRY = "Entry";
        private const string ORDER_TYPE_RANGE_ENTRY = "Range Entry";
        private const string TIME_IN_FORCE_GTD_VALUE = "GTD";
        private const string TIME_IN_FORCE_GTC_VALUE = "GTC";
        private const string CREATE_ENTRY_ORDER_DIALOG = "Create Entry Order";
        private const string ADVANCED_BUTTON_OPEN = "Advanced>>";

        public CreateEntryOrderDialog(TradingStationWindow application)
        {
            dialog = application.GetDialog(CREATE_ENTRY_ORDER_DIALOG);
            if (dialog == null)
            {
                Waiter.WaitingCondition dialogExistsCondition = () => (application.GetDialog(CREATE_ENTRY_ORDER_DIALOG)) != null;
                if (Waiter.Wait(dialogExistsCondition))
                {
                    dialog = application.GetDialog(CREATE_ENTRY_ORDER_DIALOG);
                    TestContext.Out.WriteLine("\r\nA dialog Create Entry Order appeared");
                }
                if (dialog[ADVANCED_BUTTON_OPEN] != null)
                {
                    if (dialog[ADVANCED_BUTTON_OPEN].Enabled)
                    {
                        dialog[ADVANCED_BUTTON_OPEN].Click();
                    }
                }
            }
        }

        public bool CreateEntryOrderDefault(TradingStationWindow application,
            string symbol, BuySellEnum buyOrSell, int amount, double rateCoefficient)
        {
            string valueAccaunt = GeneralInstall(symbol, amount, ORDER_TYPE_ENTRY,
                buyOrSell, TIME_IN_FORCE_GTC_VALUE);
            double price = MathHelper.RoundingDependingOnSignsAfterComma(
                InstallRateEdit(rateCoefficient));

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderDefault(symbol, valueAccaunt,
                    buyOrSell, amount, price, rateCoefficient, ORDER_TYPE_ENTRY);
            }
            return result;
        }

        public bool CreateEntryOrderStopLimit(TradingStationWindow application,
            string symbol, BuySellEnum buyOrSell, int amount, double rateCoefficient,
            double stopRateDiffernce, double limitRateDifference)
        {
            string valueAccaunt = GeneralInstall(symbol, amount, ORDER_TYPE_ENTRY,
                buyOrSell, TIME_IN_FORCE_GTC_VALUE);
            double price = MathHelper.RoundingDependingOnSignsAfterComma(
                InstallRateEdit(rateCoefficient));

            InPipsOnCheckBoks(IN_PIP_CHECK_OFF);
            double stopRateInstall = double.Parse(InstallValueStop(stopRateDiffernce,
                buyOrSell, price));
            double limitRateInstall = double.Parse(InstallValueLimit(
                limitRateDifference, buyOrSell, price));

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderWithStopLimit(symbol, valueAccaunt, buyOrSell, amount, price,
                    rateCoefficient, stopRateInstall, limitRateInstall, ORDER_TYPE_ENTRY);
            }
            return result;
        }

        public bool CreateEntryOrderStopLimitInPip(TradingStationWindow application,
            string symbol, BuySellEnum buyOrSell, int amount, double
            rateCoefficient, string stopPip, string limitStop)
        {
            string valueAccaunt = GeneralInstall(symbol, amount, ORDER_TYPE_ENTRY,
                buyOrSell, TIME_IN_FORCE_GTC_VALUE);
            double price = MathHelper.RoundingDependingOnSignsAfterComma(
                InstallRateEdit(rateCoefficient));

            InPipsOnCheckBoks(IN_PIP_CHECK_ON);
            InstallValueLimitInPip(limitStop);
            InstallValueStopInPip(stopPip);

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderWithStopLimitInPip(symbol,
                    valueAccaunt, buyOrSell, amount, price,
                    rateCoefficient, stopPip, limitStop, ORDER_TYPE_ENTRY);
            }
            return result;
        }


        public bool CreateEntryOrderRangeEntry(TradingStationWindow application,
            string symbol, BuySellEnum buyOrSell,
            int amount, double rateCoefficient, string valueRangeEntry)
        {
            string accountValue = GeneralInstall(symbol, amount,
                ORDER_TYPE_RANGE_ENTRY, buyOrSell, TIME_IN_FORCE_GTC_VALUE);
            double price = MathHelper.RoundingDependingOnSignsAfterComma(InstallRateEdit(rateCoefficient));
            InstallValueRangeEntry(valueRangeEntry);

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderWithRangeOrderType(symbol,
                    accountValue, buyOrSell, amount,
                    price, rateCoefficient, valueRangeEntry, ORDER_TYPE_RANGE_ENTRY);
            }
            return result;
        }

        public bool CreateEntryOrderRangeEntryWithStopLimit(TradingStationWindow
            application, string symbol, BuySellEnum buyOrSell, int amount, double
            rateCoefficient, string valueRangeEntry, double stopRateDiffernce,
            double limitRateDifference)
        {
            string accountValue = GeneralInstall(symbol, amount, ORDER_TYPE_RANGE_ENTRY,
                buyOrSell, TIME_IN_FORCE_GTC_VALUE);

            double price = MathHelper.RoundingDependingOnSignsAfterComma(
                InstallRateEdit(rateCoefficient));
            InstallValueRangeEntry(valueRangeEntry);
            InPipsOnCheckBoks(IN_PIP_CHECK_OFF);
            double stopRateInstall = double.Parse(InstallValueStop(stopRateDiffernce,
                buyOrSell, price));
            double limitRateInstall = double.Parse(InstallValueLimit(limitRateDifference,
                buyOrSell, price));

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderRangeEntryStopLimit(symbol,
                    accountValue, buyOrSell, amount,
                    price, rateCoefficient, valueRangeEntry, ORDER_TYPE_RANGE_ENTRY,
                    stopRateInstall, limitRateInstall);
            }
            return result;
        }

        public bool CreateEntryOrderRangeEntryGTD(TradingStationWindow application,
            string symbol, BuySellEnum buyOrSell,
            int amount, double rateCoefficient, string valueRangeEntry, int plusDay)
        {
            string accountValue = GeneralInstall(symbol, amount, ORDER_TYPE_RANGE_ENTRY,
                buyOrSell, TIME_IN_FORCE_GTD_VALUE);
            double price = MathHelper.RoundingDependingOnSignsAfterComma(
                InstallRateEdit(rateCoefficient));
            InstallValueRangeEntry(valueRangeEntry);
            string dataAndTime = DateTimeInstall(plusDay);

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderWithRangeOrderTypeGTD(symbol,
                    accountValue, buyOrSell, amount, price,
                    rateCoefficient, valueRangeEntry, ORDER_TYPE_RANGE_ENTRY,
                    dataAndTime);
            }
            return result;
        }

        public bool CreateEntryOrderGTDTimeInForce(TradingStationWindow application,
            string symbol, BuySellEnum buyOrSell,
            int amount, double rateCoefficient, int plusDay)
        {
            string accountValue = GeneralInstall(symbol, amount, ORDER_TYPE_ENTRY,
                buyOrSell, TIME_IN_FORCE_GTD_VALUE);
            double price = MathHelper.RoundingDependingOnSignsAfterComma(
                InstallRateEdit(rateCoefficient));
            string dataAndTime = DateTimeInstall(plusDay);

            CloseDialog();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckEntryOrderGTD(symbol, accountValue,
                    buyOrSell, amount, price, rateCoefficient, dataAndTime, ORDER_TYPE_ENTRY);
            }
            return result;
        }
        #region Secondary function
        private void CloseDialog()
        {
            dialog[OK_BUTTON].Click();
            TestContext.Out.WriteLine("\r\nPress button OK in dialog Create Entry Order");
            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                dialog[OK_BUTTON].Click();
                TestContext.Out.WriteLine(
                    "\r\nPress button OK second time in dialog Create Entry Order");
                Waiter.WaitingCondition conditionSecondChance = () => dialog.Exists == false;
                if (!Waiter.Wait(conditionSecondChance, 5))
                {
                    Assert.Fail();
                    TestContext.Out.WriteLine(
                        "\r\nNot close dialog Create Entry Order");
                }
            }
            else
            {
                TestContext.Out.WriteLine("\r\ndilaog create entry order close");
            }
        }

        private string GeneralInstall(string symbol, int amount, string orderType,
            BuySellEnum buyOrSell, string timeInForce)
        {
            string valueAccaunt = ReturnAccountValue();
            SelectSymbol(symbol);
            EditAmountK(amount);
            SelectOrderType(orderType);
            BuyOrSell(buyOrSell);
            SelectTimeInForce(timeInForce);
            return valueAccaunt;
        }
        #endregion

        #region return value
        private string ReturnAccountValue()
        {
            using (ComboBox selectSymbol = (ComboBox)dialog[ACCOUNT_ID])
            {
                string valueReturn = selectSymbol.Text;
                return valueReturn;
            }
        }

        private string DateTimeInstall(int plusDays)
        {
            using (DateTimePicker dateTime = (DateTimePicker)dialog[TIME_IN_FORCE_GTD_DATE_TIME_ID])
            {
                DateTime installTime = DateTime.Now;
                dateTime.Value = installTime.AddDays(plusDays);
                string resultForCheck = dateTime.Text.Insert(6, "20"); //write 2017 instead
                TestContext.Out.WriteLine("\r\ninsatall DataTime - " + resultForCheck);
                return resultForCheck;
            }
        }
        #endregion

        #region EditBox
        private void InstallValueLimitInPip(string pipLimit)
        {
            if (!dialog[RATE_LIMIT_EDIT_ID].Enabled)
            {
                dialog[LIMIT_CHECKED].Click();
            }
            using (EditBox limit = (EditBox)dialog[RATE_LIMIT_EDIT_ID])
            {
                limit.Text = pipLimit;
                TestContext.Out.WriteLine("\r\nInstall value Pegged limit - " + pipLimit);
            }
        }

        private void InstallValueStopInPip(string pipStop)
        {
            if (!dialog[RATE_STOP_EDIT_ID].Enabled)
            {
                dialog[STOP_CHECKED].Click();
            }
            using (EditBox stop = (EditBox)dialog[RATE_STOP_EDIT_ID])
            {
                stop.Text = pipStop;
                TestContext.Out.WriteLine("\r\nInstall value Pegged stop - " + pipStop);
            }
        }

        private void EditAmountK(int amount)
        {
            using (EditBox editAmount = (EditBox)dialog[AMOUNT_K_COMBOBOX_ID])
            {
                editAmount.Text = Convert.ToString(amount);
                TestContext.Out.WriteLine("\r\nInstall amount - " + amount);
            }
        }

        private double InstallRateEdit(double rateCoefficient)
        {
            return CarcasInstallValueNonPegged(rateCoefficient, RATE_EDIT_ID);
        }

        protected double CarcasInstallValueNonPegged(double rateCoefficient, int ID)
        {
            using (EditBox editRate = (EditBox)dialog[ID])
            {
                int count = editRate.Text.Length;
                double installValue = double.Parse(editRate.Text) +
                    double.Parse(editRate.Text) * rateCoefficient;
                installValue = MathHelper.RoundingDependingOnSignsAfterComma(
                    installValue);
                string installString = installValue.ToString();
                while (installString.Length < count)
                {
                    installString = installString + "0";
                }
                string recordValue = installString.Remove(count - 1, 7 - count);
                while (recordValue.Length < count)
                {
                    recordValue = recordValue + "0";
                }
                editRate.Text = recordValue;
                TestContext.Out.WriteLine("\r\nInstall value limit - " + ReturnRate());
                return double.Parse(recordValue);
            }
        }

        protected string CarcasInstallValueNonPeggedStopLimit(double rateCoefficient, int ID)
        {
            using (EditBox editRate = (EditBox)dialog[ID])
            {
                int count = editRate.Text.Length;
                double installValue = double.Parse(editRate.Text) +
                    double.Parse(editRate.Text) * rateCoefficient;
                installValue = MathHelper.RoundingDependingOnSignsAfterComma(
                    installValue);
                string installString = installValue.ToString();
                while (installString.Length < count)
                {
                    installString = installString + "0";
                }
                string recordValue = installString.Remove(count - 1, 7 - count);
                while (recordValue.Length == count)
                {
                    recordValue = recordValue + "0";
                }
                editRate.Text = recordValue;
                TestContext.Out.WriteLine("\r\nInstall value stop or limit - " + recordValue);
                return recordValue;
            }
        }
        private string CarcassReturnRate(int ID)
        {
            using (EditBox rate = (EditBox)dialog[ID])
            {
                string returnValue = rate.Text;
                return returnValue;
            }
        }
        private string ReturnRate()
        {
            return CarcassReturnRate(RATE_EDIT_ID);
        }

        private string ReturnLimit()
        {
            return CarcassReturnRate(RATE_LIMIT_EDIT_ID);
        }

        private string ReturnStop()
        {
            return CarcassReturnRate(RATE_STOP_EDIT_ID);
        }


        private string InstallValueStop(double stopRate, BuySellEnum buySell, double price)
        {
            if (!dialog[RATE_STOP_EDIT_ID].Enabled)
            {
                dialog[STOP_CHECKED].Click();
            }
            if (buySell == BuySellEnum.BUY)
            {
                stopRate = -stopRate;
            }

            return CarcasInstallValueNonPeggedStopLimit(stopRate, RATE_STOP_EDIT_ID);
        }

        private string InstallValueLimit(double limitRate, BuySellEnum buySell, double price)
        {
            if (!dialog[RATE_LIMIT_EDIT_ID].Enabled)
            {
                dialog[LIMIT_CHECKED].Click();
            }
            if (buySell == BuySellEnum.SELL)
            {
                limitRate = -limitRate;
            }
            return CarcasInstallValueNonPeggedStopLimit(limitRate, RATE_LIMIT_EDIT_ID);
        }

        private void InstallValueRangeEntry(string valueRangeEntry)
        {
            using (EditBox installValue = (EditBox)dialog[RANGE_ENTRY_EDIT_BOX_ID])
            {
                installValue.Text = valueRangeEntry;
                TestContext.Out.WriteLine("\r\nInstall range entry - " + valueRangeEntry);
            }
        }
        #endregion

        #region ComboBox
        private void SelectSymbol(string symbol)
        {
            using (ComboBox selectSymbol = (ComboBox)dialog[SYMBOL_COMBOBOX_ID])
            {
                selectSymbol.SelectItem(symbol);
            }
        }

        private void SelectOrderType(string orderType)
        {
            using (ComboBox selectOrderType = (ComboBox)dialog[ORDER_TYPE_COMBOBOX_ID])
            {
                selectOrderType.SelectItem(orderType);
            }
        }

        private void SelectTimeInForce(string timeInForceType)
        {
            using (ComboBox selectTimeInForce = (ComboBox)dialog[TIME_IN_FORCE_COMBOBOX_ID])
            {
                selectTimeInForce.SelectItem(timeInForceType);
            }
        }

        #endregion

        #region CheckBox
        private void BuyOrSell(BuySellEnum buySell)
        {
            if (buySell == BuySellEnum.BUY)
            {
                dialog[BUY_RADIOBUTTON].Click();
            }
            else
            {
                dialog[SELL_RADIOBUTTON].Click();
            }
            TestContext.Out.WriteLine("\r\nInstall Buy/Sell - " +
                ((buySell == BuySellEnum.BUY) ? "BUY" : "SELL"));
        }

        public void InPipsOnCheckBoks(bool onOff)
        {
            using (CheckBox inPips = (CheckBox)dialog[IN_PIP_CHECK_BOX_ID])
            {
                if (onOff)
                {
                    if (inPips.Checked == 0)
                    {
                        inPips.Click();
                    }
                }
                else
                {
                    if (inPips.Checked == 1)
                    {
                        inPips.Click();
                    }
                }
                TestContext.Out.WriteLine("\r\nCheckBox In Pip - " + (onOff ? "ON" : "OFF"));
            }
        }
        #endregion

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
        #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
        #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion

    }
}

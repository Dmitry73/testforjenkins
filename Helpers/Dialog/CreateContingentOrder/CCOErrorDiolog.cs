﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests.CCO
{
    class CCOErrorDialog : IDisposable
    {
        private const int LABEL_FOR_CHECK_ID = 1001;
        private const string OK_BUTTON = "OK";
        private const string TITLE_DIALOG = "FXCM Trading Station Desktop";
        private const string RATE_ERROR = "The value entered is either " + 
            "too far away from the market price or contains an invalid character.";
        private const string RANGE_ENTRY_ERROR = "The Range Entry rate must be";
    

        private Dialog dialogError;

        public CCOErrorDialog(TradingStationWindow application)
        {
            dialogError = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
        }

        public bool RangeEntryInPipError()
        {
            bool result = false;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Contains(RANGE_ENTRY_ERROR))
            {
                TestContext.Out.WriteLine("\r\nCorrect type of error");
                result = true;
            }
            PressOK();
            return result;
        }

        public bool RateError()
        {
            bool result = false;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Equals(RATE_ERROR))
            {
                TestContext.Out.WriteLine("\r\nCorrect type of error");
                result = true;
            }
            PressOK();
            return result;
        }

        public bool OrderTypeReErrorRate ()
        {
            bool result = false;
            if (dialogError!=null)
            {
                TestContext.Out.WriteLine("\r\nCorrect type of error");
                result = true;
            }
            PressOK();
            return result;
        }

        private void PressOK()
        {
            dialogError[OK_BUTTON].Click();
            TestContext.Out.WriteLine("\r\nPress button OK in dialog for error");
            Waiter.WaitingCondition condition = () => dialogError.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                TestContext.Out.WriteLine("\r\nNot close window with error");
                Assert.Fail();
            }
            else
            {
                TestContext.Out.WriteLine("\r\nDialog for error close");
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogError.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}


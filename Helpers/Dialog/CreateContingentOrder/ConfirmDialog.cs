﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests.CCO
{
    class ConfirmDialog : IDisposable
    {
        private const string OK_BUTTON = "OK";
        private const string YES_BUTTON = "Yes";
        private const string TITLE_DIALOG = "FXCM Trading Station Desktop";


        private Dialog dialogError;

        public ConfirmDialog(TradingStationWindow application)
        {
            dialogError = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
        }

        public void PressYes()
        {
            dialogError[YES_BUTTON].Click();
            TestContext.Out.WriteLine("\r\nPress button Yes in dialog confirm");
            Waiter.WaitingCondition condition = () => dialogError.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                throw new Exception("Not close window with error");
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogError.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}


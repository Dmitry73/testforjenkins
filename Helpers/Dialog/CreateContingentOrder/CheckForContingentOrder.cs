﻿using gehtsoft.tscontroller;
using NUnit.Framework;
using System;
using TSAutotests.CCO;


namespace TSAutotests
{
    class CheckForContingentOrder
    {
        private const string CHANGE_ENTRY_ORDER_POPUP = "Change Entry Order...";
        private const string REMOVE_FROM_OCO_ORDER_POPUP = "Remove from OCO order";
        private const string STOP_LIMIT_POPUP = "Stop/Limit...";
        private const string REMOVE_ORDER_POPUP = "Remove Order";
        private const string TITLE_DIALOG_CONFIRM = "FXCM Trading Station Desktop";
        private const int RATE_BORDER_PEGGED = 7;
        private const bool ON = true;
        private const bool OFF = false;

        public bool CheckEditAmountInTabOrders (TradingStationWindow application,
            string symbol, OrderGroups orderType, int changeAmount)
        {
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.ChangeAmount(changeAmount);
            }
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                result  = orderTab.CheckAmmount(symbol, changeAmount, orderType);
            }
            return result;
        }

        public bool RemoveOCOOrder (TradingStationWindow application,
            string primarySymbol, string secondarySymbol, OrderGroups orderType)
        {
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                string idPrimary = orderTab.ReturnIdOrder(orderType, primarySymbol);
                string idSecondary = orderTab.ReturnIdOrder(orderType, secondarySymbol);
                OpenItemPopupMenuInGroupOrder(application, primarySymbol, orderType,
                    REMOVE_FROM_OCO_ORDER_POPUP);
                using (var confirmDialog = new ConfirmDialog(application))
                {
                    confirmDialog.PressYes();
                }
                result = orderTab.CheckExistanceOrderByID(idPrimary, primarySymbol,
                        OrderGroups.Entry); //Must go to Entry
                result = result && orderTab.CheckExistanceOrderByID(idSecondary, 
                    secondarySymbol, OrderGroups.OCO); //Must go to OCO
            }
            return result;
        }




        public bool TaskStopLimitIfOrderTrailing(TradingStationWindow application,
            string primarySymbol, OrderGroups orderType)
        {
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                OpenItemPopupMenuInGroupOrder(application, primarySymbol, orderType,
                    STOP_LIMIT_POPUP);
                using (var stopLimitOrderDialog = new StopLimitOrderDialog(application))
                {
                    result = stopLimitOrderDialog.StopLimitPeggedTrailingOrder();
                }
            }
            return result;
        }

        public bool PrimarySameSymbolCanNotTrailing (TradingStationWindow application,
            string primarySymbol, OrderGroups orderType, BuySellEnum buySellPrimary)
        {
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                OpenItemPopupMenuInGroupOrder(application, primarySymbol, orderType,
                    CHANGE_ENTRY_ORDER_POPUP);
                using (var changeEntryOrder = new ChangeEntryOrderDialog(application))
                {
                    result = changeEntryOrder.DisableTrailing();
                }
            }
            return result;
        }

        public bool ChangeRateInPipNoError(TradingStationWindow application,
            string findSymbol, OrderGroups orderType, string changeRate)
        {
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                OpenItemPopupMenuInGroupOrder(application, findSymbol, orderType,
                    CHANGE_ENTRY_ORDER_POPUP);
                using (var changeEntryOrder = new ChangeEntryOrderDialog(application))
                {
                    result = changeEntryOrder.ChangeRatePeggedNoError(application,
                        changeRate);
                }
            }
            return result;
        }


        public bool CheckEditTrailingInTabOrders (TradingStationWindow application,
            string symbol, OrderGroups orderType, string trailing, BuySellEnum buySell,
            bool takeAccountIfThen = false)
        {
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.ChangeTrailing(trailing);
            }
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckTrailing(symbol, orderType, buySell, takeAccountIfThen);
            }
            return result;
        }

        public bool CheckEditTrailingInTabOrdersSelectBuySell(TradingStationWindow 
            application, string symbol, OrderGroups orderType, string trailing, 
            BuySellEnum buySellSelectSymbol, bool takeAccountIfThen = false)
        {
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP, buySellSelectSymbol);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.ChangeTrailing(trailing);
            }
            bool result;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckTrailing(symbol, orderType, buySellSelectSymbol, takeAccountIfThen);
            }
            return result;
        }

        public bool StopLimitNonPeggedNotSelecTtrailing(TradingStationWindow application,
            string symbol, OrderGroups orderType)
        {
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP);
            bool result = false;
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.StopLimitNonPeggedDisableTrailing();
            }
            return result;
        }

        public bool StopLimitPeggedSaveTrailing(TradingStationWindow application,
            string symbol, OrderGroups orderType, string trailing)
        {
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.ChangeTrailing(trailing);
            }
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP);
            bool result = false;
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                if (result = changeOrderDialog.ReturnValueTrailing().Equals(trailing))
                {
                    TestContext.Out.WriteLine("\r\nThe value of trailing matches");
                }
                changeOrderDialog.PressCancel();
            }
            return result;
        }

        public bool ErrorInEditRate(TradingStationWindow application,
            string symbol, OrderGroups orderType, double changeRate)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.CheckErrorChangeRate(application, changeRate);
            }

            return result;
        }

        public bool EditValueRate(TradingStationWindow application,
            string symbol, OrderGroups orderType, double rateCoeff, 
            BuySellEnum buySell, bool takeAccountIfThen = false)
        {
            bool result = false;
            double checkRate = 0;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                checkRate = changeOrderDialog.ChangeRate(rateCoeff);
            }
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckRate(symbol, orderType, buySell,
                    checkRate, takeAccountIfThen);
            }
            return result;
        }

        public bool EditRateOnOffFormatNonPeggedOrPegged(TradingStationWindow 
            application, string symbol, OrderGroups orderType, BuySellEnum buySell,
            bool onOffInPip)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.InPipsCheckBoxOnOff(onOffInPip, 0.01);
            }
            using (var orderTab = new OrdersTab(application))
            {
                if (onOffInPip)
                {
                    Waiter.WaitingCondition condition = () => (result =
                    orderTab.ReturnRate(symbol, buySell, orderType).Length <
                        RATE_BORDER_PEGGED);
                    Waiter.Wait(condition);
                }
                else
                {
                    Waiter.WaitingCondition condition = () => (result =
                    orderTab.ReturnRate(symbol, buySell, orderType).Length ==
                     RATE_BORDER_PEGGED);
                    Waiter.Wait(condition);
                }

                if (result == false)
                {
                    TestContext.Out.WriteLine("\r\nReturn rate = " +
                        orderTab.ReturnRate(symbol, buySell, orderType));
                }
            }
            return result;
        }

        public bool EditRateOnFormatNonPeggedSecondary(TradingStationWindow
            application, string symbol, OrderGroups orderType,
            BuySellEnum buySell, bool takeAccountIfThen = false)
        {
            bool result = false, OFF = false;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.InPipsCheckBoxOnOff(OFF);
            }
            using (var orderTab = new OrdersTab(application))
            {
                Waiter.WaitingCondition condition = () => (result =
                orderTab.ReturnRate(symbol, buySell, orderType).Length ==
                    RATE_BORDER_PEGGED);
                Waiter.Wait(condition);

                if (result == false)
                {
                    TestContext.Out.WriteLine("\r\nReturn rate = " +
                        orderTab.ReturnRate(symbol, buySell, orderType, 
                        takeAccountIfThen));
                }
            }
            return result;
        }

        public bool DisableAndOffInPip(TradingStationWindow application,
            string symbol, OrderGroups orderType, double rateCoefficient)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP);

            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.DisableAndOffInPip(rateCoefficient);
            }
            return result;
        }

        public bool ChangeEntryRatePeggedAndDisable(TradingStationWindow application,
            string symbol, OrderGroups orderType, BuySellEnum buySell)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);

            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.ReturnRate().Length <
                    RATE_BORDER_PEGGED && changeOrderDialog.DisableInPip();
                changeOrderDialog.PressCancel();
            }
            return result;
        }

        public bool ErrorRangeEntry (TradingStationWindow application,
            string symbol, OrderGroups orderType, double rateCoefficient)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType, 
                CHANGE_ENTRY_ORDER_POPUP);

            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.RangeEntryError(application, rateCoefficient);
            }
            return result;
        }
        public bool DisableTrailing(TradingStationWindow application,
            string symbol, OrderGroups orderType, BuySellEnum buySell)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.DisableTrailing();
            }
            return result;
        }

        public bool OffCheckBoxInPIp(TradingStationWindow application,
            string symbol, OrderGroups orderType, BuySellEnum buySell)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.DisableTrailing();
            }
            return result;
        }

        public bool DisableInPipsNonPeggedFormat(TradingStationWindow application,
            string symbol, OrderGroups orderType, BuySellEnum buySell)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.DisableInPip() &&
                    changeOrderDialog.ReturnRate().Length == RATE_BORDER_PEGGED;
                changeOrderDialog.PressCancel();
            }
            return result;
        }

        public bool OffCheckBoxInPipChangeEntry(TradingStationWindow application,
            string symbol, OrderGroups orderType, BuySellEnum buySell)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                changeOrderDialog.InPipsCheckBoxOnOff(OFF);
            }
            using (var orderTab = new OrdersTab(application))
            {
                Waiter.WaitingCondition condition = () => (result =
                orderTab.ReturnRate(symbol, buySell, orderType).Length ==
                    RATE_BORDER_PEGGED);
                Waiter.Wait(condition);  ;
            }
            return result;
        }

        public bool CheckErrorEditRateSecondaryLESE(TradingStationWindow 
            application, string symbol, OrderGroups orderType, BuySellEnum 
            buySell, double changeRateCoeff)
        {

            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.ErrorEditRate(application, 
                    changeRateCoeff);
            }
            return result;
        }

        public bool ChangeRateOnNotValidErrorRE(TradingStationWindow 
            application, string symbol, OrderGroups orderType, BuySellEnum
            buySell, double changeRateCoeff)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.ErrorEditRateRE(application, 
                    changeRateCoeff);
            }
            return result;
        }

        public bool ChangeRateOnNotValidErrorREInPip(TradingStationWindow
            application, string symbol, OrderGroups orderType, BuySellEnum
            buySell, double changeRateCoeff)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.ErrorEditRateREInPip(application, 
                    changeRateCoeff);
            }
            return result;
        }

        public bool EditStopLimitRate(TradingStationWindow application,
            string secondarySymbol, OrderGroups orderType, double stopValueRate,
            double limitValueRate)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrder(application, secondarySymbol, orderType,
                STOP_LIMIT_POPUP);
            using (var stopLimitDialog = new StopLimitOrderDialog(application))
            {
                stopLimitDialog.EditStopLimitSecondaryInPip(stopValueRate,
                    limitValueRate);
            }
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckStopLimitInPip(orderType, secondarySymbol, 
                    stopValueRate, limitValueRate);
            }

            return result;
        }

        public bool CheckDeleteOrder(TradingStationWindow application,
            string symbol, OrderGroups orderType)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrder(application, symbol, orderType,
                REMOVE_ORDER_POPUP);
            YesNoDialog.PressYes(TITLE_DIALOG_CONFIRM, application);

            using (var orderTab = new OrdersTab(application))
            {
                Waiter.WaitingCondition condition = () => 
                    (orderTab.FindFirstNonEmptyOrdersGroup()) == null;
                if (!Waiter.Wait(condition))
                {
                    throw new Exception("not delete order");
                }
                result = true;
            }
            return result;



        }

        public bool CheckErrorEditRateSecondaryLESEInPip(TradingStationWindow
            application, string symbol, OrderGroups orderType, BuySellEnum 
            buySell, double changeRate)
        {
            bool result = false;
            OpenItemPopupMenuInGroupOrderBuySell(application, symbol, orderType,
                CHANGE_ENTRY_ORDER_POPUP, buySell);
            using (var changeOrderDialog = new ChangeEntryOrderDialog(application))
            {
                result = changeOrderDialog.ErrorEditRateInPip(application, 
                    changeRate);
            }
            return result;
        }

        private void OpenItemPopupMenuInGroupOrder(TradingStationWindow 
            application, string symbol, OrderGroups orderType, string itemMenu)
        {
            var popUpMenu = new PopupMenuHelper();
            popUpMenu.OrdersPopupMenuItem(application, itemMenu, orderType, symbol);
        }

        private void OpenItemPopupMenuInGroupOrderBuySell(TradingStationWindow
            application, string symbol, OrderGroups orderType, string itemMenu,
            BuySellEnum buySell)
        {
            var popUpMenu = new PopupMenuHelper();
            popUpMenu.OrdersPopupMenuItemSymbolAndBuySell(application, itemMenu, 
                orderType, symbol, buySell);
        }
    }
}

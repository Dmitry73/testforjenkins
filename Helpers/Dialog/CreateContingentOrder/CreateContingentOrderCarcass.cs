﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests.CCO
{
    class CreateContingentOrderDialogCarcass : IDisposable
    {
        #region CONST
        protected const string ORDER_TYPE = "Order Type:";

        protected const int OK_BUTTON_ID = 1;
        protected const int CANCEL_BUTTON_ID = 2;
        protected const int ACCOUNT_COMBOBOX_ID = 49301;
        protected const int ORDER_TYPE_ID = 55061;
        protected const int TIME_IN_FORCE_ID = 47106;  
        protected const string GTC_TIME_IN_FORCE = "GTC";
        protected const string GTD_TIME_IN_FORCE = "GTD";
        protected const int GTD_TIME_PIKER_ID = 55055;

        protected const int OTOCO_AND_IF_THEN_PRIMARY_CURRENCY_PAIR_ID = 2102;
        protected const int OTOCO_AND_IF_THEN_PRIMARY_RATE_ID = 2104;
        protected const int OTOCO_PRIMARY_TRAILING_OPTION_ID = 2120;
        protected const int OTOCO_PRIMARY_ORDER_TYPE_ID = 2109;
        protected const int OTOCO_RANGE_PRIMARY_ID = 2111;
        protected const int PRIMARY_SELL_BUY_OTOCO_ID = 2107;
        protected const int PRIMARY_OTOCO_AMOUNT_K_COMBOBOX_ID = 2114;

        protected const int PRIMARY_CURRENCY_PAIR_ID = 2202;
        protected const int PRIMARY_SELL_BUY_ID = 2207;
        protected const int PRIMARY_RATE_ID = 2204;
        protected const int PRIMARY_AMOUNT_K_COMBOBOX_ID = 2214;
        protected const int TRAILING_PRIMARY_ID = 2229;
        protected const int TRAILING_PRIMARY_OPTION_ID = 2220;
        protected const int PRIMARY_ORDER_TYPE_ID = 2209;
        protected const int RANGE_PRIMARY_ID = 2211;

        protected const int SECONDARY_CURRENCY_PAIR_ID = 2302;
        protected const int SECONDARY_SELL_BUY_ID = 2307; 
        protected const int SECONDARY_RATE_ID = 2304;
        protected const int SECONDARY_AMOUNT_K_COMBOBOX_ID = 2314;
        protected const int TRAILING_SECONDARY_ID = 2329;
        protected const int TRAILING_SECONDARY_OPTION_ID = 2320;
        protected const int SECONDARY_ORDER_TYPE_ID = 2309;
        protected const int RANGE_SECONDARY_ID = 2311;

        protected const string CONTINGENT_ORDER_DIALOG_TITLE = "Create Contingent Order";
        protected const string ORDER_TYPE_OCO = "OCO";
        protected const string ORDER_TYPE_OTO = "OTO";
        protected const string ORDER_TYPE_IF_THEN = "If-Then";
        protected const string ORDER_TYPE_OTOCO = "OTOCO";
        protected const string ORDER_TYPE_ENTRY = "Entry";
        protected const string ORDER_TYPE_RANGE_ENTRY = "Range Entry";
        protected const string BUY_SELECT_COMBO_BOX = "Buy";
        protected const string SELL_SELECT_COMBO_BOX = "Sell";
        protected const string TRAILING_VALUE_NONE = "None";
        protected const string TRAILING_VALUE_FIXED = "Fixed";
        protected const string TRAILING_VALUE_DINAMIC = "Dynamic";
        protected const string OPEN_ADVANCED = "Advanced>>";
        protected const string OK_BUTTON = "OK";
        protected const string CANCEL_BUTTON = "Cancel";


        protected const int SYNC_RATES_CHECK_BOX_ID = 49310;
        protected const int IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID = 45734;
        protected const int STOP_PRIMARY_OTOCO_CHECK_BOX_ID = 2126;
        protected const int LIMIT_PRIMARY_OTOCO_CHECK_BOX_ID = 2135;
        protected const int STOP_PRIMARY_CHECK_BOX_ID = 2226;
        protected const int LIMIT_PRIMARY_CHECK_BOX_ID = 2235;
        protected const int STOP_SECONDARY_CHECK_BOX_ID = 2326;
        protected const int LIMIT_SECONDARY_CHECK_BOX_ID = 2335;
        protected const int IN_PIPS_CHECK_BOX_ID = 49403;

        protected const int STOP_EDIT_PRIMARY_ORDER_OTOCO_ID = 2123;
        protected const int STOP_EDIT_PRIMARY_ORDER_ID = 2223;
        protected const int STOP_EDIT_PRIMARY_ORDER_ID_BUTTON = 2226;
        protected const int STOP_EDIT_SECONDARY_ORDER_ID = 2323;
        protected const int STOP_EDIT_SECONDARY_ORDER_ID_BUTTON = 2326;

        protected const int LIMIT_EDIT_PRIMARY_ORDER_OTOCO_ID = 2132;
        protected const int LIMIT_EDIT_PRIMARY_ORDER_ID = 2232;
        protected const int LIMIT_EDIT_SECONDARY_ORDER_ID = 2332;


        protected const int LENGTH_RATE = 7;
        protected const double RATE_COEFFICENT = 0.02;

        protected const bool ON = true;
        protected const bool OFF = false;
        protected const string ADVANCED_BUTTON_OPEN = "Advanced>>";
        #endregion

        protected Dialog dialog;

        #region CheckBox
        protected void AllOnOffStopLimit(bool onOff, OrderGroups orderType)
        {
            if (orderType == OrderGroups.OTOCO)
            {
                PrimaryLimitOTOCOCheckBox(onOff);
                PrimaryStopOTOCOCheckBox(onOff);
            }
            if (orderType != OrderGroups.If_Then)
            {
                PrimaryLimitCheckBox(onOff);
                PrimaryStopCheckBox(onOff);
            }
            SecondaryLimitCheckBox(onOff);
            SecondaryStopCheckBox(onOff);
        }

        protected void AllOnOffStopLimitINPeggedEdit(double rateCoeff,
            OrderGroups orderType)
        {
            InPipsCheckBox(OFF);
            if (orderType == OrderGroups.OTOCO)
            {
                InstallOTOCOLimit(rateCoeff);
                InstallOTOCOStop(-rateCoeff);
            }
            if (orderType != OrderGroups.If_Then)
            {
                InstallPrimaryLimit(rateCoeff);
                InstallPrimaryStop(-rateCoeff);
            }
            InstallSecondaryLimit(-rateCoeff);
            InstallSecondaryStop(rateCoeff);
            InPipsCheckBox(ON);
        }

        private string onOffWord(bool onOff)
        {
            return onOff ? "ON" : "OFF";
        }

        protected void InPipsCheckBox(bool onOff)
        {
            CarcasCheckBox(IN_PIPS_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox In pips " + onOffWord(onOff));
        }

        protected void SecondaryOrdersInPipsCheckBox(bool onOff)
        {
            if (dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled)
            {
                CarcasCheckBox(IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID, onOff);
                TestContext.Out.WriteLine("\r\nCheckBox Secondary orders in pips "
                    + onOffWord(onOff));
            }
        }

        protected void SyncRatesCheckBox(bool onOff)
        {
            if (dialog[SYNC_RATES_CHECK_BOX_ID].Enabled)
            {
                CarcasCheckBox(SYNC_RATES_CHECK_BOX_ID, onOff);
                TestContext.Out.WriteLine("\r\nCheckBox Sync Rates " + onOffWord(onOff));
            }
        }

        protected void PrimaryLimitOTOCOCheckBox(bool onOff)
        {
            CarcasCheckBox(LIMIT_PRIMARY_OTOCO_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox Limit first OTOCO " + onOffWord(onOff));
        }

        protected void PrimaryStopOTOCOCheckBox(bool onOff)
        {
            CarcasCheckBox(STOP_PRIMARY_OTOCO_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox Stop first OTOCO " + onOffWord(onOff));
        }

        protected void PrimaryLimitCheckBox(bool onOff)
        {
            CarcasCheckBox(LIMIT_PRIMARY_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox Limit Primary " + onOffWord(onOff));
        }

        protected void PrimaryStopCheckBox(bool onOff)
        {
            CarcasCheckBox(STOP_PRIMARY_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox Stop Primary " + onOffWord(onOff));
        }

        protected void SecondaryLimitCheckBox(bool onOff)
        {
            CarcasCheckBox(LIMIT_SECONDARY_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox Limit Secondary " + onOffWord(onOff));
        }

        protected void SecondaryStopCheckBox(bool onOff)
        {
            CarcasCheckBox(STOP_SECONDARY_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox Stop Secondary " + onOffWord(onOff));
        }

        protected void CarcasCheckBox(int idCheckBox, bool onOff)
        {
            using (CheckBox carcas = (CheckBox)dialog[idCheckBox])
            {
                if (onOff == true)
                {
                    if (carcas.Checked == 0) { carcas.Click(); }
                }
                else
                {
                    if (carcas.Checked == 1) { carcas.Click(); }
                }
            }
        }

        #endregion

        #region return Value
        protected string ReturnOrderType()
        {
            return CarcasReturnValueComboBox(ORDER_TYPE_ID);
        }
        
        private string CarcasReturnValueComboBox(int Id)
        {
            using (ComboBox comboBox = (ComboBox)dialog[Id])
            {
                string returnValue = comboBox.Text;
                return returnValue;
            }
        }

        protected string ReturnFirstSymbol()
        {
            return CarcasReturnValueComboBox(OTOCO_AND_IF_THEN_PRIMARY_CURRENCY_PAIR_ID);
        }

        protected string ReturnPrimarySymbol()
        {
            return CarcasReturnValueComboBox(PRIMARY_CURRENCY_PAIR_ID);
        }

        protected string ReturnSecondarySymbol()
        {
            return CarcasReturnValueComboBox(SECONDARY_CURRENCY_PAIR_ID);
        }

        protected string ReturnStopPrimaryOTOCO()
        {
            return CarcasReturnValueFromEdit(STOP_EDIT_PRIMARY_ORDER_OTOCO_ID);
        }
        
        protected string ReturnLimitPrimaryOTOCO()
        {
            return CarcasReturnValueFromEdit(LIMIT_EDIT_PRIMARY_ORDER_OTOCO_ID);
        }

        protected string ReturnStopPrimary()
        {
            return CarcasReturnValueFromEdit(STOP_EDIT_PRIMARY_ORDER_ID);
        }

        protected string ReturnLimitPrimary()
        {
            return CarcasReturnValueFromEdit(LIMIT_EDIT_PRIMARY_ORDER_ID);
        }

        protected string ReturnStopSecondary()
        {
            return CarcasReturnValueFromEdit(STOP_EDIT_SECONDARY_ORDER_ID);
        }

        protected string ReturnLimitSecondary()
        {
            return CarcasReturnValueFromEdit(LIMIT_EDIT_SECONDARY_ORDER_ID);
        }

        protected string ReturnValuePrimaryRateOTOCOIfThenOCO()
        {
            return CarcasReturnValueFromEdit(OTOCO_AND_IF_THEN_PRIMARY_RATE_ID);
        }

        protected string ReturnValuePrimaryRate()
        {
            return CarcasReturnValueFromEdit(PRIMARY_RATE_ID);
        }

        protected string ReturnValueSecondaryRate()
        {
            return CarcasReturnValueFromEdit(SECONDARY_RATE_ID);
        }

        protected string CarcasReturnValueFromEdit(int ID)
        {
            using (EditBox editRate = (EditBox)dialog[ID])
            {
                string returnvalue = editRate.Text;
                return returnvalue;
            }
        }

        protected bool ReturnSecondaryInPipsCheckBox()
        {
            using (CheckBox inPips = (CheckBox)dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID])
            {
                return (inPips.Checked == 1);
            }
        }

        protected bool ReturnInPipsOnOffCheckBox()
        {
            using (CheckBox inPips = (CheckBox)dialog[IN_PIPS_CHECK_BOX_ID])
            {
                return (inPips.Checked == 1);
            }
        }
        #endregion

        #region Install Value
        protected double InstallPrimaryRateOTOCOIfThenOCOEdit(double rateCoefficient)
        {
            double installValue =  CarcasInstallValueNonPegged(rateCoefficient, 
                OTOCO_AND_IF_THEN_PRIMARY_RATE_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary first rate for OTOCO/If-Then OCO " 
                + installValue);
            return installValue;
        }

        protected double InstallPrimaryRateEdit(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient,
                PRIMARY_RATE_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary rate " + + installValue);
            return installValue;
        }

        protected double InstallSecondaryRateEdit(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient, 
                SECONDARY_RATE_ID);
            TestContext.Out.WriteLine("\r\nInstall Secondary rate " + +installValue);
            return installValue;
        }


        protected void InstallOTOCOLimitPegged(double rate)
        {
            CarcasInstallStopLimitInPegged(rate, LIMIT_EDIT_PRIMARY_ORDER_OTOCO_ID);
            TestContext.Out.WriteLine(
                "\r\nInstall Primary first Limit PEGGED for OTOCO " + rate);
        }

        protected void InstallOTOCOStopPegged(double rate)
        {
            CarcasInstallStopLimitInPegged(rate, STOP_EDIT_PRIMARY_ORDER_OTOCO_ID);
            TestContext.Out.WriteLine
                ("\r\nInstall Primary first Stop PEGGED for OTOCO " + rate);
        }

        protected void InstallPrimaryLimitPegged(double rate)
        {
            CarcasInstallStopLimitInPegged(rate, LIMIT_EDIT_PRIMARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary Limit PEGGED " + rate);
        }

        protected void InstallPrimaryStopPegged(double rate)
        {
            CarcasInstallStopLimitInPegged(rate, STOP_EDIT_PRIMARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary Stop PEGGED " + rate);
        }

        protected void InstallSecondaryLimitPegged(double rate)
        {
            CarcasInstallStopLimitInPegged(rate, LIMIT_EDIT_SECONDARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Secondary Limit PEGGED " + rate);
        }

        protected void InstallSecondaryStopPegged(double rate)
        {
            CarcasInstallStopLimitInPegged(rate, STOP_EDIT_SECONDARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary Stop PEGGED " + rate);
        }

        protected void CarcasInstallStopLimitInPegged(double rate, int ID)
        {
            using (EditBox editRate = (EditBox)dialog[ID])
            {
                editRate.Text = rate.ToString();
            }
        }


        protected double CarcasInstallValueNonPegged (double rateCoefficient, int ID)
        {
            using (EditBox editRate = (EditBox)dialog[ID])
            {
                int count = editRate.Text.Length;
                double installValue = double.Parse(editRate.Text) +
                    double.Parse(editRate.Text) * rateCoefficient;
                installValue = MathHelper.RoundingDependingOnSignsAfterComma(
                    installValue);
                string installString = installValue.ToString();
                while (installString.Length < count)
                {
                    installString = installString + "0";
                }
                string recordValue = installString.Remove(count - 1, 7 - count);
                while (recordValue.Length < count)
                {
                    recordValue = recordValue + "0";
                }
                editRate.Text = recordValue;
                return double.Parse(recordValue);
            }            
        }

        protected double InstallOTOCOStop (double rateCoefficient)
        {
            double installValue =  CarcasInstallValueNonPegged(rateCoefficient, 
                STOP_EDIT_PRIMARY_ORDER_OTOCO_ID);
            TestContext.Out.WriteLine("\r\nInstall OTOCO first Stop " + installValue);
            return installValue;
        }

        protected double InstallOTOCOLimit(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient,
                LIMIT_EDIT_PRIMARY_ORDER_OTOCO_ID);
            TestContext.Out.WriteLine("\r\nInstall OTOCO first Limit " + installValue);
            return installValue;
        }

        protected double InstallPrimaryStop(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient,
                STOP_EDIT_PRIMARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary Stop " + installValue);
            return installValue;
        }

        protected double InstallPrimaryLimit(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient, 
                LIMIT_EDIT_PRIMARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Primary Limit " + installValue);
            return installValue;
        }

        protected double InstallSecondaryStop(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient, 
                STOP_EDIT_SECONDARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Secondary Stop " + installValue);
            return installValue;
        }

        protected double InstallSecondaryLimit(double rateCoefficient)
        {
            double installValue = CarcasInstallValueNonPegged(rateCoefficient,
                LIMIT_EDIT_SECONDARY_ORDER_ID);
            TestContext.Out.WriteLine("\r\nInstall Secondary Limit " + installValue);
            return installValue;
        }

        protected void InstallAllStopLimitNonPegged(double CoeffStopLimit,
            OrderGroups orderType, BuySellEnum buySellOTOCO, BuySellEnum
            buySellPrimary, BuySellEnum buySellSecondary)
        {
            if (orderType == OrderGroups.OTOCO)
            {
                if (BuySellEnum.BUY == buySellOTOCO)
                {
                    InstallOTOCOStop(-CoeffStopLimit);
                    InstallOTOCOLimit(CoeffStopLimit);
                }
                else
                {
                    InstallOTOCOStop(CoeffStopLimit);
                    InstallOTOCOLimit(-CoeffStopLimit);
                }
            }
            if (orderType != OrderGroups.If_Then)
            {
                if (BuySellEnum.BUY == buySellPrimary)
                {
                    InstallPrimaryLimit(CoeffStopLimit);
                    InstallPrimaryStop(-CoeffStopLimit);
                }
                else
                {
                    InstallPrimaryLimit(-CoeffStopLimit);
                    InstallPrimaryStop(CoeffStopLimit);
                }
            }
            if (BuySellEnum.BUY == buySellSecondary)
            {
                InstallSecondaryLimit(CoeffStopLimit);
                InstallSecondaryStop(-CoeffStopLimit);
            }
            else
            {
                InstallSecondaryLimit(-CoeffStopLimit);
                InstallSecondaryStop(CoeffStopLimit);
            }
        }

        protected void InstallPrimaryRatePegged(double rate)
        {
            using (EditBox editRange = (EditBox)dialog[PRIMARY_RATE_ID])
            {
                rate = Math.Round(rate, 1);
                editRange.Text = Convert.ToString(rate);
            }
        }

        protected void InstallSecondaryRatePegged(double rate)
        {
            using (EditBox editRange = (EditBox)dialog[SECONDARY_RATE_ID])
            {
                rate = Math.Round(rate, 1);
                rate = rate * 10;//HACK Creates a value less than 10 given
                editRange.Text = Convert.ToString(rate);
                TestContext.Out.WriteLine("\r\nInstall Secondary rate Pegged - " + rate);
            }
        }

        protected void InstallSecondaryRatePeggedString(string rate)
        {
            using (EditBox editRange = (EditBox)dialog[SECONDARY_RATE_ID])
            {
                editRange.Text = rate;
                TestContext.Out.WriteLine("\r\nInstall Secondary rate Pegged - " + rate);
            }
        }

        protected void InstallOTOCOPrimaryRange (double range)
        {
            using (EditBox editRange = (EditBox)dialog[OTOCO_RANGE_PRIMARY_ID])
            {
                range = Math.Round(range, 1);
                editRange.Text = Convert.ToString(range);
                TestContext.Out.WriteLine("\r\nInsatall Primary OTOCO Range - " + range);
            }
        }

        protected void InstallPrimaryRange(double range)
        {
            using (EditBox editRange = (EditBox)dialog[RANGE_PRIMARY_ID])
            {
                range = Math.Round(range, 1);
                editRange.Text = Convert.ToString(range);
                TestContext.Out.WriteLine("\r\nInsatall Primary Range - " + range);
            }
        }

        protected void InstallSecondaryRange(double range)
        {
            using (EditBox editRange = (EditBox)dialog[RANGE_SECONDARY_ID])
            {
                range = Math.Round(range, 1);
                editRange.Text = Convert.ToString(range);
                TestContext.Out.WriteLine("\r\nInsatall Secondary Range - " + range);
            }
        }
        #endregion

        #region ComboBox
        protected void SelectOTOCOIfThenOCOFirstSymbol(string symbol)
        {
            CarcasComboBox(OTOCO_AND_IF_THEN_PRIMARY_CURRENCY_PAIR_ID, symbol);
            TestContext.Out.WriteLine("\r\nFirst symbol for OTOCO/If-Then OCO select - " 
                + symbol);
        }

        protected void SelectTimeInForce(string timeInForce)
        {
            CarcasComboBox(TIME_IN_FORCE_ID, timeInForce);
            TestContext.Out.WriteLine("\r\nTime In Force selected - " + timeInForce);
        }

        protected void SelectOTOCOPrimaryOrderType(string orderType)
        {
            CarcasComboBox(OTOCO_PRIMARY_ORDER_TYPE_ID, orderType);
            TestContext.Out.WriteLine(
                "\r\nOrder Type for first currancy pair OTOCO selected - " + orderType);
        }

        protected void SelectPrimaryOrderType(string orderType)
        {
            CarcasComboBox(PRIMARY_ORDER_TYPE_ID, orderType);
            TestContext.Out.WriteLine("\r\nPrimary Order Type selected - " + orderType);
        }

        protected void SelectSecondaryOrderType(string orderType)
        {
            CarcasComboBox(SECONDARY_ORDER_TYPE_ID, orderType);
        }

        protected void TrailingOTOCOOptionPrimary(string trailing)
        {
            CarcasComboBox(OTOCO_PRIMARY_TRAILING_OPTION_ID, trailing);
            TestContext.Out.WriteLine(
                "\r\nTrailing for first currancy pair for OTOCO selected - " + trailing);
        }

        protected void TrailingOptionPrimary(string trailing)
        {
            CarcasComboBox(TRAILING_PRIMARY_OPTION_ID, trailing);
            TestContext.Out.WriteLine("\r\nPrimary Trailing selected - " + trailing);
        }

        protected void TrailingOptionSecondary(string trailing)
        {
            CarcasComboBox(TRAILING_SECONDARY_OPTION_ID, trailing);
            TestContext.Out.WriteLine("\r\nSecondary Trailing selected - " + trailing);
        }

        protected void SelectPrimarySymbol(string primarySymbol)
        {
            CarcasComboBox(PRIMARY_CURRENCY_PAIR_ID, primarySymbol);
            TestContext.Out.WriteLine("\r\nPrimary symbol selected - " + primarySymbol);
        }

        protected void SelectSecondarySymbol(string secondarySymbol)
        {
            CarcasComboBox(SECONDARY_CURRENCY_PAIR_ID, secondarySymbol);
            TestContext.Out.WriteLine("\r\nSecondary symbol selected - " + secondarySymbol);
        }

        protected void SelectOrderType(string orderType)
        {
            CarcasComboBox(ORDER_TYPE_ID, orderType);
            TestContext.Out.WriteLine("\r\nOrder Type selected - " + orderType);
        }

        protected void SelectOrderTypeEnum(OrderGroups orderType)
        {
            CarcasComboBox(ORDER_TYPE_ID, OrderGroupDictionary.orderGroupNames[orderType]);
            TestContext.Out.WriteLine("\r\nOrder Type selected - " + 
                OrderGroupDictionary.orderGroupNames[orderType]);
        }

        protected void EditAmountOTOCO(string amount)
        {
            CarcasComboBox(PRIMARY_OTOCO_AMOUNT_K_COMBOBOX_ID, amount);
            TestContext.Out.WriteLine("\r\nEdit OTOCO first amount  - " + amount);
        }

        protected void EditAmountPrimary(string amount)
        {
            CarcasComboBox(PRIMARY_AMOUNT_K_COMBOBOX_ID, amount);
            TestContext.Out.WriteLine("\r\nEdit primary amount  - " + amount);
        }

        protected void EditAmountSecondary(string amount)
        {
            CarcasComboBox(SECONDARY_AMOUNT_K_COMBOBOX_ID, amount);
            TestContext.Out.WriteLine("\r\nEdit secondary amount  - " + amount);
        }


        protected void CarcasComboBox(int idComboBox, string orderType)
        {
            using (ComboBox select = (ComboBox)dialog[idComboBox])
            {
                select.SelectItem(orderType);
            }
        }

        private string BuySellWord(BuySellEnum buySell)
        {
            return (buySell == BuySellEnum.BUY) ? "BUY" : "SELL";
        }

        protected void BuySellOTOCOPrimary(BuySellEnum buySellPrimary)
        {
            using (ComboBox buySellComboBox = (ComboBox)dialog[PRIMARY_SELL_BUY_OTOCO_ID])
            {
                if (buySellPrimary == BuySellEnum.BUY)
                {
                    buySellComboBox.SelectItem(BUY_SELECT_COMBO_BOX);
                }
                else
                {
                    buySellComboBox.SelectItem(SELL_SELECT_COMBO_BOX);
                }
                TestContext.Out.WriteLine("\r\nBuySell for OTOCO selected - " +
                    BuySellWord(buySellPrimary));
            }
        }

        protected void BuySellPrimary(BuySellEnum buySellPrimary)
        {
            using (ComboBox buySellComboBox = (ComboBox)dialog[PRIMARY_SELL_BUY_ID])
            {
                if (buySellPrimary == BuySellEnum.BUY)
                {
                    buySellComboBox.SelectItem(BUY_SELECT_COMBO_BOX);
                }
                else
                {
                    buySellComboBox.SelectItem(SELL_SELECT_COMBO_BOX);
                }
                TestContext.Out.WriteLine("\r\nBuySell Primary selected - " +
                    BuySellWord(buySellPrimary));
            }
        }

        protected void BuySellSecondary(BuySellEnum buySellSecondary)
        {
            using (ComboBox buySellComboBox = (ComboBox)dialog[SECONDARY_SELL_BUY_ID])
            {
                if (buySellSecondary == BuySellEnum.BUY)
                {
                    buySellComboBox.SelectItem(BUY_SELECT_COMBO_BOX);
                }
                else
                {
                    buySellComboBox.SelectItem(SELL_SELECT_COMBO_BOX);
                }
                TestContext.Out.WriteLine("\r\nBuySell Secondary selected - " +
                    BuySellWord(buySellSecondary));
            }
        }
        #endregion

        #region DataTimePicker
        protected string DateTimeInstall(int plusDays)
        {
            using (DateTimePicker dateTime = (DateTimePicker)dialog[GTD_TIME_PIKER_ID])
            {
                DateTime installTime = DateTime.Now;
                dateTime.Value = installTime.AddDays(plusDays);
                string resultForCheck = dateTime.Text.Insert(6, "20"); //write 2017 instead
                TestContext.Out.WriteLine("\r\nDataTime install - " + resultForCheck);
                return resultForCheck;
            }
        }
        #endregion

        #region Secondary function
        protected void DefaultOption(OrderGroups orderType)
        {
            if (orderType == OrderGroups.OTOCO)
            {
                SelectOTOCOPrimaryOrderType(ORDER_TYPE_ENTRY);
                TrailingOTOCOOptionPrimary(TRAILING_VALUE_NONE);
            }
            AllOnOffStopLimit(OFF, orderType);
            SelectTimeInForce(GTC_TIME_IN_FORCE);
            if (orderType != OrderGroups.If_Then)
            {
                SelectPrimaryOrderType(ORDER_TYPE_ENTRY);
                TrailingOptionPrimary(TRAILING_VALUE_NONE);
            }
            SelectSecondaryOrderType(ORDER_TYPE_ENTRY);
            TrailingOptionSecondary(TRAILING_VALUE_NONE);
            if (dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled)
            {
                if (ReturnSecondaryInPipsCheckBox())
                {
                    SecondaryOrdersInPipsCheckBox(OFF);
                }
            }
        }

        protected void PressOK()
        {
            dialog[OK_BUTTON].Click();
            TestContext.Out.WriteLine(
                "\r\nPress button OK in dialog Create Contingent Order");
            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine(
                "\r\nPress button OK second time in dialog Create Contingent Order");
                dialog[OK_BUTTON].Click();
                Waiter.WaitingCondition secondChanceCondition = () => dialog.Exists == false;
                if (!Waiter.Wait(secondChanceCondition))
                {
                    dialog.SendClose();
                    TestContext.Out.WriteLine(
                        "\r\nNot close dialog Create Contingent Order");
                    Assert.Fail();
                }
                else
                {
                    TestContext.Out.WriteLine("\r\nA dialog Create Contingent Order Close");
                }
            }
            else
            {
                TestContext.Out.WriteLine("\r\nA dialog Create Contingent Order Close");
            }
        }

        public void PressCancel()
        {
            dialog[CANCEL_BUTTON].Click();
            TestContext.Out.WriteLine(
                "\r\nPress button Cancel in dialog Create Contingent Order");
            Waiter.WaitingCondition condition = () => { return dialog.Exists == false; };
            if (!Waiter.Wait(condition))
            {
                dialog[CANCEL_BUTTON].Click();
                TestContext.Out.WriteLine(
                "\r\nPress button Cancel second time in dialog Create Contingent Order");
                Waiter.WaitingCondition conditionSecondChance = () => 
                    dialog.Exists == false;
                if (!Waiter.Wait(conditionSecondChance))
                {
                    dialog.SendClose();
                    TestContext.Out.WriteLine(
                        "\r\nNot close dialog Create Contingent Order");
                    Assert.Fail();
                }
                else
                {
                    TestContext.Out.WriteLine("\r\nA dialog Create Contingent Order Close");
                }
            }
            else
            {
                TestContext.Out.WriteLine("\r\nA dialog Create Contingent Order Close");
            }
        }

        protected void GeneralOCOInstall(string primarySymbol, string secondarySymbol,
            BuySellEnum buySellPrimary, BuySellEnum buySellSecondary)
        {
            SelectOrderType(ORDER_TYPE_OCO);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
        }

        protected bool DeterminationOTOCOIfThenOCO(OrderGroups orderType)
        {
            if ((orderType == OrderGroups.OTOCO) || (orderType == OrderGroups.If_Then_OCO))
            {
                return true;
            }
            return false;
        }

        protected void OpenDialogCreateContingent(TradingStationWindow application)
        {
            var aplicationMenu = new ApplicationMenuHelper();
            aplicationMenu.PressOpenCreateContingentOrder(application);
        }
        #endregion

        #region IDisposable 
        protected bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

﻿using System;
using gehtsoft.tscontroller;
using TSAutotests.CCO;
using NUnit.Framework;


namespace TSAutotests
{
    class CreateContingentOrder : CreateContingentOrderDialogCarcass 
    {
        public CreateContingentOrder(TradingStationWindow application)
        {
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressOpenCreateContingentOrder(application);
            Waiter.WaitingCondition condition = () => (dialog =
            application.GetDialog(CONTINGENT_ORDER_DIALOG_TITLE)) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found window Create Contingent Order");
            }
            if (dialog[ADVANCED_BUTTON_OPEN] != null)
            {
                if (dialog[ADVANCED_BUTTON_OPEN].Enabled)
                {
                    dialog[ADVANCED_BUTTON_OPEN].Click();
                }
            }
            TestContext.Out.WriteLine("\r\nWindow Create Contingent Order found");
        }

        public void CreateDefaultOrderOTOOCOIfThen(OrderGroups orderType, 
            string primarySymbol, string secondarySymbol, BuySellEnum buySellPrimary,
            BuySellEnum buySellSecondary, double primaryRateCoeff, double
            secondaryRateCoeff, bool onOffStopLimit, bool onOffInPip, 
            double rateCoeffLimit = 0.01)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            if (orderType != OrderGroups.If_Then)
            {
                BuySellPrimary(buySellPrimary);
            }
            BuySellSecondary(buySellSecondary);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);

            AllOnOffStopLimit(onOffStopLimit, orderType);

            if (dialog[IN_PIPS_CHECK_BOX_ID].Enabled)
            {
                InPipsCheckBox(onOffInPip);
                if (onOffInPip)
                {
                    Waiter.WaitingCondition condition = () =>
                        ReturnLimitSecondary().Length < LENGTH_RATE;
                    if (!Waiter.Wait(condition))
                    {
                        throw new Exception("Not Pegged Format Limit");
                    }
                }
                else
                {
                    Waiter.WaitingCondition condition = () =>
                       ReturnLimitSecondary().Length == LENGTH_RATE;
                    if (!Waiter.Wait(condition))
                    {
                        throw new Exception("Pegged Format Limit");
                    }
                }

                if ((onOffInPip == false) && (onOffStopLimit))
                {
                    InstallAllStopLimitNonPegged(rateCoeffLimit, orderType,
                        BuySellEnum.BUY, buySellPrimary, buySellSecondary);
                }
            }
            PressOK();
        }

        public void CreateOrderOTOOCOIfThen(OrderGroups orderType, string
            primarySymbol, string secondarySymbol, BuySellEnum buySellPrimary,
            BuySellEnum buySellSecondary, double primaryRateCoeff, double
            secondaryRateCoeff, bool onOffStopLimit, bool onOffInPip, double 
            rateCoeffLimit = 0.05)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            if (orderType != OrderGroups.If_Then)
            {
                BuySellPrimary(buySellPrimary);
            }
            BuySellSecondary(buySellSecondary);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);

            AllOnOffStopLimit(onOffStopLimit, orderType);

            if (dialog[IN_PIPS_CHECK_BOX_ID].Enabled)
            {
                InPipsCheckBox(onOffInPip);
                if (onOffInPip)
                {
                    Waiter.WaitingCondition condition = () =>
                        ReturnLimitSecondary().Length < LENGTH_RATE;
                    if (!Waiter.Wait(condition))
                    { throw new Exception("Not Pegged Format Limit"); }
                }
                else
                {
                    Waiter.WaitingCondition condition = () =>
                       ReturnLimitSecondary().Length == LENGTH_RATE;
                    if (!Waiter.Wait(condition))
                    { throw new Exception("Pegged Format Limit"); }
                }

                if ((onOffInPip == false) && (onOffStopLimit))
                {
                    InstallAllStopLimitNonPegged(rateCoeffLimit, orderType, 
                        BuySellEnum.BUY, buySellPrimary, buySellSecondary);
                }
            }
            PressOK();
        }

        public void SecondaryInPipTwoDecimalPlaces(OrderGroups orderType, string
            secondarySymbol, string secondaryRateInPip)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                InstallPrimaryRateOTOCOIfThenOCOEdit(RATE_COEFFICENT);
            }
            else
            {
                InstallPrimaryRateEdit(RATE_COEFFICENT);
            }
            SecondaryOrdersInPipsCheckBox(ON);
            SelectSecondarySymbol(secondarySymbol);
            SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            BuySellSecondary(BuySellEnum.SELL);
            Waiter.WaitingCondition condition = () =>
                ReturnLimitSecondary().Length < LENGTH_RATE;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not Pegged Format Limit");

            }
            InstallSecondaryRatePeggedString(secondaryRateInPip);
            PressOK();
        }

        public void CreateDefaultOrderOTOCOIfThenOCO(OrderGroups orderType, 
            string firstPrimarySymbol, string primarySymbol,
            string secondarySymbol, BuySellEnum buySellFirstOTOCO, BuySellEnum 
            buySellPrimary, BuySellEnum buySellSecondary, double 
            firstPrimaryCoeff, double primaryRateCoeff, double secondaryRateCoeff, 
            bool onOffStopLimit, bool onOffInPip)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            InstallPrimaryRateOTOCOIfThenOCOEdit(firstPrimaryCoeff);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (orderType != OrderGroups.If_Then_OCO)
            {
                BuySellOTOCOPrimary(buySellFirstOTOCO);
            }
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            if (dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled)
            {
                SecondaryOrdersInPipsCheckBox(OFF);
            }
            AllOnOffStopLimit(onOffStopLimit, orderType);
            InPipsCheckBox(onOffInPip);

            if (onOffInPip)
            {
                Waiter.WaitingCondition condition = () =>
                    (ReturnLimitSecondary().Length < LENGTH_RATE) &&
                    (ReturnLimitPrimary().Length < LENGTH_RATE);
                if (!Waiter.Wait(condition))
                {
                    throw new Exception("Not Pegged Format Limit");
                }
            }
            else
            {
                Waiter.WaitingCondition condition = () =>
                   (ReturnLimitSecondary().Length == LENGTH_RATE) &&
                   (ReturnLimitPrimary().Length == LENGTH_RATE);
                if (!Waiter.Wait(condition))
                { throw new Exception("Pegged Format Limit"); }
            }

            if ((onOffStopLimit) && (!onOffInPip))
            {
                InstallAllStopLimitNonPegged(0.01, orderType, buySellFirstOTOCO,
                    buySellPrimary, buySellSecondary);
            }

            PressOK();
        }

        public void CreateOrderOTOOCOIfThenSecondaryInPip(OrderGroups orderType, 
            string primarySymbol, string secondarySymbol, BuySellEnum
            buySellPrimary, BuySellEnum buySellSecondary, double primaryRateCoeff,
            double secondaryRateCoeff, bool onOffSecondaryInPip)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (orderType != OrderGroups.If_Then)
            {
                BuySellPrimary(buySellPrimary);
            }
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(onOffSecondaryInPip);
            PressOK();
        }

        public void CreateOrderOTOOCOIfThenSecondaryInPipStopLimit(OrderGroups 
            orderType, string primarySymbol, string secondarySymbol, BuySellEnum
            buySellPrimary, BuySellEnum buySellSecondary, double primaryRateCoeff, 
            double secondaryRateCoeff, bool onOffSecondaryInPip, bool 
            onOffStopLimit)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (orderType != OrderGroups.If_Then)
            {
                BuySellPrimary(buySellPrimary);
            }
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(onOffSecondaryInPip);
            AllOnOffStopLimit(onOffStopLimit, orderType);
            PressOK();
        }

        public void CreateOrderOTOCOIfThenOCOSecondaryInPip(OrderGroups orderType,
            string firstPrimarySymbol, string primarySymbol,
            string secondarySymbol, BuySellEnum buySellFirstPrimary,
            BuySellEnum buySellPrimary, BuySellEnum buySellSecondary, double
            primaryfirstRateCoeff, double primaryRateCoeff, double 
            secondaryRateCoeff, bool onOffSecondaryInPip)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            InstallPrimaryRateOTOCOIfThenOCOEdit(primaryfirstRateCoeff);
            if (orderType != OrderGroups.If_Then_OCO)
            {
                BuySellOTOCOPrimary(buySellFirstPrimary);
            }
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(onOffSecondaryInPip);

            PressOK();
        }

        public void CreateOrderOTOCOIfThenOCOSecondaryInPipStopLimit(OrderGroups 
           orderType, string firstPrimarySymbol, string primarySymbol,
           string secondarySymbol, BuySellEnum buySellFirstPrimary,
           BuySellEnum buySellPrimary, BuySellEnum buySellSecondary, double
           primaryfirstRateCoeff, double primaryRateCoeff, double
           secondaryRateCoeff, bool onOffSecondaryInPip, bool onOffStopLimit)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            InstallPrimaryRateOTOCOIfThenOCOEdit(primaryfirstRateCoeff);
            if (orderType != OrderGroups.If_Then_OCO)
            {
                BuySellOTOCOPrimary(buySellFirstPrimary);
            }
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(onOffSecondaryInPip);
            AllOnOffStopLimit(onOffStopLimit, orderType);
            PressOK();
        }

        public void CreateOrderOTOOCOIfThenRangeEntry(OrderGroups orderType, 
            string primarySymbol, string SecondarySymbol, BuySellEnum buySellPrimary,
            BuySellEnum buySellSecondary, double primaryRateCoeff, double
            secondaryRateCoeff, bool truePrimaryFalseSecondaryRange, 
            bool onOffSecondaryInPip)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            if (truePrimaryFalseSecondaryRange)
            {
                SelectPrimaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            }
            else
            {
                SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
                InstallSecondaryRange(10);
            }
            SelectSecondarySymbol(SecondarySymbol);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (orderType != OrderGroups.If_Then)
            {
                BuySellPrimary(buySellPrimary);
            }
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(onOffSecondaryInPip);

            PressOK();
        }

        public void CreateOrderOTOCOIfThenOCORangeEntry(OrderGroups orderType,
            string firstPrimarySymbol, string primarySymbol, string SecondarySymbol, 
            BuySellEnum OTOCOBuySell, BuySellEnum buySellPrimary, BuySellEnum 
            buySellSecondary, double firstPrimaryCoeff, double primaryRateCoeff, 
            double secondaryRateCoeff, bool truePrimaryFalseSecondaryRange, 
            bool onOffSecondaryInPip)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            if (truePrimaryFalseSecondaryRange)
            {
                SelectPrimaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            }
            else
            {
                SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
                InstallSecondaryRange(10);
            }
            SelectSecondarySymbol(SecondarySymbol);
            InstallPrimaryRateOTOCOIfThenOCOEdit(firstPrimaryCoeff);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (orderType != OrderGroups.If_Then_OCO)
            {
                BuySellOTOCOPrimary(OTOCOBuySell);
            }
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(onOffSecondaryInPip);

            PressOK();
        }

        public void CreateOTOOCOIfThenPrimaryTrailing(OrderGroups orderType, 
            string primarySymbol, string SecondarySymbol, BuySellEnum 
            buySellPrimary, BuySellEnum buySellSecondary, double 
            primaryRateCoeff, double secondaryRateCoeff, bool 
            trailingPrimaryOrSecondary)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(SecondarySymbol);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (trailingPrimaryOrSecondary)
            {
                TrailingOptionPrimary(TRAILING_VALUE_FIXED);
            }
            else
            {
                TrailingOptionSecondary(TRAILING_VALUE_DINAMIC);
            }
            InstallPrimaryRateEdit(primaryRateCoeff);

            if (orderType != OrderGroups.If_Then)
            {
                BuySellPrimary(buySellPrimary);
            }
            BuySellSecondary(buySellSecondary);

            PressOK();
        }

        public void CreateOTOCOIfThenOCOPrimaryTrailing(OrderGroups orderType, 
            string firstPrimarySymbol, string primarySymbol, string
            SecondarySymbol, BuySellEnum buySellOTOCO, BuySellEnum buySellPrimary,
            BuySellEnum   buySellSecondary, double firstRateCoeff, double 
            primaryRateCoeff, double secondaryRateCoeff, 
            bool trailingPrimaryOrSecondary)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(SecondarySymbol);
            if (orderType != OrderGroups.If_Then_OCO)
            {
                BuySellOTOCOPrimary(buySellOTOCO);
            }
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            InstallPrimaryRateOTOCOIfThenOCOEdit(firstRateCoeff);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            if (trailingPrimaryOrSecondary)
            {
                TrailingOTOCOOptionPrimary(TRAILING_VALUE_FIXED);
            }
            else
            {
                TrailingOptionSecondary(TRAILING_VALUE_DINAMIC);
            }
            PressOK();
        }

        public void CreateDefaultOrderOTOCOIfThenOCO(OrderGroups orderType, 
            string firstPrimarySymbol, string primarySymbol,
            string secondarySymbol, BuySellEnum buySellFirstPrimary,
            BuySellEnum buySellPrimary, BuySellEnum buySellSecondary, double
            primaryfirstRateCoeff, double primaryRateCoeff, double secondaryRateCoeff)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            if (orderType != OrderGroups.If_Then_OCO)
            {
                BuySellOTOCOPrimary(buySellFirstPrimary);
            }
            InstallPrimaryRateOTOCOIfThenOCOEdit(primaryfirstRateCoeff);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);

            PressOK();
        }


    }
}

﻿using System;
using gehtsoft.tscontroller;
using TSAutotests.CCO;
using System.Threading;
using NUnit.Framework;

namespace TSAutotests
{
    class ContingentOrderTestDialog : CreateContingentOrderDialogCarcass
    {
        public ContingentOrderTestDialog(TradingStationWindow application)
        {
            Waiter.WaitingCondition condition = () => (dialog =
            application.GetDialog(CONTINGENT_ORDER_DIALOG_TITLE)) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found window Create Contingent Order");
            }
            if (dialog[ADVANCED_BUTTON_OPEN] != null)
            {
                if (dialog[ADVANCED_BUTTON_OPEN].Enabled)
                {
                    dialog[ADVANCED_BUTTON_OPEN].Click();
                }
            }
            TestContext.Out.WriteLine("\r\nWindow Create Contingent Order found");
        }

        #region For Test Suits
        public bool ReplaceOrderOTOCOIfTHenOCO_In_OTOOCOIfThen(OrderGroups
            orderTypeFirst, OrderGroups changeOrderType, string firstSymbol,
            string primarySymbol, string secondarySymbol, BuySellEnum firstBuySell,
            BuySellEnum primaryBuySell, BuySellEnum secondaryBuySell, double
            firstCoeff, double primaryCoeff, double secondaryCoeff)
        {
            SelectOrderTypeEnum(orderTypeFirst);
            DefaultOption(orderTypeFirst);
            SelectOTOCOIfThenOCOFirstSymbol(firstSymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            if (OrderGroups.OTOCO == orderTypeFirst)
            {
                BuySellOTOCOPrimary(firstBuySell);
            }
            BuySellPrimary(primaryBuySell);
            BuySellSecondary(secondaryBuySell);
            string firstRate = InstallPrimaryRateOTOCOIfThenOCOEdit(firstCoeff).ToString();
            string primaryRate = InstallPrimaryRateEdit(primaryCoeff).ToString();
            string secondaryRate = InstallSecondaryRateEdit(secondaryCoeff).ToString();

            SelectOrderTypeEnum(changeOrderType);
            bool result = ReturnPrimarySymbol().Equals(firstSymbol) &&
                ReturnSecondarySymbol().Equals(primarySymbol) &&
                ReturnValuePrimaryRate().Equals(firstRate) &&
                ReturnValueSecondaryRate().Equals(primarySymbol);

            TestContext.Out.WriteLine("\r\nOrders shifted - " + (result ? "RIGHT" : "WRONG"));
            PressCancel();

            return result;
        }

        public bool AccuracyValuePeggedSecondary(OrderGroups orderType, string 
            doubleSecondarySymbol, string standartSecondarySymbol)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(ON);
            SelectSecondarySymbol(doubleSecondarySymbol);
            bool result = false;

            Waiter.WaitingCondition conditionTwoNumber = () =>
                  DecimalPlaces(ReturnValueSecondaryRate()) == 2;
            if (!Waiter.Wait(conditionTwoNumber))
            {
                throw new Exception("No two number after comma");
            }
            else
            {
                result = true;
                TestContext.Out.WriteLine("\r\nsecondary rate two number after comma");
            }
            SelectSecondarySymbol(standartSecondarySymbol);
            int iii = DecimalPlaces(ReturnValueSecondaryRate());
            Waiter.WaitingCondition conditionOneNumber = () =>
                DecimalPlaces(ReturnValueSecondaryRate()) == 1;
            if (!Waiter.Wait(conditionOneNumber))
            {
                throw new Exception("No one number after comma");
            }
            else
            {
                result = result && true;
                TestContext.Out.WriteLine("\r\nsecondary rate one number after comma");
            }
            PressCancel();

            return result;
        }

        private int DecimalPlaces(string value)
        {
            return value.Length - 1 - value.IndexOf(',');
        }

        public bool OCOOneSymbolOffSyncRates(string symbol, double primaryCoefficient,
            double secondaryCoefficient)
        {
            SelectOrderType(ORDER_TYPE_OCO);
            DefaultOption(OrderGroups.OCO);
            SyncRatesCheckBox(OFF);
            SelectPrimarySymbol(symbol);
            SelectSecondarySymbol(symbol);
            string rateForCheckPrimary = 
                InstallPrimaryRateEdit(primaryCoefficient).ToString();
            string rateForCheckSecondary = 
                InstallSecondaryRateEdit(secondaryCoefficient).ToString();
            string valuePrimaryNow = ReturnValuePrimaryRate().TrimEnd('0');
            string valueSecondaryNow = ReturnValueSecondaryRate().TrimEnd('0');
            PressCancel();
            bool result = rateForCheckPrimary.Equals(valuePrimaryNow) &&
                rateForCheckSecondary.Equals(valueSecondaryNow);
            if (result)
            {
                TestContext.Out.WriteLine("\r\nValue rate correct");
            }
            return result;
        }

        public bool OCOOneSymbolOnSyncRates(string symbol, double primaryCoefficient,
            BuySellEnum buySellPrimary, BuySellEnum buySellSecondary)
        {
            SelectOrderType(ORDER_TYPE_OCO);
            DefaultOption(OrderGroups.OCO);
            GeneralOCOInstall(symbol, symbol, buySellPrimary, buySellSecondary);
            SyncRatesCheckBox(ON);
            InstallPrimaryRateEdit(primaryCoefficient); //So that the value does not change when we take it
            double primaryRateBefore = double.Parse(ReturnValuePrimaryRate());
            double secondaryRateBefore = double.Parse(ReturnValueSecondaryRate());
            double editPrimaryValue = InstallPrimaryRateEdit(primaryCoefficient);
            double changedOn = editPrimaryValue - primaryRateBefore;
            double changeSeconadaryRate;
            if (buySellPrimary != buySellSecondary)
            {
                changeSeconadaryRate = secondaryRateBefore - changedOn;
            }
            else
            {
                changeSeconadaryRate = secondaryRateBefore + changedOn;
            }
            string compareValue = Math.Round(changeSeconadaryRate, 5).ToString();
            string compare = ReturnValueSecondaryRate().TrimEnd('0');
            bool result = compare.Equals(compareValue);
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine(
                    "\r\nChanging the value rate when checkbox Sync Rates ON - Correctly");
            }
            return result;
        }

        public bool DiffererntSymbolIndependence(string primarySymbol, string
            secondarySymbol, BuySellEnum buySellPrimary, BuySellEnum buySellSecondary,
            double primaryCoefficient, double secondaryCoefficient)
        {
            GeneralOCOInstall(primarySymbol, secondarySymbol, buySellPrimary, buySellSecondary);
            string editPrimaryValue = InstallPrimaryRateEdit
                (primaryCoefficient).ToString();
            string editSecondaryValue = InstallSecondaryRateEdit
                (secondaryCoefficient).ToString();
            bool result = ReturnValuePrimaryRate().TrimEnd('0').
                Equals(editPrimaryValue);
            InstallPrimaryRateEdit(primaryCoefficient);
            result = result && ReturnValueSecondaryRate().TrimEnd('0').
                Equals(editSecondaryValue) && !dialog[SYNC_RATES_CHECK_BOX_ID].Enabled;

            PressOK();
            if (result)
            {
                TestContext.Out.WriteLine("\r\nThe Sync Rates checkbox is unavailable, " +
                    "the entered Rate values for both orders are retained");
            }
            return result;
        }

        public bool CheckBoxInPipTrailing(string primarySymbol, string secondarySymbol)
        {
            SelectOrderType(ORDER_TYPE_OCO);
            TrailingOptionPrimary(TRAILING_VALUE_FIXED);

            Waiter.WaitingCondition condition = () => ReturnInPipsOnOffCheckBox();
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Check box In Pip OFF");
            }
            bool problem = false;
            bool result = !dialog[IN_PIPS_CHECK_BOX_ID].Enabled && ReturnInPipsOnOffCheckBox();
            if (result == false)
            {
                TestContext.Out.WriteLine("After exhibiting Trailing is available check box In Pips or it is off");
                problem = true;
            }
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled &&
                ReturnInPipsOnOffCheckBox();
            if ((result == false) && (problem == false))
            {
                TestContext.Out.WriteLine("After exhibiting Trailing is available check box In Pips or it is off");
                problem = true;
            }
            AllOnOffStopLimit(ON, OrderGroups.OCO);
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled &&
                ReturnInPipsOnOffCheckBox();
            if ((result == false) && (problem == false))
            {
                TestContext.Out.WriteLine("After turning on the check box stop / limit check box In Pips is available or it is off");
                problem = true;
            }
            AllOnOffStopLimit(OFF, OrderGroups.OCO);
            PressCancel();
            return result;
        }

        public bool DisableSecondaryOrderInPip(string primarySymbol, string
            secondarySymbol)
        {
            SelectOrderType(ORDER_TYPE_OCO);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            bool result = !dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled;
            PrimaryStopCheckBox(ON);
            SecondaryStopCheckBox(ON);
            SecondaryLimitCheckBox(ON);
            result = result && !dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled;
            TrailingOptionPrimary(TRAILING_VALUE_DINAMIC);
            TrailingOptionSecondary(TRAILING_VALUE_FIXED);
            result = result && !dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled;
            PressCancel();
            TestContext.Out.WriteLine(
                "\r\nThe Secondary orders in pips checkbox is always unavailable for OCO");

            return result;
        }

        public bool ErrorRateEntryRange(TradingStationWindow application,
            string primarySymbol, string secondarySymbol)
        {
            SelectOrderType(ORDER_TYPE_OCO);
            DefaultOption(OrderGroups.OCO);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(primarySymbol);
            SyncRatesCheckBox(OFF);
            SelectPrimaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            BuySellPrimary(BuySellEnum.BUY);
            BuySellSecondary(BuySellEnum.SELL);
            InstallPrimaryRateEdit(-0.1);
            InstallSecondaryRateEdit(0.1);
            dialog[OK_BUTTON_ID].Click();
            bool result;
            using (var errorDialog = new CCOErrorDialog(application))
            {
                result = errorDialog.OrderTypeReErrorRate();
            }
            DefaultOption(OrderGroups.OCO);
            PressCancel();
            return result;
        }

        public bool CreateGTD(TradingStationWindow application, int plusDays,
            string primarySymbol, string secondarySymbol, OrderGroups orderType)
        {

            SelectOrderTypeEnum(orderType);
            //DefaultOption(orderType);
            SelectTimeInForce(GTD_TIME_IN_FORCE);

            if (DeterminationOTOCOIfThenOCO(orderType))
            { SelectOTOCOIfThenOCOFirstSymbol(primarySymbol); }
            else
            { SelectPrimarySymbol(primarySymbol); }

            SelectSecondarySymbol(secondarySymbol);
            string dataAndTime = DateTimeInstall(plusDays);
            if (DeterminationOTOCOIfThenOCO(orderType))
            { InstallPrimaryRateOTOCOIfThenOCOEdit(0.01); }
            InstallPrimaryRateEdit(0.01);
            InstallSecondaryRateEdit(0.015);

            PressOK();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CheckDataOrder(dataAndTime, orderType,
                    primarySymbol, secondarySymbol);
            }
            return result;
        }

        public bool PriceChangeWithoutTouchingPrimary(TradingStationWindow application,
            string primarySymbol, string secondarySymbol, BuySellEnum buySellPrimary,
            OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            if (!DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectPrimarySymbol(primarySymbol);
                SelectSecondarySymbol(secondarySymbol);
                string primaryValue = ReturnValuePrimaryRate();

                return PrimaryChangeChangeCurrientPrice(application, primarySymbol,
                    buySellPrimary, orderType, primaryValue);
            }
            else
            {
                SelectOTOCOIfThenOCOFirstSymbol(primarySymbol);
                SelectPrimarySymbol(secondarySymbol);
                SelectSecondarySymbol(secondarySymbol);
                string primaryValue = ReturnValuePrimaryRateOTOCOIfThenOCO();
                return PrimaryChangeChangeCurrientPrice(application, primarySymbol,
                    buySellPrimary, orderType, primaryValue);
            }
        }

        public bool PriceNotChangeWhenItChangesRatePrimarySymbol(TradingStationWindow application,
                string primarySymbol, double rateCoefficient,
                BuySellEnum buySellPrimary, OrderGroups orderType)
        {
            bool result = false;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            if (!DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectPrimarySymbol(primarySymbol);
                InstallPrimaryRateEdit(rateCoefficient);
                result = primaryNotChangeWhenItChanges(application, primarySymbol,
                    buySellPrimary, ReturnValuePrimaryRate(), orderType);
            }
            else
            {
                SelectOTOCOIfThenOCOFirstSymbol(primarySymbol);
                InstallPrimaryRateOTOCOIfThenOCOEdit(rateCoefficient);

                result = primaryNotChangeWhenItChanges(application, primarySymbol,
                     buySellPrimary, ReturnValuePrimaryRateOTOCOIfThenOCO(), orderType);
            }
            PressCancel();
            return result;
        }

        public bool ErrorRateRangeEntrySecondaryInPip(TradingStationWindow
            application, OrderGroups orderType, string primarySymbol, string
            secondarySymbol, BuySellEnum buySellSecondary, double rateSecondaryCoeff)
        {
            bool result = false;
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(primarySymbol);
                InstallPrimaryRateOTOCOIfThenOCOEdit(0.1);
            }
            SelectPrimarySymbol(primarySymbol);
            InstallPrimaryRateEdit(0.1);

            SelectSecondarySymbol(secondarySymbol);
            BuySellSecondary(buySellSecondary);
            SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            InstallSecondaryRateEdit(rateSecondaryCoeff);
            SecondaryOrdersInPipsCheckBox(ON);
            dialog[OK_BUTTON_ID].Click();
            using (var errorDialog = new CCOErrorDialog(application))
            {
                result = errorDialog.RangeEntryInPipError();
            }
            PressCancel();
            return result;
        }

        public bool TrailingPrimaryChangeOrderType(OrderGroups firstOrderType,
            OrderGroups secondOrderType)
        {
            SelectOrderTypeEnum(firstOrderType);
            DefaultOption(firstOrderType);
            TrailingOptionPrimary(TRAILING_VALUE_FIXED);
            SelectOrderTypeEnum(secondOrderType);
            if (dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled)
            {
                TestContext.Out.WriteLine("\r\nCheck box Secondary in pips enabled");
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool PriceNotChangeWhenItChangesRateSecondarySymbol(TradingStationWindow
            application, string secondarySymbol, double rateCoefficient,
        BuySellEnum buySellSecondary, OrderGroups orderType)
        {
            DefaultOption(orderType);
            SelectOrderTypeEnum(orderType);
            SelectSecondarySymbol(secondarySymbol);
            InstallSecondaryRateEdit(rateCoefficient);
            string secondaryValue = ReturnValueSecondaryRate();
            return SecondaryPriceNotChange(application, secondarySymbol,
                buySellSecondary, secondaryValue);
        }

        public bool PriceChangeWithoutTouchingSecondary(TradingStationWindow application,
           string primarySymbol, string secondarySymbol, BuySellEnum buySellSecondary,
           OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            //SyncRatesCheckBox(OFF);
            bool result = ChangeSecondarySymbolWithoutTouching(application, primarySymbol,
                secondarySymbol, buySellSecondary);
            PressCancel();
            return result;
        }

        public bool DefaultRatePeggedSecondary(TradingStationWindow application,
            string secondarySymbol, BuySellEnum buySellSecondary, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);

            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(secondarySymbol);
            }
            SelectPrimarySymbol(secondarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            SecondaryOrdersInPipsCheckBox(ON);
            string secondaryValue = ReturnValueSecondaryRate();
            return SecondaryPriceNotChange(application, secondarySymbol,
                buySellSecondary, secondaryValue);
        }

        public bool PriceChangeSecondaryOnOffSecondaryOrdersInPipsAll
            (TradingStationWindow application, string primarySymbol,
            string secondarySymbol, BuySellEnum buySellSecondary, OrderGroups orderType)
        {
            DefaultOption(orderType);
            installOrderTypeOnOffSecondaryOrdersInPips(primarySymbol,
                secondarySymbol, buySellSecondary, orderType);
            bool result = ChangeSecondarySymbolWithoutTouching(application, primarySymbol,
                secondarySymbol, buySellSecondary);
            PressCancel();
            return result;
        }

        public bool ChangePrimarySyncSecondarySameSymbol(string genrealSymbol,
            double primaryCoefficient, double secondaryCoefficient, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            //SyncRatesCheckBox(OFF);
            DefaultOption(orderType);

            InstallSecondaryRateEdit(secondaryCoefficient);
            string compareValue = ReturnValueSecondaryRate();
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                InstallPrimaryRateOTOCOIfThenOCOEdit(primaryCoefficient);
            }
            else
            {
                InstallPrimaryRateEdit(primaryCoefficient);
            }
            string finalValue = ReturnValueSecondaryRate();
            bool result = compareValue.Equals(finalValue);
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine("\r\nChange primary does not affect secondary");
            }
            return result;
        }

        public bool DisableSyncRateChangeSymbolOTOIfThen(string generalSymbol,
            string changeSymbol, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(generalSymbol);
            SelectSecondarySymbol(generalSymbol);
            bool result = !dialog[SYNC_RATES_CHECK_BOX_ID].Enabled;
            SelectSecondarySymbol(changeSymbol);
            result = result && !dialog[SYNC_RATES_CHECK_BOX_ID].Enabled;
            SelectSecondarySymbol(generalSymbol);
            result = result && !dialog[SYNC_RATES_CHECK_BOX_ID].Enabled;
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine(
                "\r\nWhen changing above the check box Sync Rates is not available");
            }
            return result;
        }

        public bool CheckBoxOnAndDisbleInTrailingOTOOTOCOPrimary(string primaryOTOCOSymbol,
            string primarySymbol, string secondarySymbol, string changeSymbol,
            string trailingPrimary, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            if (OrderGroups.OTOCO == orderType)
            {
                SelectOTOCOIfThenOCOFirstSymbol(primaryOTOCOSymbol);
                TrailingOTOCOOptionPrimary(trailingPrimary);
            }
            else
            {
                TrailingOptionPrimary(trailingPrimary);
            }
            SelectSecondarySymbol(secondarySymbol);
            bool result = !dialog[IN_PIPS_CHECK_BOX_ID].Enabled && ReturnInPipsOnOffCheckBox();
            if (OrderGroups.OTOCO == orderType)
            {
                SelectOTOCOIfThenOCOFirstSymbol(changeSymbol);
            }
            else
            {
                SelectPrimarySymbol(changeSymbol);
            }
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled &&
                ReturnInPipsOnOffCheckBox();
            AllOnOffStopLimit(ON, orderType);
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled &&
               ReturnInPipsOnOffCheckBox();
            AllOnOffStopLimit(OFF, orderType);
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine(
                "\r\nCheck box In pip for all actions above is not available and enabled");
            }
            return result;
        }

        public bool CheckBoxOnAndDisbleInTrailingSecondary(string primaryOTOCOSymbol,
            string primarySymbol, string secondarySymbol, string changeSymbol,
            string trailingSecondary, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(primaryOTOCOSymbol);
            }
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            TrailingOptionSecondary(trailingSecondary);
            bool result = !dialog[IN_PIPS_CHECK_BOX_ID].Enabled && ReturnInPipsOnOffCheckBox();
            SelectSecondarySymbol(changeSymbol);
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled &&
               ReturnInPipsOnOffCheckBox();
            AllOnOffStopLimit(ON, orderType);
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled &&
               ReturnInPipsOnOffCheckBox();
            AllOnOffStopLimit(OFF, orderType);
            PressCancel();
            return result;
        }

        public bool OnSecondaryOrdersInPipStopLimitChangeSymbol(string secondarySymbol,
            string changeSymbol, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectSecondarySymbol(secondarySymbol);
            SecondaryOrdersInPipsCheckBox(ON);
            bool result = !dialog[IN_PIPS_CHECK_BOX_ID].Enabled && ReturnInPipsOnOffCheckBox();
            AllOnOffStopLimit(ON, orderType);
            result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled && ReturnInPipsOnOffCheckBox();
            SelectSecondarySymbol(changeSymbol);

            Waiter.WaitingCondition conditionClose = () => (ReturnStopSecondary().Length < LENGTH_RATE);
            if (!Waiter.Wait(conditionClose))
            {
                throw new Exception("value secondary stop not pegged");
            }

            if ((ReturnStopSecondary().Length < LENGTH_RATE) &&
               (ReturnLimitSecondary().Length < LENGTH_RATE))
            {
                if ((orderType != OrderGroups.If_Then) &&
                    (ReturnLimitPrimary().Length < LENGTH_RATE) &&
                    (ReturnStopPrimary().Length < LENGTH_RATE))
                {
                    result = result && !dialog[IN_PIPS_CHECK_BOX_ID].Enabled;
                }
                ReturnInPipsOnOffCheckBox();
            }
            else
            {
                PressCancel();
                throw new Exception("Stop / Limit is not in pips");
            }
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine(
                "\r\nvalue stop and limit pegged and check box In pip not available");
            }
            return result;
        }

        public bool OTOCheckTrailingChangeRateSameSymbol(string symbol, string trailing)
        {
            SelectOrderType(ORDER_TYPE_OTO);
            DefaultOption(OrderGroups.OTO);
            SelectPrimarySymbol(symbol);
            TrailingOptionPrimary(trailing);
            SelectSecondarySymbol(symbol);

            bool result = !dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled &&
                ReturnSecondaryInPipsCheckBox() &&
                (ReturnValueSecondaryRate().Length < LENGTH_RATE);
            DefaultOption(OrderGroups.OTO);
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine(
                "\r\nCheck box Secondary in pips On and not available? secondary rate - pegged");
            }
            return result;
        }

        public bool CheckDefaultStopLimitInPips(OrderGroups orderType,
            string symbol)
        {
            SelectOrderTypeEnum(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(symbol);
            }
            SelectPrimarySymbol(symbol);
            SelectSecondarySymbol(symbol);
            DefaultOption(orderType);
            AllOnOffStopLimit(ON, orderType);
            InPipsCheckBox(ON);
            Thread.Sleep(1000);
            bool result = true;

            if (orderType != OrderGroups.If_Then)
            {
                result = (double.Parse(ReturnLimitPrimary()) + 0.1 ) ==
                   ((double.Parse(ReturnStopPrimary()) - 0.1)*-1);
            }

            if (orderType == OrderGroups.OTOCO)
            {
                result = result && (double.Parse(ReturnLimitPrimaryOTOCO()) + 0.1) ==
                   ((double.Parse(ReturnStopPrimaryOTOCO()) - 0.1) * -1);
            }

            result = result && (double.Parse(ReturnLimitSecondary()) + 0.1) ==
                   ((double.Parse(ReturnStopSecondary()) - 0.1) * -1);


            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine(
                "\r\nThe stop / limit values are the same as the default values");
            }
            return result;
        }

        public bool OTOCheckTrailingChangeRateDiffSymbol(string primarySymbol,
            string secondarySymbol, string trailing)
        {
            SelectOrderType(ORDER_TYPE_OTO);
            DefaultOption(OrderGroups.OTO);
            SelectPrimarySymbol(primarySymbol);
            TrailingOptionPrimary(trailing);
            SelectSecondarySymbol(secondarySymbol);
            InstallSecondaryRateEdit(0.1);
            bool result = dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled;
            DefaultOption(OrderGroups.OTO);
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine("\r\nCheck box Secondary in pips enabled");
            }
            return result;
        }

        public bool OTOPeggedSecondaryOnSecondaryInPips()
        {
            SelectOrderType(ORDER_TYPE_OTO);
            DefaultOption(OrderGroups.OTO);
            TrailingOptionPrimary(TRAILING_VALUE_NONE);
            SecondaryOrdersInPipsCheckBox(ON);
            bool result = ReturnValueSecondaryRate().Length < LENGTH_RATE;
            PressCancel();
            if (result)
            {
                TestContext.Out.WriteLine("\r\nSecondary value pegged");
            }
            return result;
        }

        public bool OTOPeggedSecondaryOffSecondaryInPips()
        {
            SelectOrderType(ORDER_TYPE_OTO);
            DefaultOption(OrderGroups.OTO);
            TrailingOptionPrimary(TRAILING_VALUE_NONE);
            SecondaryOrdersInPipsCheckBox(OFF);
            bool result = ReturnValueSecondaryRate().Length == LENGTH_RATE;
            if (result)
            {
                TestContext.Out.WriteLine("\r\nSecondary value not pegged");
            }
            PressCancel();
            return result;
        }

        public bool OTOOTOCONonPeggedSecondaryStopLimit(OrderGroups orderType)
        {
            PreapirOTOOTOCONonPeggedSecondaryStopLimit(orderType, OFF);
            InPipsCheckBox(OFF);
            Thread.Sleep(500);
            bool result = (ReturnLimitSecondary().Length == LENGTH_RATE) &&
                (ReturnStopPrimary().Length == LENGTH_RATE);
            if (orderType == OrderGroups.OTOCO)
            {
                result = result && ReturnLimitPrimaryOTOCO().Length == LENGTH_RATE;
            }
            DefaultOption(orderType);
            if (result)
            {
                TestContext.Out.WriteLine("\r\nSecondary value stop/limit not pegged");
            }
            PressCancel();
            return result;
        }

        public bool OTOOTOCOPeggedSecondaryStopLimitInPip(OrderGroups orderType)
        {
            PreapirOTOOTOCONonPeggedSecondaryStopLimit(orderType, OFF);
            InPipsCheckBox(ON);
            bool result = (ReturnLimitSecondary().Length < LENGTH_RATE) &&
                (ReturnStopPrimary().Length < LENGTH_RATE);
            if (orderType == OrderGroups.OTOCO)
            {
                result = result && ReturnLimitPrimaryOTOCO().Length < LENGTH_RATE;
            }
            DefaultOption(orderType);
            if (result)
            {
                TestContext.Out.WriteLine("\r\nValue secondary stop/limit pegged");
            }
            PressCancel();
            return result;
        }

        public bool OTOOTOCOPeggedSecondaryStopLimitOnSecondaryInPips(
            OrderGroups orderType)
        {
            PreapirOTOOTOCONonPeggedSecondaryStopLimit(orderType, ON);
            bool result = (ReturnLimitSecondary().Length < LENGTH_RATE) &&
                (ReturnStopPrimary().Length < LENGTH_RATE);
            if (orderType == OrderGroups.OTOCO)
            {
                result = result && ReturnLimitPrimaryOTOCO().Length < LENGTH_RATE;
            }
            if (result)
            {
                TestContext.Out.WriteLine("\r\nValue secondary stop/limit pegged");
            }
            DefaultOption(orderType);
            PressCancel();
            return result;
        }

        public bool OTOOTOCOPeggedSecondaryWithStopLimitAbs(OrderGroups orderType)
        {
            PreapirOTOOTOCONonPeggedSecondaryStopLimit(orderType, ON);
            bool result = (ReturnLimitSecondary().Length < LENGTH_RATE) &&
                (ReturnStopPrimary().Length < LENGTH_RATE);
            if (orderType == OrderGroups.OTOCO)
            {
                result = result && ReturnLimitPrimaryOTOCO().Length < LENGTH_RATE;
            }
            result = result && ReturnInPipsOnOffCheckBox() &&
                !dialog[IN_PIPS_CHECK_BOX_ID].Enabled;
            if (result)
            {
                TestContext.Out.WriteLine("\r\nValue secondary stop/limit pegged");
            }
            DefaultOption(orderType);
            PressCancel();
            return result;
        }

        public bool PrimaryREErrorRate(TradingStationWindow application,
            BuySellEnum buySell, double range, OrderGroups orderType)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOPrimaryOrderType(ORDER_TYPE_RANGE_ENTRY);
                InstallOTOCOPrimaryRange(range);
                BuySellOTOCOPrimary(buySell);
                if (BuySellEnum.BUY == buySell)
                {
                    InstallPrimaryRateOTOCOIfThenOCOEdit(-0.1);
                }
                else
                {
                    InstallPrimaryRateOTOCOIfThenOCOEdit(0.1);
                }
            }
            else
            {
                SelectPrimaryOrderType(ORDER_TYPE_RANGE_ENTRY);
                InstallPrimaryRange(range);
                BuySellPrimary(buySell);
                if (BuySellEnum.BUY == buySell)
                {
                    InstallPrimaryRateEdit(-0.1);
                }
                else
                {
                    InstallPrimaryRateEdit(0.1);
                }
            }

            bool result = false;
            dialog[OK_BUTTON_ID].Click();
            using (var error = new CCOErrorDialog(application))
            {
                result = error.OrderTypeReErrorRate();
            }
            DefaultOption(orderType);
            PressCancel();

            return result;
        }

        public bool SecondaryREErrorRate(TradingStationWindow application,
            BuySellEnum buySell, double range, OrderGroups orderType, string
            primarySymbol, string secondarySymbol)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(OFF);
            SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            InstallSecondaryRange(range);
            if (orderType != OrderGroups.If_Then)
            {
                SelectPrimarySymbol(primarySymbol);
            }
            SelectSecondarySymbol(secondarySymbol);
            BuySellSecondary(buySell);
            if (BuySellEnum.BUY == buySell)
            {
                InstallSecondaryRateEdit(-0.1);
            }
            else
            {
                InstallSecondaryRateEdit(0.1);
            }

            bool result = false;
            dialog[OK_BUTTON_ID].Click();
            using (var error = new CCOErrorDialog(application))
            {
                result = error.OrderTypeReErrorRate();
            }
            DefaultOption(orderType);
            PressCancel();

            return result;
        }

        public bool SecondaryREErrorRatePegged(TradingStationWindow application,
            BuySellEnum buySell, double range, OrderGroups orderType)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(ON);
            SelectSecondaryOrderType(ORDER_TYPE_RANGE_ENTRY);
            InstallSecondaryRange(range);
            BuySellSecondary(buySell);
            if (BuySellEnum.BUY == buySell)
            {
                InstallSecondaryRatePegged(-10.3);
            }
            else
            {
                InstallSecondaryRatePegged(10.1);
            }

            bool result = false;
            dialog[OK_BUTTON_ID].Click();
            using (var error = new CCOErrorDialog(application))
            {
                result = error.OrderTypeReErrorRate();
            }
            DefaultOption(orderType);
            PressCancel();

            return result;
        }

        public bool PrimaryBuySellTypeInTabOrder(TradingStationWindow application,
            BuySellEnum buySell, double rateCoefficient, OrderGroups orderType,
            string primarySymbol, string secondarySymbol, string primaryOTOCOsymbol,
            string orderTypeCurrancyPair)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            //DefaultOption(orderType);

            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            InstallSecondaryRateEdit(rateCoefficient);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(primaryOTOCOsymbol);
                InstallPrimaryRateOTOCOIfThenOCOEdit(rateCoefficient);
            }
            if (orderType == OrderGroups.OTOCO)
            {
                SelectOTOCOPrimaryOrderType(ORDER_TYPE_ENTRY);
                BuySellOTOCOPrimary(buySell);
            }
            else
            {
                if (orderType != OrderGroups.If_Then)
                {
                    InstallPrimaryRateEdit(rateCoefficient);
                    BuySellPrimary(buySell);
                }
                SelectPrimaryOrderType(ORDER_TYPE_ENTRY);
            }
            PressOK();

            bool result = false;
            string symbol = OrderGroups.OTOCO == orderType ? primaryOTOCOsymbol : primarySymbol;

            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CCOCheckOrderType(symbol, orderType, buySell,
                    rateCoefficient, orderTypeCurrancyPair);
            }

            return result;
        }

        public bool SecondaryBuySellTypeInTabOrder(TradingStationWindow application,
            BuySellEnum buySell, double rateCoefficient, OrderGroups orderType,
            string primarySymbol, string secondarySymbol, string primaryOTOCOsymbol,
            string orderTypeCurrancyPair)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(primaryOTOCOsymbol);
                InstallPrimaryRateOTOCOIfThenOCOEdit(rateCoefficient);
            }
            if (orderType != OrderGroups.If_Then)
            {
                SelectPrimarySymbol(primarySymbol);
                InstallPrimaryRateEdit(rateCoefficient);
            }
            SelectSecondarySymbol(secondarySymbol);
            InstallSecondaryRateEdit(rateCoefficient);
            SelectSecondaryOrderType(orderTypeCurrancyPair);
            BuySellSecondary(buySell);

            PressOK();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                result = orderTab.CCOCheckOrderType(secondarySymbol, orderType, buySell,
                    rateCoefficient, orderTypeCurrancyPair);
            }
            return result;
        }

        public bool SecondaryBuySellTypeInTabOrderPegged(TradingStationWindow application,
           BuySellEnum buySell, double ratePegged, OrderGroups orderType,
           string primarySymbol, string secondarySymbol, string primaryOTOCOsymbol,
           string orderTypeCurrancyPair)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SelectOTOCOIfThenOCOFirstSymbol(primaryOTOCOsymbol);
                InstallPrimaryRateOTOCOIfThenOCOEdit(0.1);
            }
            SecondaryOrdersInPipsCheckBox(ON);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            BuySellPrimary(buySell);
            BuySellSecondary(buySell);
            if (DeterminationOTOCOIfThenOCO(orderType))
            {
                SecondaryOrdersInPipsCheckBox(OFF);
                Waiter.WaitingCondition conditionNonPeggedPrimary = () => ReturnValueSecondaryRate().Length == 7;
                Waiter.Wait(conditionNonPeggedPrimary);
                InstallPrimaryRateEdit(ratePegged);
                SecondaryOrdersInPipsCheckBox(ON);
                Waiter.WaitingCondition conditionPeggedPrimary = () => ReturnValueSecondaryRate().Length < 7;
                Waiter.Wait(conditionPeggedPrimary);
            }
            else
            {
                InstallPrimaryRateEdit(0.1);
                SelectSecondaryOrderType(orderTypeCurrancyPair);

                SecondaryOrdersInPipsCheckBox(OFF);
                Waiter.WaitingCondition conditionNonPeggedSecondary = () => ReturnValueSecondaryRate().Length == 7;
                Waiter.Wait(conditionNonPeggedSecondary);
                InstallSecondaryRateEdit(ratePegged);
                SecondaryOrdersInPipsCheckBox(ON);
                Waiter.WaitingCondition conditionPeggedSecondary = () => ReturnValueSecondaryRate().Length < 7;
                Waiter.Wait(conditionPeggedSecondary);
            }

            PressOK();

            bool result = false;
            using (var orderTab = new OrdersTab(application))
            {
                if (DeterminationOTOCOIfThenOCO(orderType))
                {
                    result = orderTab.CCOCheckOrderType(primarySymbol, orderType, buySell,
                        ratePegged, orderTypeCurrancyPair);
                }
                else
                {
                    result = orderTab.CCOCheckOrderType(secondarySymbol, orderType, buySell,
                        ratePegged, orderTypeCurrancyPair);
                }
            }
            return result;
        }

        public bool OTOCODiffSecondarySymbol(string primarySymbol, string secondarySymbol,
            double primaryCoeff, double secondaryCoeff)
        {
            OrderGroups orderType = OrderGroups.OTOCO;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            string primaryRate = InstallPrimaryRateEdit(primaryCoeff).ToString();
            string SecondaryRate = InstallSecondaryRateEdit(secondaryCoeff).ToString();
            Thread.Sleep(1000); //Give a chance to the races
            string valuePrimaryNow = ReturnValuePrimaryRate().TrimEnd('0');
            string valueSecondaryNow = ReturnValueSecondaryRate().TrimEnd('0');

            if (valuePrimaryNow.Equals(primaryRate) &&
                valueSecondaryNow.Equals(SecondaryRate) &&
                !dialog[SYNC_RATES_CHECK_BOX_ID].Enabled)
            {
                TestContext.Out.WriteLine("\r\nValue rate not change, Sync Rates not available");
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool OTOCOSecondaryINPipsOnTrailingPrimarySameSymbol(string primarySymbol,
            string changeSymbol)
        {
            OrderGroups orderType = OrderGroups.OTOCO;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(primarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(primarySymbol);
            TrailingOTOCOOptionPrimary(TRAILING_VALUE_FIXED);
            SelectSecondarySymbol(changeSymbol);

            if (ReturnValuePrimaryRate().Length < LENGTH_RATE &&
                ReturnValueSecondaryRate().Length < LENGTH_RATE &&
                ReturnSecondaryInPipsCheckBox() &&
                !dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled)
            {
                TestContext.Out.WriteLine("\r\nvalue pegged, secondary in pip ON and not available");
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool OTOCOSecondaryINPipsOnTrailingPrimaryDiffSymbol(string primarySymbol,
            string secondarySymbol)
        {
            OrderGroups orderType = OrderGroups.OTOCO;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(primarySymbol);
            TrailingOTOCOOptionPrimary(TRAILING_VALUE_FIXED);
            SelectPrimarySymbol(secondarySymbol);
            SelectSecondarySymbol(secondarySymbol);

            bool result = dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled;
            if (result)
            {
                TestContext.Out.WriteLine("\r\nCheck box Secondary In Pips Enabled");
            }
            return result;
        }

        public bool OTOCOPeggedSecondarySecondaryInPipsOn(string firstPrimarySymbol,
            string primarySymbol, string secondarySymbol)
        {
            OrderGroups orderType = OrderGroups.OTOCO;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            SecondaryOrdersInPipsCheckBox(ON);
            bool result = ReturnValuePrimaryRate().Length < LENGTH_RATE &&
                ReturnValueSecondaryRate().Length < LENGTH_RATE;
            if (result)
            {
                TestContext.Out.WriteLine("\r\nvalue secondary rates - pegged");
            }
            return result;
        }

        public bool OTOCOPeggedSecondarySecondaryInPipsOff(string firstPrimarySymbol,
            string primarySymbol, string secondarySymbol)
        {
            OrderGroups orderType = OrderGroups.OTOCO;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            SecondaryOrdersInPipsCheckBox(OFF);
            bool result = ReturnValuePrimaryRate().Length == LENGTH_RATE &&
                ReturnValueSecondaryRate().Length == LENGTH_RATE;
            if (result)
            {
                TestContext.Out.WriteLine("\r\nvalue secondary rates - not pegged");
            }
            return result;
        }

        public bool IfThenIfThenOcoEnableCheckBOxSecondaryOrderInPips(
            OrderGroups orderType, double primaryRateCoeff, double secondaryRateCoeff)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            TrailingOptionSecondary(TRAILING_VALUE_FIXED);
            bool result = dialog[IN_PIPS_SECONDARY_ORDER_CHECK_BOX_ID].Enabled;
            if (result)
            {
                TestContext.Out.WriteLine("\r\nCheck box secondary in pips enabled");
            }
            return result;
        }

        public bool IfThenOnOffSecondaryPip(bool OnOffSecondaryInPips)
        {
            OrderGroups orderType = OrderGroups.If_Then;
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(OnOffSecondaryInPips);
            bool result;
            if (OnOffSecondaryInPips)
            {
                result = ReturnValueSecondaryRate().Length < LENGTH_RATE;
            }
            else
            {
                result = ReturnValueSecondaryRate().Length == LENGTH_RATE;
            }
            if (result)
            {
                TestContext.Out.WriteLine("\r\nvalue secondary rate correct format");
            }
            return result;
        }

        public bool IfThenOCOOnOffSecondaryPip(string firstPrimarySymbol, string
            primarySymbol, string secondarySymbol, bool OnOffSecondaryInPips)
        {
            OrderGroups orderType = OrderGroups.If_Then;
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SelectOTOCOIfThenOCOFirstSymbol(firstPrimarySymbol);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            SecondaryOrdersInPipsCheckBox(OnOffSecondaryInPips);
            bool result;
            if (OnOffSecondaryInPips)
            {
                result = ReturnValueSecondaryRate().Length < LENGTH_RATE;
            }
            else
            {
                result = ReturnValueSecondaryRate().Length == LENGTH_RATE;
            }
            if (result)
            {
                TestContext.Out.WriteLine("\r\nvalue secondary rate correct format");
            }
            return result;
        }

        public bool IfThenIfThenOCONonPeggedSecondaryStopLimitInPipsOff(OrderGroups orderType)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            SecondaryOrdersInPipsCheckBox(OFF);
            AllOnOffStopLimit(ON, orderType);
            InPipsCheckBox(OFF);
            Thread.Sleep(200);
            bool result = ReturnLimitSecondary().Length == LENGTH_RATE &&
                ReturnStopSecondary().Length == LENGTH_RATE;
            if (orderType == OrderGroups.If_Then_OCO)
            {
                result = result && ReturnStopPrimary().Length == LENGTH_RATE &&
                    ReturnLimitPrimary().Length == LENGTH_RATE;
            }
            if (result)
            {
                TestContext.Out.WriteLine("\r\nvalues stop/limit not pegged");
            }
            return result;
        }

        public bool IfThenIfThenOCONonPeggedSecondaryInPip(OrderGroups orderType)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(OFF);
            AllOnOffStopLimit(ON, orderType);
            InPipsCheckBox(ON);
            bool result = ReturnLimitSecondary().Length < LENGTH_RATE &&
               ReturnStopSecondary().Length < LENGTH_RATE;
            if (orderType == OrderGroups.If_Then_OCO)
            {
                result = result && ReturnStopPrimary().Length < LENGTH_RATE &&
                    ReturnLimitPrimary().Length < LENGTH_RATE;
            }
            if (result)
            {
                TestContext.Out.WriteLine("\r\nvalue secondary rstop/limit pegged");
            }
            return result;
        }

        public bool IfThenIfThenOCONonPeggedSecondaryInPipOn(OrderGroups orderType)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(ON);
            AllOnOffStopLimit(ON, orderType);
            bool result = ReturnLimitSecondary().Length < LENGTH_RATE &&
               ReturnStopSecondary().Length < LENGTH_RATE &&
               ReturnInPipsOnOffCheckBox() &&
               !dialog[IN_PIPS_CHECK_BOX_ID].Enabled;
            if (orderType == OrderGroups.If_Then_OCO)
            {
                result = result && ReturnStopPrimary().Length < LENGTH_RATE &&
                    ReturnLimitPrimary().Length < LENGTH_RATE;
            }
            if (result)
            {
                TestContext.Out.WriteLine(
                    "\r\nvalue secondary stop/limit pegged, check box in pips On and not available");
            }
            return result;
        }

        public void createOTOorOCOOrder(OrderGroups orderType, string primarySymbol,
            string secondarySymbol, BuySellEnum buySellPrimary, BuySellEnum
            buySellSecondary, double primaryRateCoeff, double secondaryRateCoeff)
        {
            SelectOrderType(OrderGroupDictionary.orderGroupNames[orderType]);
            DefaultOption(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            BuySellPrimary(buySellPrimary);
            BuySellSecondary(buySellSecondary);
            InstallPrimaryRateEdit(primaryRateCoeff);
            InstallSecondaryRateEdit(secondaryRateCoeff);
            PressOK();
        }

        #region Private function this block
        private void PreapirOTOOTOCONonPeggedSecondaryStopLimit(OrderGroups orderType,
                    bool onnOffSecondaryInPips)
        {
            SelectOrderTypeEnum(orderType);
            DefaultOption(orderType);
            SecondaryOrdersInPipsCheckBox(onnOffSecondaryInPips);
            AllOnOffStopLimit(ON, orderType);
        }

        private void installOrderTypeOnOffSecondaryOrdersInPips(string primarySymbol,
            string secondarySymbol, BuySellEnum buySellSecondary, OrderGroups orderType)
        {
            SelectOrderTypeEnum(orderType);
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            BuySellSecondary(buySellSecondary);
            SecondaryOrdersInPipsCheckBox(ON);
            SecondaryOrdersInPipsCheckBox(OFF);
        }

        private bool ChangeSecondarySymbolWithoutTouching(TradingStationWindow application,
            string primarySymbol, string secondarySymbol, BuySellEnum buySellSecondary)
        {
            SelectPrimarySymbol(primarySymbol);
            SelectSecondarySymbol(secondarySymbol);
            string secondaryValue = ReturnValueSecondaryRate();

            var timeoutMonitor = new TimeoutMonitor(60);
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                string beginRateSimple = simpleDealingRates.ReturnValueBuySellSymbol(
                    secondarySymbol, buySellSecondary);
                while (!timeoutMonitor.CheckTimeout())
                {
                    if (!beginRateSimple.Equals(simpleDealingRates.ReturnValueBuySellSymbol(
                    secondarySymbol, buySellSecondary)))
                    {
                        Waiter.WaitingCondition condition = () => secondaryValue.Equals(ReturnValuePrimaryRate()) == false;
                        if (!Waiter.Wait(condition, 5))
                        {
                            throw new Exception("There is no price change in type Order - " + ReturnOrderType());
                        }
                        TestContext.Out.WriteLine("\r\nCorrect change of rate");
                        return true;
                    }
                    Thread.Sleep(500); // Checking values from SimpleDealingRate every half a second
                }
            }
            PressCancel();
            throw new Exception("The price did not change within the allotted time");
        }

        private bool SecondaryPriceNotChange(TradingStationWindow application,
            string secondarySymbol, BuySellEnum buySellSecondary, string secondaryValue)
        {
            var timeoutMonitor = new TimeoutMonitor(60);
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                string beginRateSimple = simpleDealingRates.ReturnValueBuySellSymbol(
                    secondarySymbol, buySellSecondary);
                while (!timeoutMonitor.CheckTimeout())
                {
                    if (!beginRateSimple.Equals(simpleDealingRates.ReturnValueBuySellSymbol(
                    secondarySymbol, buySellSecondary)))
                    {
                        Thread.Sleep(1000); //Give chance to change 
                        if (secondaryValue.Equals(ReturnValueSecondaryRate()))
                        {
                            PressCancel();
                            TestContext.Out.WriteLine("\r\nCorrect not change of rate");
                            return true;
                        }
                    }
                    Thread.Sleep(500); // Checking values from SimpleDealingRate every half a second
                }
            }
            PressCancel();
            throw new Exception("The price did not change within the allotted time");
        }

        private bool primaryNotChangeWhenItChanges(TradingStationWindow application,
           string primarySymbol, BuySellEnum buySellPrimary, string primaryValue,
           OrderGroups orderType)
        {
            var timeoutMonitor = new TimeoutMonitor(60);
            if (!DeterminationOTOCOIfThenOCO(orderType))
            {
                using (var simpleDealingRates = new SimpleDealingRatesTab(application))
                {
                    string beginRateSimple = simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary);
                    while (!timeoutMonitor.CheckTimeout())
                    {
                        if (!beginRateSimple.Equals(simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary)))
                        {
                            Thread.Sleep(1000); //Give chance to change 
                            if (primaryValue.Equals(ReturnValuePrimaryRate()))
                            {
                                PressCancel();
                                TestContext.Out.WriteLine("\r\nCorrect not change of rate");
                                return true;
                            }
                        }
                        Thread.Sleep(500); // Checking values from SimpleDealingRate every half a second
                    }
                }
            }
            else
            {
                using (var simpleDealingRates = new SimpleDealingRatesTab(application))
                {
                    string beginRateSimple = simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary);
                    while (!timeoutMonitor.CheckTimeout())
                    {
                        if (!beginRateSimple.Equals(simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary)))
                        {
                            Thread.Sleep(1000); //Give chance to change 
                            if (primaryValue.Equals(ReturnValuePrimaryRateOTOCOIfThenOCO()))
                            {
                                PressCancel();
                                TestContext.Out.WriteLine("\r\nCorrect not change of rate");
                                return true;
                            }
                        }
                        Thread.Sleep(500); // Checking values from SimpleDealingRate every half a second
                    }
                }
            }
            PressCancel();
            throw new Exception("The price did not change within the allotted time");
        }

        private bool PrimaryChangeChangeCurrientPrice(TradingStationWindow application,
           string primarySymbol, BuySellEnum buySellPrimary, OrderGroups orderType,
           string primaryValue)
        {
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                string beginRateSimple = simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary);
                var timeoutMonitor = new TimeoutMonitor(60);
                if (!DeterminationOTOCOIfThenOCO(orderType))
                {
                    while (!timeoutMonitor.CheckTimeout())
                    {
                        if (!beginRateSimple.Equals(simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary)))
                        {
                            Waiter.WaitingCondition condition = () => primaryValue.Equals(ReturnValuePrimaryRate()) == false;
                            if (!Waiter.Wait(condition, 5))
                            {
                                PressCancel();
                                throw new Exception("There is no price change");
                            }
                            PressCancel();
                            TestContext.Out.WriteLine("\r\nCorrect change of rate");
                            return true;
                        }
                        Thread.Sleep(500); // Checking values from SimpleDealingRate every half a second
                    }
                }
                else
                {
                    while (!timeoutMonitor.CheckTimeout())
                    {
                        if (!beginRateSimple.Equals(simpleDealingRates.ReturnValueBuySellSymbol(
                        primarySymbol, buySellPrimary)))
                        {
                            Waiter.WaitingCondition condition = () => primaryValue.Equals(ReturnValuePrimaryRateOTOCOIfThenOCO()) == false;
                            if (!Waiter.Wait(condition, 5))
                            {
                                throw new Exception("There is no price change");
                            }
                            PressCancel();
                            TestContext.Out.WriteLine("\r\nCorrect change of rate");
                            return true;
                        }
                        Thread.Sleep(500); // Checking values from SimpleDealingRate every half a second
                    }
                }
            }
            PressCancel();
            throw new Exception("The price did not change within the allotted time");
        }

        public bool OTOCOOneSymbolOffSyncRates(string symbol, double firstPrimaryCoefficient,
            double primaryCoefficient, double secondaryCoefficient)
        {
            SelectOrderType(ORDER_TYPE_OTOCO);
            DefaultOption(OrderGroups.OTOCO);
            SyncRatesCheckBox(OFF);
            SelectOTOCOIfThenOCOFirstSymbol(symbol);
            SelectPrimarySymbol(symbol);
            SelectSecondarySymbol(symbol);
            string rateForCheckFirstPrimary = InstallPrimaryRateOTOCOIfThenOCOEdit
                (firstPrimaryCoefficient).ToString();
            string rateForCheckPrimary = InstallPrimaryRateEdit(primaryCoefficient).ToString();
            string rateForCheckSecondary = InstallSecondaryRateEdit(secondaryCoefficient).ToString();
            string valueFirstPrimaryNow = ReturnValuePrimaryRateOTOCOIfThenOCO().TrimEnd('0');
            string valuePrimaryNow = ReturnValuePrimaryRate().TrimEnd('0');
            string valueSecondaryNow = ReturnValueSecondaryRate().TrimEnd('0');
            PressCancel();
            return rateForCheckPrimary.Equals(valuePrimaryNow) &&
                rateForCheckSecondary.Equals(valueSecondaryNow) &&
                rateForCheckFirstPrimary.Equals(valueFirstPrimaryNow);
        }

        public bool OTOCOOnSyncRatesSameSymbol(string symbol, BuySellEnum buySellPrimary,
            BuySellEnum buySellSecondary, double primaryCoefficient)
        {
            SelectOrderType(ORDER_TYPE_OTOCO);
            DefaultOption(OrderGroups.OTOCO);
            GeneralOCOInstall(symbol, symbol, buySellPrimary, buySellSecondary);
            SyncRatesCheckBox(ON);
            InstallPrimaryRateEdit(primaryCoefficient);
            double primaryRateBefore = double.Parse(ReturnValuePrimaryRate());
            double secondaryRateBefore = double.Parse(ReturnValueSecondaryRate());
            double editPrimaryValue = InstallPrimaryRateEdit(primaryCoefficient);
            double changedOn = editPrimaryValue - primaryRateBefore;
            double changeSeconadaryRate;
            if (buySellPrimary != buySellSecondary)
            {
                changeSeconadaryRate = secondaryRateBefore - changedOn;
            }
            else
            {
                changeSeconadaryRate = secondaryRateBefore + changedOn;
            }
            string compareValue = Math.Round(changeSeconadaryRate, 5).ToString();
            string compare = ReturnValueSecondaryRate().TrimEnd('0');
            bool result = compare.Equals(compareValue);
            PressCancel();
            return result;
        }

        #endregion
        #endregion
    }
}

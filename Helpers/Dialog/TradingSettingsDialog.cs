﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests
{
    class TradingSettingsDialog : IDisposable
    {
        private Dialog dialog;

        private const string TRADING_SETTING_DIALOG_TITLE = "Trading Settings";
        private const string DEFAULT_CLICK_AND_CONFIRM_MODE = "Default (Click and Confirm)";
        private const string ONE_CLICK_MODE = "One-Click";
        private const string DOUBLE_CLICK_MODE = "Double-Click";
        private const string ONE_CLICK_MODE_LABEL = "Click once to submit an order instantly. " +
                "First you must setup your desired order parameters below.";
        private const string DOUBLE_CLICK_MODE_LABEL = "Double-click to submit an order instantly. " +
            "First you must setup your desired order parameters below.";
        private const string OK_BUTTON = "OK";
        private const string CANCEL_BUTTON = "Cancel";
        private const int ORDER_TYPE_ID = 49221;
        private const int MARKET_ORDERS_TIME_IN_FORCE_ID = 49522;
        private const int MARKET_RANGE_VALUE_ID = 46747;
        private const int STOP_DISTANCE_VALUE_ID = 46885;
        private const int LIMIT_DISTANCE_VALUE_ID = 46888;
        private const int MODE_CLICK_LABEL_ID = 5651;
        private const int STOP_CHECK_BOX_ID = 30020;
        private const int LIMIT_CHECK_BOX_ID = 30021;
        private const bool ON = true;
        private const bool OFF = false;
        private const string AT_MARKET_ORDER_TYPE_COMBO_BOX = "At Market";
        private const string MARKET_RANGE_ORDER_TYPE_COMBO_BOX = "Market Range";
        private const string GTC_VALUE_COMBO_BOX = "GTC";
        private const string IOK_VALUE_TIME_IN_FORCE_COMBO_BOX = "IOC";
        private const string STOP_CHECKED = "Stop:";
        private const string LIMIT_CHECEKED = "Limit:";

        private const string DISCLAIMER_TITLE = "Disclaimer";
        private const string DISCLAIMER_CHECKED_ACCEPT =
            "I Accept the terms of this agreement";


        public TradingSettingsDialog(TradingStationWindow application)
        {
            dialog = application.GetDialog(TRADING_SETTING_DIALOG_TITLE);

            if (dialog == null)
            {
                var applicationMenu = new ApplicationMenuHelper();
                applicationMenu.PressTradingDealingRatesSettings(application);

                Waiter.WaitingCondition condition = () =>
                    application.GetDialog(TRADING_SETTING_DIALOG_TITLE) != null;
                if (!Waiter.Wait(condition))
                {
                    throw new Exception("Not found window Trading Setting");
                }
                else
                {
                    dialog = application.GetDialog(TRADING_SETTING_DIALOG_TITLE);
                    TestContext.Out.WriteLine("\r\nDialog Trading Setting apear");
                }
            }
        }
        public bool CheckOneClickOnlyLimitSetting(TradingStationWindow application,
            double limitValue, string orderType, string marketOrderTimeInForce)
        {
            installOneClick(application);
            StopCheckBox(OFF);
            EnterLimitEdit(limitValue);
            GeneralSettingOneDoubleClick(marketOrderTimeInForce, orderType);

            CloseDialog();

            FindWindowTradingSetting(application);
            bool result = false;
            result = CheckValueLimit(limitValue) &&
                        CheckOnOneClick() &&
                        CheckValueOrderTypeComboBox(orderType) &&
                        CheckValueTimeInForceComboBox(marketOrderTimeInForce);
            CloseDialog();

            return result;

        }

        public bool CheckedDefault(TradingStationWindow application)
        {
            bool result = false;

            DefaultClickAndConfirm();
            CloseDialog();

            FindWindowTradingSetting(application);
            if (!dialog[ORDER_TYPE_ID].Enabled)
            {
                result = true;
            }
            CloseDialog();

            return result;
        }

        public bool CheckedAgreementOneClick(TradingStationWindow application)
        {
            bool result = false;

            DefaultClickAndConfirm();
            CloseDialog();

            FindWindowTradingSetting(application);
            dialog[ONE_CLICK_MODE].Click();

            StopCheckBox(OFF);
            LimitCheckBox(OFF);
            SettingOrderType(GTC_VALUE_COMBO_BOX);
            SettingMarketTimeInForce(AT_MARKET_ORDER_TYPE_COMBO_BOX);

            dialog[OK_BUTTON].Click();

            result = DisclamerDialog(application);
            return result;

        }

        public bool CheckedDoubleClickMode(TradingStationWindow application)
        {
            bool result = false;
            DefaultClickAndConfirm();

            FindWindowTradingSetting(application);
            dialog[DOUBLE_CLICK_MODE].Click();
            dialog[OK_BUTTON].Click();
            DisclamerDialog(application);

            FindWindowTradingSetting(application);
            result = CheckDoubleClick();
            CloseDialog();

            return result;
        }

        public bool CheckOneClick(TradingStationWindow application)
        {
            bool result = false;

            CheckedAgreementOneClick(application);
            FindWindowTradingSetting(application);

            result = CheckOnOneClick();

            CloseDialog();

            return result;
        }

        public bool OneClickModeWithStopAndLimit(TradingStationWindow application, double limitValue, double stopValue)
        {
            installOneClick(application);
            string orderType = AT_MARKET_ORDER_TYPE_COMBO_BOX;
            string marketTimeInForce = GTC_VALUE_COMBO_BOX;

            return InstallAndCheckStopLimit(application, limitValue, stopValue, orderType, marketTimeInForce);
        }

        public bool OneClickWithLimitBuySell(TradingStationWindow application, double limitValue, double stopValue,
            string symbol, BuySellEnum buySell)
        {
            bool result = false;
            OneClickModeWithStopAndLimit(application, limitValue, stopValue);
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                simpleDealingRates.PressBuyOrSellOnCurrancyPair(symbol,
                    buySell, application);
            }
            using (var openPosition = new OpenPositionsTab(application))
            {
                int rowItem = openPosition.FindOnSymbolWaitingOpenPosition(symbol);
                result = openPosition.CheckLimitExposedPip(limitValue, buySell, rowItem);
            }
            return result;
        }

        public bool OneClickWithOutStopLimitBuySell(TradingStationWindow 
            application, string symbol, BuySellEnum buySell)
        {
            installOneClick(application);
            StopCheckBox(OFF);
            LimitCheckBox(OFF);
            CloseDialog();

            bool result;
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                simpleDealingRates.PressBuyOrSellOnCurrancyPair(symbol,
                    buySell, application);
            }
            using (var openPosition = new OpenPositionsTab(application))
            {
                openPosition.ClickOpenPosition(application);
                int rowItem = openPosition.FindOnSymbolWaitingOpenPosition(symbol);
                result = openPosition.CheckForEmptyColumsStopLimit(rowItem);
            }
            return result;
        }

        public bool OneClickSetting(TradingStationWindow application, string 
            marketOrderTimeInForce, string orderType, double marketRangeValue = 0)
        {
            installOneClick(application);
            GeneralSettingOneDoubleClick(marketOrderTimeInForce, orderType, 
                marketRangeValue);
            StopCheckBox(OFF);
            LimitCheckBox(OFF);

            CloseDialog();

            FindWindowTradingSetting(application);
            bool result = false;
            result = CheckOnOneClick() &&
                        CheckMarketRangeValue(marketRangeValue) &&
                        CheckValueOrderTypeComboBox(orderType) &&
                        CheckValueTimeInForceComboBox(marketOrderTimeInForce);
            CloseDialog();

            return result;
        }

        public bool DoubleClickSetting(TradingStationWindow application, 
            string marketOrderTimeInForce, string orderType, double 
            marketRangeValue = 0)
        {
            if (CheckDoubleClick() == false)
            {
                CheckedDoubleClickMode(application);
            }
            GeneralSettingOneDoubleClick(marketOrderTimeInForce, orderType, 
                marketRangeValue);
            StopCheckBox(OFF);
            LimitCheckBox(OFF);

            CloseDialog();

            FindWindowTradingSetting(application);
            bool result = false;
            result = CheckDoubleClick() &&
                        CheckMarketRangeValue(marketRangeValue) &&
                        CheckValueOrderTypeComboBox(orderType) &&
                        CheckValueTimeInForceComboBox(marketOrderTimeInForce);
            CloseDialog();

            return result;
        }

        public bool DoubleClickStopLimit(TradingStationWindow application, 
            double stopValue, double limitValue, string orderType, string 
            marketTimeInForce)
        {
            if (CheckDoubleClick() == false)
            {
                CheckedDoubleClickMode(application);
                FindWindowTradingSetting(application);
            }

            bool result = false;
            result = InstallAndCheckStopLimit(application, limitValue, 
                stopValue, orderType, marketTimeInForce);
            return result;
        }

        public bool OneClickWithOnlyLimitBuySell(TradingStationWindow 
            application, double limitValue, string symbol, BuySellEnum buySell,
            string orderType, string marketTimeInForce)
        {
            bool result = false;
            CheckOneClickOnlyLimitSetting(application, limitValue, 
                orderType, marketTimeInForce);
            using (var simpleDealingRates = new SimpleDealingRatesTab(application))
            {
                simpleDealingRates.PressBuyOrSellOnCurrancyPair(symbol,
                    buySell, application);
            }
            using (var openPosition = new OpenPositionsTab(application))
            {
                int rowItem = openPosition.FindOnSymbolWaitingOpenPosition(symbol);
                result = openPosition.CheckLimitExposedPip(limitValue, buySell, 
                    rowItem);
            }
            return result;
        }

        private void GeneralSettingOneDoubleClick(string marketOrderTimeInForce,
            string orderType, double marketRangeValue = 0)
        {
            SettingOrderType(orderType);
            SettingMarketTimeInForce(marketOrderTimeInForce);
            if (orderType == MARKET_RANGE_ORDER_TYPE_COMBO_BOX)
            {
                SettingMarketRange(marketRangeValue);
            }
        }

        private void installOneClick(TradingStationWindow application)
        {
            if (dialog[MODE_CLICK_LABEL_ID].Text != ONE_CLICK_MODE_LABEL)
            {
                CheckedAgreementOneClick(application);
                FindWindowTradingSetting(application);
            }
        }

        public bool CheckOneClickSettingWithLimit()
        {
            bool result = false;
            if (dialog[MODE_CLICK_LABEL_ID].Text == ONE_CLICK_MODE_LABEL)
            {
                result = true;
            }
            return result;
        }

        public bool CheckClosingPressCancel()
        {
            dialog[CANCEL_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition))
            {
                return false;
            }
            return true;
        }
        #region 


        #endregion

        #region Internal functions
        private bool InstallAndCheckStopLimit(TradingStationWindow application, 
            double limitValue, double stopValue, string orderType, string 
            marketTimeInForce)
        {
            EnterLimitEdit(limitValue);
            EnterStopEdit(stopValue);
            SettingOrderType(orderType);
            SettingMarketTimeInForce(marketTimeInForce);
            CloseDialog();

            FindWindowTradingSetting(application);
            bool result = false;
            result = CheckValueLimit(limitValue) &&
                CheckValueStop(stopValue) &&
                CheckValueOrderTypeComboBox(orderType) &&
                CheckValueTimeInForceComboBox(marketTimeInForce);
            CloseDialog();
            return result;
        }

        private bool CheckDoubleClick()
        {
            bool result = false;
            if (dialog[MODE_CLICK_LABEL_ID].Text == DOUBLE_CLICK_MODE_LABEL)
            {
                result = true;
            }
            return result;
        }

        private void CloseDialog()
        {
            dialog[OK_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not close window Trading Setting");
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nPress button OK and dialog Trading setting disappear");
            }
        }

        public void DefaultClickAndConfirm()
        {
            dialog[DEFAULT_CLICK_AND_CONFIRM_MODE].Click();
            CloseDialog();
        }

        private void FindWindowTradingSetting(TradingStationWindow application)
        {
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressTradingDealingRatesSettings(application);
            Waiter.WaitingCondition condition = () =>
            application.GetDialog(TRADING_SETTING_DIALOG_TITLE) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found window Trading Setting");
            }
            else
            {
                dialog = application.GetDialog(TRADING_SETTING_DIALOG_TITLE);
                TestContext.Out.WriteLine("\r\nWindow Trading Setting found");
            }
        }


        private bool DisclamerDialog(TradingStationWindow application)
        {
            bool result = false;
            Dialog dialogDisclamer = null;

            Waiter.WaitingCondition condition = () => (dialogDisclamer = application.GetDialog(DISCLAIMER_TITLE)) != null;

            if (!Waiter.Wait(condition))
            {
                return false;
            }
            else
            {
                TestContext.Out.WriteLine("\r\nDisclamer dialog appear");
                result = true;
            }
            dialogDisclamer[DISCLAIMER_CHECKED_ACCEPT].Click();
            dialogDisclamer[OK_BUTTON].Click();
            Waiter.WaitingCondition conditionClose = () => (dialogDisclamer = application.GetDialog(DISCLAIMER_TITLE)) == null;
            using (dialogDisclamer)
            {
                if (!Waiter.Wait(conditionClose))
                {
                    throw new Exception("Not close window Disclamer");
                }
                else
                {
                    TestContext.Out.WriteLine("\r\nDisclamer dialog disappear");
                }
                return result;
            }
        }
        #endregion

        #region Check Value
        private bool CheckOnOneClick()
        {
            if (dialog[MODE_CLICK_LABEL_ID].Text == ONE_CLICK_MODE_LABEL)
            {
                TestContext.Out.WriteLine("\r\ninstall mode One-Click");
                return true;
            }
            throw new Exception("One click is not enabled");
        }

        private bool CheckValueOrderTypeComboBox(string orderTypeCompareValue)
        {
            using (ComboBox orderType = (ComboBox)dialog[ORDER_TYPE_ID])
            {
                bool result = orderTypeCompareValue.Equals(orderType.Text);
                if (result)
                {
                    TestContext.Out.WriteLine("\r\nvalue order type right");
                }
                return result;
            }            
        }

        private bool CheckValueTimeInForceComboBox(string timeInForceCompareValue)
        {
            using (ComboBox marketOrdersTimeInForce = (ComboBox)dialog[MARKET_ORDERS_TIME_IN_FORCE_ID])
            {
                bool result = marketOrdersTimeInForce.Text.Equals(timeInForceCompareValue);
                if (result)
                {
                    TestContext.Out.WriteLine("\r\nvalue time in force right");
                }
                return result;
            }
        }

        private bool CheckValueLimit(double limitValue)
        {
            using (EditBox limitDistanceValue = (EditBox)dialog[LIMIT_DISTANCE_VALUE_ID])
            {
                double compareValue = double.Parse(limitDistanceValue.Text);
                bool result = Math.Round(limitValue, 1) == Math.Round(compareValue, 1);
                if (result)
                {
                    TestContext.Out.WriteLine("\r\nvalue limit right");
                }
                return result;
            }            
        }

        private bool CheckValueStop(double stopValue)
        {
            using (EditBox stopDistanceValue = (EditBox)dialog[STOP_DISTANCE_VALUE_ID])
            {
                double compareValue = double.Parse(stopDistanceValue.Text);
                bool result = Math.Round(stopValue, 1) == Math.Round(compareValue, 1);
                if (result)
                {
                    TestContext.Out.WriteLine("\r\nvalue limit right");
                }
                return result;
            }
        }

        private bool CheckMarketRangeValue(double checkValue)
        {
            using (EditBox editValueMarketRangeOrder = (EditBox)dialog[MARKET_RANGE_VALUE_ID])
            {
                string i = editValueMarketRangeOrder.Text;
                if (double.Parse(editValueMarketRangeOrder.Text) == checkValue)
                {
                    TestContext.Out.WriteLine("\r\nvalue market range right");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region CheckBox
        private void StopCheckBox(bool onOff)
        {
            using (CheckBox stop = (CheckBox)dialog[STOP_CHECK_BOX_ID])
            {
                if (onOff == true)
                {
                    if (stop.Checked == 0)
                    {
                        stop.Click();
                    }
                }
                else
                {
                    if (stop.Checked == 1)
                    {
                        stop.Click();
                    }
                }
            }
        }

        private void LimitCheckBox(bool onOff)
        {
            using (CheckBox limit = (CheckBox)dialog[LIMIT_CHECK_BOX_ID])
            {
                if (onOff == true)
                {
                    if (limit.Checked == 0)
                    {
                        limit.Click();
                    }
                }
                else
                {
                    if (limit.Checked == 1)
                    {
                        limit.Click();
                    }
                }
            }
        }
        #endregion

        #region Setting value 
        private void SettingOrderType(string typeOrder)
        {
            using (ComboBox orderType = (ComboBox)dialog[ORDER_TYPE_ID])
            {
                orderType.SelectItem(typeOrder);
                TestContext.Out.WriteLine("\r\nOrder type  selected- " + typeOrder);
            }
        }

        private void SettingMarketTimeInForce(string type)
        {
            using (ComboBox marketOrdersTimeInForce = (ComboBox)dialog[MARKET_ORDERS_TIME_IN_FORCE_ID])
            {
                marketOrdersTimeInForce.SelectItem(type);
                TestContext.Out.WriteLine("\r\nTime in force selected - " + type);
            }
        }

        private void SettingMarketRange(double valueMarketRange)
        {
            using (EditBox editValueMarketRangeOrder = (EditBox)dialog[MARKET_RANGE_VALUE_ID])
            {
                editValueMarketRangeOrder.Text = Convert.ToString(valueMarketRange);
                TestContext.Out.WriteLine("\r\nMarket Range edited - " + valueMarketRange);
            }
        }

        private void EnterStopEdit(double valueStopDistance)
        {
            StopCheckBox(ON);
            using (EditBox stopDistanceValue = (EditBox)dialog[STOP_DISTANCE_VALUE_ID])
            {
                valueStopDistance = Math.Round(valueStopDistance, 1);
                stopDistanceValue.Text = Convert.ToString(valueStopDistance);
                TestContext.Out.WriteLine("\r\nStop edited - " + valueStopDistance);
            }
        }

        private void EnterLimitEdit(double valueLimitDistance)
        {
            LimitCheckBox(ON);
            using (EditBox limitDistanceValue = (EditBox)dialog[LIMIT_DISTANCE_VALUE_ID])
            {
                valueLimitDistance = Math.Round(valueLimitDistance, 1);
                limitDistanceValue.Text = Convert.ToString(valueLimitDistance * 10); //HACK on 10 less asks
                TestContext.Out.WriteLine("\r\nLimit edited - " + valueLimitDistance);
            }
        }
        #endregion

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

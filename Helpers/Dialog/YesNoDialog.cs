﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests
{
    static class YesNoDialog
    {
        private const string YES_BUTTON = "Yes";

        public static bool PressYes(string title, TradingStationWindow app)
        {
            using (Dialog dlg = Waiter.WaitDialogAppear(app, title))
            {
                if (dlg == null)
                {
                    return false;
                }
                else
                {
                    dlg[YES_BUTTON].Click();
                    TestContext.Out.WriteLine("\r\nPress button Yes in dialog " 
                        + title);
                    return true;
                }
            }
        }
    }
}

﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;
using System.Threading;

namespace TSAutotests
{
    class CreateMarketOrderDialog : IDisposable
    {
        private const string CREATE_MARKET_ORDER_TITLE = "Create Market Order";
        private const int SYMBOL_COMBOBOX_ID = 4408;
        private const int AMOUNT_K_COMBOBOX_ID = 1001;
        private const int ORDER_TYPE_COMBOBOX_ID = 49221;
        private const int TIME_IN_FORCE_COMBOBOX_ID = 47106;
        private const int STOP_CHECK_BOX_ID = 4417;
        private const int LIMIT_CHECK_BOX_ID = 4418;
        private const int RATE_STOP_EDIT_ID = 4421;
        private const int RATE_LIMIT_EDIT_ID = 4422;
        private const int RATE_ID = 4412;
        private const int ACCOUNT_ID = 4410;
        private const int MARKET_RANGE_VALUE = 4413;
        private const int TRAILING_COMBOBOX_ID = 49530;
        private const int IN_PIPS_CHECK_BOX_ID = 49381;
        private const string SELL_RADIOBUTTON = "Sell";
        private const string BUY_RADIOBUTTON = "Buy";
        private const string ADVANCED_BUTTON = "Advanced>>";
        private const string OK_BUTTON = "OK";
        private const string CANCEL_BUTTON = "Cancel";
        private const string ORDER_TYPE_MARKET_RANGE = "Market Range";
        private const string STOP_CHECKED = "Stop:";
        private const string LIMIT_CHECEKED = "Limit:";
        private const string FXCM_ERROR = "FXCM Trading Station Desktop";
        private const bool IN_PIP_CHECK_ON = true;
        private const bool IN_PIP_CHECK_OFF = false;
        private const string ADVANCED_BUTTON_OPEN = "Advanced>>";
        private const string ERROR_NO_TRADABLE_PRICE = "There is no tradable price.";

        private Dialog dialog;

        public CreateMarketOrderDialog(TradingStationWindow application)
        {
            dialog = application.GetDialog(CREATE_MARKET_ORDER_TITLE);
            if (dialog == null)
            {
                Waiter.WaitingCondition dialogExistsCondition = () =>
                    (application.GetDialog(CREATE_MARKET_ORDER_TITLE)) != null;
                if (Waiter.Wait(dialogExistsCondition))
                {
                    dialog = application.GetDialog(CREATE_MARKET_ORDER_TITLE);
                    TestContext.Out.WriteLine("\r\nA dialog Create Market Order appeared");
                }
            }

            if (dialog[ADVANCED_BUTTON_OPEN] != null)
            {
                if (dialog[ADVANCED_BUTTON_OPEN].Enabled)
                {
                    dialog[ADVANCED_BUTTON_OPEN].Click();
                }
            }
        }

        public bool CreateMarketOrderDefault(TradingStationWindow application,
            string symbol, BuySellEnum buySell,
            int amount, string orderType, double valueMarketRange = 0.5)
        {
            SelectSymbol(symbol);
            BuyOrSell(buySell);
            EditAmmountK(amount);
            SelectOrderType(orderType);
            StopLimitOff();
            if (orderType == ORDER_TYPE_MARKET_RANGE)
            {
                MarketRangeValue(valueMarketRange);
            }
            string valueAccaunt = ReturnAccountValue();

            if (!CloseDialog(application))
            {
                return false;
            }

            bool result = false;
            using (var openPostionTab = new OpenPositionsTab(application))
            {
                result = openPostionTab.CheckCreateMarketOrderDefault(symbol, amount.ToString(), buySell, valueAccaunt);
            }
            return result;
        }

        public bool CreateMarketOrderMenu(TradingStationWindow application,
            string symbol, BuySellEnum buySell,
            int amount, string orderType, double valueMarketRange = 0.5)
        {
            EditAmmountK(amount);
            SelectOrderType(orderType);
            BuyOrSell(buySell);
            StopLimitOff();
            if (orderType == ORDER_TYPE_MARKET_RANGE)
            {
                MarketRangeValue(valueMarketRange);
            }
            string valueAccaunt = ReturnAccountValue();

            if (!CloseDialog(application))
            {
                return false;
            }

            bool result = false;
            using (var openPostionTab = new OpenPositionsTab(application))
            {
                result = openPostionTab.CheckCreateMarketOrderDefault(symbol, amount.ToString(), buySell, valueAccaunt);
            }
            return result;
        }

        public bool CreateMarketOrderMenuStopLimit(TradingStationWindow application,
            string symbol, BuySellEnum buySell,
            int amount, string orderType, double stopRateDiffernce, double
            limitRateDifference, double valueMarketRange = 0.5)
        {
            SelectSymbol(symbol);
            EditAmmountK(amount);
            SelectOrderType(orderType);
            BuyOrSell(buySell);
            if (orderType == ORDER_TYPE_MARKET_RANGE)
            {
                MarketRangeValue(valueMarketRange);
            }

            //InPipsOnCheckBoks(IN_PIP_CHECK_OFF);
            string valueAccaunt = ReturnAccountValue();
            double stopRateInstall = double.Parse(InstallValueStop(stopRateDiffernce, buySell));
            double limitRateInstall = double.Parse(InstallValueLimit(limitRateDifference, buySell));

            if (!CloseDialog(application))
            {
                return false;
            }

            bool result = false;
            using (var openPostionTab = new OpenPositionsTab(application))
            {
                result = openPostionTab.CheckCreateMarketOrderStopLimit(symbol, amount.ToString(), buySell, valueAccaunt, stopRateInstall, limitRateInstall);
            }
            return result;
        }

        public bool CreateMarketOrderMenuLimitInPip(TradingStationWindow application,
            string symbol, BuySellEnum buySell, int amount, string orderType,
            double limitInPip, double valueMarketRange = 0.5)
        {
            SelectSymbol(symbol);
            EditAmmountK(amount);
            SelectOrderType(orderType);
            BuyOrSell(buySell);
            if (orderType == ORDER_TYPE_MARKET_RANGE)
            {
                MarketRangeValue(valueMarketRange);
            }
            string valueAccaunt = ReturnAccountValue();
            InPipsOnCheckBoks(IN_PIP_CHECK_ON);
            InstallValueLimitInPip(limitInPip);

            if (!CloseDialog(application))
            {
                return false;
            }

            bool result = false;
            using (var openPostionTab = new OpenPositionsTab(application))
            {
                result = openPostionTab.CheckCreateMarketOrderLimitPip(symbol, amount.ToString(), buySell, valueAccaunt, limitInPip);
            }
            return result;
        }

        #region install value
        private string InstallValueStop(double stopRate, BuySellEnum buySell)
        {
            if (!dialog[RATE_STOP_EDIT_ID].Enabled)
            {
                dialog[STOP_CHECKED].Click();
                TestContext.Out.WriteLine("\r\nCheck box Stop ON");
            }
            InPipsOnCheckBoks(IN_PIP_CHECK_OFF);
            int count = ReturnRateNow().ToString().Length;
            using (EditBox rateStop = (EditBox)dialog[RATE_STOP_EDIT_ID])
            {
                //Waiter.WaitingCondition conditionRateNonPegged = () =>
                //     count == rateStop.Text.Length;
                //if (!Waiter.Wait(conditionRateNonPegged))
                //{
                //    throw new Exception("nonPegged value stop");
                //}
                double valueStop;
                double rateNow = ReturnRateNow();
                if (buySell == BuySellEnum.BUY)
                {
                    valueStop = rateNow - rateNow * stopRate;
                }
                else
                {
                    valueStop = rateNow + rateNow * stopRate;
                }
                valueStop = MathHelper.RoundingDependingOnSignsAfterComma(valueStop);
                string installString = valueStop.ToString();
                while (installString.Length < count)
                {
                    installString = installString + "0";
                }
                string recordValue = installString.Remove(count - 1, 7 - count);
                while (recordValue.Length == count)
                {
                    recordValue = recordValue + "0";
                }
                rateStop.Text = recordValue;
                TestContext.Out.WriteLine("\r\nInstal value Stop - " + recordValue);
                return recordValue;
            }
        }

        private string InstallValueLimit(double limitRate, BuySellEnum buySell)
        {
            if (!dialog[RATE_LIMIT_EDIT_ID].Enabled)
            {
                dialog[LIMIT_CHECEKED].Click();
                TestContext.Out.WriteLine("\r\nCheck box Limit ON");
            }
            InPipsOnCheckBoks(IN_PIP_CHECK_OFF);
            int count = ReturnRateNow().ToString().Length;
            using (EditBox limit = (EditBox)dialog[RATE_LIMIT_EDIT_ID])
            {

                //Waiter.WaitingCondition conditionRateNonPegged = () =>
                //    count == limit.Text.Length;
                //if (!Waiter.Wait(conditionRateNonPegged))
                //{
                //    throw new Exception("nonPegged value limit");
                //}
                double rateNow = ReturnRateNow();
                double valueLimit;
                if (buySell == BuySellEnum.BUY)
                {
                    valueLimit = rateNow + rateNow * limitRate;
                }
                else
                {
                    valueLimit = rateNow - rateNow * limitRate;
                }
                valueLimit = MathHelper.RoundingDependingOnSignsAfterComma(valueLimit);
                string installString = valueLimit.ToString();
                while (installString.Length < count)
                {
                    installString = installString + "0";
                }
                string recordValue = installString.Remove(count - 1, 7 - count);
                while (recordValue.Length < count)
                {
                    recordValue = recordValue + "0";
                }
                limit.Text = recordValue;
                TestContext.Out.WriteLine("\r\nInstal value Limit - " + recordValue);
                return recordValue;
            }
        }

        private double CarcasInstallRate(int ID, double rateCoefficient)
        {
            using (EditBox editRate = (EditBox)dialog[ID])
            {
                int count = editRate.Text.Length;
                double installValue = double.Parse(editRate.Text) +
                    double.Parse(editRate.Text) * rateCoefficient;
                installValue = MathHelper.RoundingDependingOnSignsAfterComma(
                    installValue);
                string recordValue = installValue.ToString().Remove(count - 1, 7 - count);
                while (recordValue.Length == count)
                {
                    recordValue = recordValue + "0";
                }
                editRate.Text = recordValue;
                return double.Parse(recordValue);
            }
        }

        private void InstallValueLimitInPip(double pipLimit)
        {
            if (!dialog[RATE_LIMIT_EDIT_ID].Enabled)
            {
                dialog[LIMIT_CHECEKED].Click();
            }
            pipLimit = Math.Round(pipLimit, 1);
            using (EditBox limit = (EditBox)dialog[RATE_LIMIT_EDIT_ID])
            {
                limit.Text = Convert.ToString(pipLimit * 10); //HACK Otherwise it sets 10 less
                TestContext.Out.WriteLine("\r\nInstal value Limit in pip - " + pipLimit);
            }
        }
        #endregion

        #region Secondary functions
        private void OpenAndFindWindow(TradingStationWindow application)
        {
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressTradingDealingRatesMarketOrder(application);

            Waiter.WaitingCondition dialogExistsCondition = () => 
                (application.GetDialog(CREATE_MARKET_ORDER_TITLE)) != null;
            if (!Waiter.Wait(dialogExistsCondition))
            {
                throw new Exception("not find window Create Market Order");
            }
            else
            {
                dialog = application.GetDialog(CREATE_MARKET_ORDER_TITLE);
                TestContext.Out.WriteLine("\r\nA dialog Crate Market Order appeared");
            }
        }

        public bool CloseDialog(TradingStationWindow application)
        {
            dialog[OK_BUTTON].Click();
            TestContext.Out.WriteLine("\r\nPress OK in dialog Create Market Order");
            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                dialog[OK_BUTTON].Click();
                TestContext.Out.WriteLine(
                    "\r\nPress OK second time in dialog Create Market Order");
                Waiter.WaitingCondition conditionSecondChance = () => dialog.Exists == false;
                if (!Waiter.Wait(conditionSecondChance, 5))
                {
                    if (!NoTradeblePriceError())
                    {
                        dialog[CANCEL_BUTTON].Click();
                        return false;
                    }

                    Dialog dialogNoLongerBeHit = application.GetDialog(FXCM_ERROR);
                    if (dialogNoLongerBeHit[OK_BUTTON].Exists)
                    {
                        dialogNoLongerBeHit[OK_BUTTON].Click();
                        Waiter.WaitingCondition conditionDisepear = () => dialogNoLongerBeHit.Exists == false;
                        if (!Waiter.Wait(conditionDisepear, 5))
                        {
                        }
                        CloseDialog(application);
                    }
                }
            }
            return true;
        }

        private bool NoTradeblePriceError()
        {
            using (Dialog dialogTradeblePrice = new Dialog())
            {
                Waiter.WaitingCondition conditionTradeblePrice = () =>
                    dialogTradeblePrice.Exists == true;
                if (!Waiter.Wait(conditionTradeblePrice, 5))
                {
                    if (dialogTradeblePrice[1001].Text.Contains(ERROR_NO_TRADABLE_PRICE))
                    {
                        string symbol = dialog[SYMBOL_COMBOBOX_ID].Text;
                        TestContext.Out.WriteLine(
                            "\r\nApear ERROR - There is no tradeble price for symbol - "
                            + symbol);
                        dialogTradeblePrice[OK_BUTTON].Click();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                return true;
            }
        }
        #endregion

        #region return value
        private string ReturnAccountValue()
        {
            using (ComboBox selectSymbol = (ComboBox)dialog[ACCOUNT_ID])
            {
                string returnValue = selectSymbol.Text;
                return returnValue;
            }            
        }

        private double ReturnRateNow()
        {
            using (EditBox rateValue = (EditBox)dialog[RATE_ID])
            {
                double returnValue = double.Parse(rateValue.Text);
                return returnValue;
            }            
        }


        #endregion

        #region edit value
        private void EditAmmountK(int quantity)
        {
            using (EditBox editAmount = (EditBox)dialog[AMOUNT_K_COMBOBOX_ID])
            {
                editAmount.Text = Convert.ToString(quantity);
                TestContext.Out.WriteLine("\r\nEdit Amount - " + quantity);
            }
        }
        #endregion

        #region select item
        private void StopLimitOff()
        {
            using (CheckBox limit = (CheckBox)dialog[LIMIT_CHECK_BOX_ID])
            {
                using (CheckBox stop = (CheckBox)dialog[STOP_CHECK_BOX_ID])
                {
                    if (limit.Checked == 1)
                    {
                        limit.Click();
                    }
                    if (stop.Checked == 1)
                    {
                        stop.Click();
                    }
                }
                TestContext.Out.WriteLine("\r\nStop/Limit OFF");
            }
        }

        public void InPipsOnCheckBoks(bool onOff)
        {
            using (CheckBox inPips = (CheckBox)dialog[IN_PIPS_CHECK_BOX_ID])
            {
                if (onOff)
                {
                    if (inPips.Checked == 0)
                    {
                        inPips.Click();
                    }
                }
                else
                {
                    if (inPips.Checked == 1)
                    {
                        inPips.Click();
                    }
                }
                TestContext.Out.WriteLine("\r\nCheckBox In Pips - " + (onOff ? "ON" : "OFF"));
            }
        }

        private void SelectSymbol(string symbol)
        {
            using (ComboBox selectSymbol = (ComboBox)dialog[SYMBOL_COMBOBOX_ID])
            {
                selectSymbol.SelectItem(symbol);
                TestContext.Out.WriteLine("\r\nSelect symbol - " + symbol);
            }
        }

        private void SelectOrderType(string orderType)
        {
            using (ComboBox selectOrderTupe = (ComboBox)dialog[ORDER_TYPE_COMBOBOX_ID])
            {
                selectOrderTupe.SelectItem(orderType);
                TestContext.Out.WriteLine("\r\nSelect Order Type - " + orderType);
            }
        }

        private void MarketRangeValue(double value)
        {
            using (EditBox marketRangeValue = (EditBox)dialog[MARKET_RANGE_VALUE])
            {
                marketRangeValue.Text = Convert.ToString(value);
                TestContext.Out.WriteLine("\r\nEnter market range value - " + value);
            } 
        }

        private void BuyOrSell(BuySellEnum buySell)
        {
            if (buySell == BuySellEnum.BUY)
            {
                dialog[BUY_RADIOBUTTON].Click();
            }
            else
            {

                dialog[SELL_RADIOBUTTON].Click();
            }
            TestContext.Out.WriteLine("\r\nSelect Buy/Sell - " +
                ((buySell == BuySellEnum.BUY) ? "BUY" : "SELL"));
        }

        #endregion

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;

namespace TSAutotests
{
    class ReportParametersDialog : IDisposable
    {
        private const int ACCOUNT_COMBO_BOX_ID = 46592;
        private const int TYPE_COMBO_BOX_ID = 46584;
        private const int FROM_DATA_TIME_PICKER_ID = 46596;
        private const int TO_DATA_TIME_PICKER_ID = 46597;
        private const int SINCE_OPEN_CHECK_BOX_ID = 46599;
        private const int NOW_CHECK_BOX_ID = 46600;
        private const int LOAD_IN_BACKGROUND_CHECK_BOX_ID = 46902;
        private const string REPORT_PARAMETERS_TITLE = "Report Parameters";
        private const string CANCEL_BUTTON = "Cancel";
        private const bool ON = true;
        private const bool OFF = false;

        private Dialog dialog;

        public ReportParametersDialog (TradingStationWindow application)
        {
            dialog = application.GetDialog(REPORT_PARAMETERS_TITLE);
            if (dialog == null)
            {
                Waiter.WaitingCondition dialogExistsCondition = () => (application.GetDialog(REPORT_PARAMETERS_TITLE)) != null;
                if (Waiter.Wait(dialogExistsCondition))
                {
                    dialog = application.GetDialog(REPORT_PARAMETERS_TITLE);
                }
            }
        }

        public void CancelDialog ()
        {
            dialog[CANCEL_BUTTON].Click();
            Waiter.WaitingCondition condition = () => dialog.Exists == false;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not close dialog Report Parametrs");
            }
        }

        public bool ValidationOfLaunchSuccess()
        {
            bool result = false;
            if (dialog!=null)
            {
                result = true;
                CancelDialog();
            }
            if (result == false)
            {
                throw new Exception("dialog not run");
            }
            return result;
        }

        #region Check Box
        private void SinceOpenCheckBox (bool onnOff)
        {
            using (CheckBox checkSinceOpen = (CheckBox)dialog[SINCE_OPEN_CHECK_BOX_ID])
            {
                if (onnOff == true)
                {
                    if (checkSinceOpen.Checked == 0)
                    {
                        checkSinceOpen.Click();
                    }
                }
                else
                {
                    if (checkSinceOpen.Checked == 1)
                    {
                        checkSinceOpen.Click();
                    }
                }
            }
        }

        private void NowCheckBox(bool onnOff)
        {
            using (CheckBox checkNow = (CheckBox)dialog[NOW_CHECK_BOX_ID])
            {
                if (onnOff == true)
                {
                    if (checkNow.Checked == 0)
                    {
                        checkNow.Click();
                    }
                }
                else
                {
                    if (checkNow.Checked == 1)
                    {
                        checkNow.Click();
                    }
                }
            }
        }
        #endregion


        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~ReportParametersDialog()
        {
            Dispose(false);
        }
#endregion
    }
}

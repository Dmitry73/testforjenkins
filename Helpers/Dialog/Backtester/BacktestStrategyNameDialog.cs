﻿using System;
using NUnit.Framework;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Threading;
using System.Collections.Generic;

namespace TSAutotests
{
    class BacktestStrategyNameDialog : IDisposable
    {
        private Dialog dialogBacktestName;
        private string BACKTEST_NAME_TITLE_DIALOG = "Backtest Strategy - ";
        private const string NEXT_BUTTON = "Next >";
        private const string START_BUTTON = "Start";

        #region Account Parametr
        private const int ACCOUNT_CURRANCY_COMBOBOX_ID = 3007;
        private const int INITIAL_BALANCE_EDIT_ID = 3008;
        private const int ACCOUNT_LOT_SIZE_COMOBOX_ID = 3009;
        private const int MAINTENANCE_TYPE_COMBO_BOX_ID = 3010;

        private const string ACCOUNT_CURRANCY_STANDART = "USD";
        private const int INITIAL_BALANCE_STANDART = 50000;
        private const string ACCOUNT_LOT_SIZE_STANDART = "1K";
        private const string MAINTENANCE_TYPE_STANDART =
            "Non-FIFO account with hedging";
        #endregion

        #region Strategy Parametrs
        private const int INSTRUMENT_COMBOBOX_ID = 500;
        private const int CURRANCY_PAIR_STANDART_INDEX = 0;
        #endregion

        #region Market Data
        private const int CUURANCY_LIST_ID = 669;
        private const int FROM_DATE_PIKER_ID = 3003;
        private const int TO_DATE_PIKER_ID = 3004;
        private const int FIND_EDIT_ID = 3005;
        #endregion

        public BacktestStrategyNameDialog(TradingStationWindow application,
            string nameStrategy)
        {
            Waiter.WaitingCondition dialogBacktestStrategyExist = () =>
                (dialogBacktestName = application.GetDialog(
                    BACKTEST_NAME_TITLE_DIALOG + nameStrategy)) != null;
            if (!Waiter.Wait(dialogBacktestStrategyExist))
            {
                throw new Exception("Not appear dialog Backtest Strategy - NAME");
            }
        }

        public void NoParamatrsTest()
        {
            Waiter.WaitingCondition conditionStart = () =>
                NextDoStart(dialogBacktestName) == true;
            if (Waiter.Wait(conditionStart, 40))
            {
                dialogBacktestName[START_BUTTON].Click();
            }
        }


        public bool RunAndCheckDefault(TradingStationWindow application,
            string nameStrategy)
        {
            DateTime fromDate = new DateTime(2017, 01, 01);
            string fromDateForCheck = "01.01.2017";

            DateTime toDate = new DateTime(2017, 07, 07);
            string toDateForCheck = "07.07.2017";

            string instrument = GeneralInstallOption(fromDate, toDate,
                CURRANCY_PAIR_STANDART_INDEX);

            bool result = false;
            using (var backtestingTab = new BacktestingDashboardTab(application))
            {
                result = backtestingTab.CheckStrategy(nameStrategy,
                    instrument, fromDateForCheck, toDateForCheck);
            }
            return result;
        }

        public bool RunAndCheckParametr(TradingStationWindow application,
            string nameStrategy, DateTime fromDate, DateTime toDate, 
            int instrumentIndex)
        {
            string instrument = GeneralInstallOption(fromDate, toDate, 
                instrumentIndex);

            string fromDateForCheck = ConvertData(fromDate);
            string toDateForCheck = ConvertData(toDate);

            bool result = false;
            using (var backtestingTab = new BacktestingDashboardTab(application))
            {
                result = backtestingTab.CheckStrategy(nameStrategy,
                    instrument, fromDateForCheck, toDateForCheck);
            }
            return result;
        }

        private string ConvertData(DateTime data)
        {
            return (data.Day.ToString().Length == 1? "0" : "") + (data.Day) + "." +
                (data.Month.ToString().Length == 1 ? "0" : "") + data.Month +
                "." + data.Year;
        }


        public string GeneralInstallOption(DateTime fromData, DateTime toDate, 
            int instrumentIndex)
        {
            AccountParametrStandart();
            Thread.Sleep(2000);
            string instrument = StrategyParamters(instrumentIndex);
            Thread.Sleep(2000);
            MarketData(fromData, toDate);
            Waiter.WaitingCondition conditionStart = () =>
                NextDoStart(dialogBacktestName) == true;
            if (Waiter.Wait(conditionStart, 40))
            {
                dialogBacktestName[START_BUTTON].Click();
            }
            return instrument;
        }

        public void AccountParametrStandart()
        {
            AccountParametr(ACCOUNT_CURRANCY_STANDART, INITIAL_BALANCE_STANDART,
                ACCOUNT_LOT_SIZE_STANDART, MAINTENANCE_TYPE_STANDART);
            dialogBacktestName[NEXT_BUTTON].Click();
            Thread.Sleep(1000);
        }
        public void AccountParametr(string accountCurrancy, int
            initialBalance, string accountLotSize, string maintenceType)
        {
            AccountCurrencyComboBox(accountCurrancy);
            InitialBalance(initialBalance);
            AccountLotSize(accountLotSize);
            MaintenanceType(maintenceType);
        }

        public string StrategyParamters(int instrumentIndex)
        {
            ListBox listBoxParametrs = null;
            Waiter.WaitingCondition findListbox = () =>
            (listBoxParametrs = (ListBox)FindChildElement(
                dialogBacktestName, "ListBox")) != null;
            if (!Waiter.Wait(findListbox))
            {
                throw new Exception("Not appear ListBox Paramters Strategy");
            }

            listBoxParametrs.Dispose();
            string instrument = IntrumentComboBox(instrumentIndex);
            dialogBacktestName[NEXT_BUTTON].Click();
            return instrument;
        }

        public void MarketData(DateTime fromDate, DateTime toDate)
        {
            FindEdit("U");

            List<string> CurrancyPairs = new List<string>() { "EUR/USD",
            "EUR/JPY", "AUD/CAD", "USD/JPY"};
            ListBoxCurrancyPair(CurrancyPairs);

            FromDataPiker(fromDate);
            ToDataPiker(toDate);
        }


        private void ListBoxCurrancyPair(List<string> CurrancyPairs)
        {
            using (ListBox listBoxCurrancyPair = (ListBox)FindChildElementID(
            dialogBacktestName, "ListBox", CUURANCY_LIST_ID))
            {
                for (int i = 0; i < listBoxCurrancyPair.ItemCount; i++)
                {
                    foreach (string currancyPair in CurrancyPairs)
                    {
                        if (listBoxCurrancyPair.GetItem(i).Equals(currancyPair))
                        {
                            listBoxCurrancyPair.DoubleClickItem(i, InputEventModifiers.LeftButton);
                            Thread.Sleep(500);
                        }
                    }
                }
            }
        }

        private string IntrumentComboBox(int currancyPairIndex)
        {
            using (ComboBox instrument = (ComboBox)FindChildElementID(
                dialogBacktestName, "ComboBox", INSTRUMENT_COMBOBOX_ID)) //not find by ID((((
            {
                instrument.SelectIndex(currancyPairIndex);
                return instrument.Text;
            }
        }

        private void AccountCurrencyComboBox(string currancy)
        {
            CarcasComboBox(ACCOUNT_CURRANCY_COMBOBOX_ID, currancy);
        }

        private void AccountLotSize(string size)
        {
            CarcasComboBox(ACCOUNT_LOT_SIZE_COMOBOX_ID, size);
        }

        private void MaintenanceType(string type)
        {
            CarcasComboBox(MAINTENANCE_TYPE_COMBO_BOX_ID, type);
        }

        private void FindEdit(string findText)
        {
            using (EditBox find = (EditBox)FindChildElementID(
                           dialogBacktestName, "Edit", FIND_EDIT_ID))
            {
                find.Text = findText;
            }
        }

        private void InitialBalance(int balance)
        {
            using (EditBox editBalance = (EditBox)dialogBacktestName[
                INITIAL_BALANCE_EDIT_ID])
            {
                editBalance.Text = balance.ToString();
            }
        }

        private void CarcasComboBox(int idComboBox, string selectItem)
        {
            using (ComboBox select = (ComboBox)dialogBacktestName[idComboBox])
            {
                select.SelectItem(selectItem);
            }
        }

        private void FromDataPiker(DateTime data)
        {
            DataPickerCarcas(FROM_DATE_PIKER_ID, data);
        }

        private void ToDataPiker(DateTime data)
        {
            DataPickerCarcas(TO_DATE_PIKER_ID, data);
        }


        private void DataPickerCarcas(int ID, DateTime data)
        {
            using (DateTimePicker dataPiker = (DateTimePicker)FindChildElementID(
               dialogBacktestName, "SysDateTimePick32", ID))
            {
                DateTime thisDate1 = new DateTime(2017, 01, 01);
                dataPiker.Value = data;
                TestContext.Out.WriteLine("\r\nDataTime install - " +
                    data.ToString());
            }
        }

        private Window FindChildElement(Window window, string nameClass)
        {
            WindowCollection children = window.GetChildren(false);
            foreach (Window child in children)
            {
                if (child.ClassName.Equals(nameClass))
                {
                    if (child.Enabled)
                    {
                        return child;
                    }
                }
                Window returnObject = FindChildElement(child, nameClass);
                if (returnObject != null)
                {
                    if (child.Enabled)
                    {
                        return returnObject;
                    }
                }
            }
            return null;
        }

        private Window FindChildElementID(Window window, string nameClass,
            int ID)
        {
            WindowCollection children = window.GetChildren(false);
            foreach (Window child in children)
            {
                if ((child.ClassName.Equals(nameClass)) && (child.ID == ID))
                {
                    return child;
                }
                Window returnObject = FindChildElementID(child, nameClass, ID);
                if ((returnObject != null) && (returnObject.ID == ID))
                {
                    return returnObject;
                }
            }
            return null;
        }

        private bool NextDoStart(Dialog dialog)
        {
            if (dialog[NEXT_BUTTON].Enabled)
            {
                dialog[NEXT_BUTTON].Click();
            }
            Thread.Sleep(300);
            return dialog[START_BUTTON].Enabled;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (dialogBacktestName != null)
                    {
                        dialogBacktestName.Dispose();
                    }
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

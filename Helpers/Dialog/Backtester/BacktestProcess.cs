﻿using NUnit.Framework;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;
using System.Threading;

namespace TSAutotests
{
    class BackTestProcess : IDisposable
    {
        private const string WINDOW_STRATEGY_BACKTESTER_TITLE = "Strategy Backtester";
        private const string BACKTEST_STRATEGY_DIALOG_TITLE = "Backtest Strategy";
        private const string GROUP_STRATEGIES = "Strategies";
        private const string GROUP_ALERTS_AND_SIGNALS = "Alert and Signals";
        private const int FIND_EDIT_ID = 3002;
        private const string NEXT_BUTTON = "Next >";
        private const string START_BUTTON = "Start";
        #region Account Parameters
        private const int ACCOUNT_CURAENCY_COMBOBOX_ID = 3007;
        private const int INITAL_BALANCE_EDIT_ID = 3008;
        private const int ACCOUNT_LOT_SIZE = 3009;
        private const int MAINTENANCE_TYPE_COMBOBOX_ID = 3010;
        private const string ACCOUNT_CURAENCY_USD = "USD";
        private const string ACCOUNT_LOT_SIZE_1K = "1K";
        private const string MAINTENANCE_TYPE_Y_ACCOUNT =
            "Non-FIFO account with hedging";
        #endregion
        protected List<string> CurrancyPairs = new List<string>() { "EUR/USD",
            "EUR/JPY", "AUD/CAD", "USD/JPY"};

        private BacktesterWindow backTesterWindow;
        private Dialog dialogBacktestStrategy;

        public BackTestProcess()
        {
            Waiter.WaitingCondition windowsBacktester = () =>
                (backTesterWindow = TradingStation.FindBacktesterWindow(
                WINDOW_STRATEGY_BACKTESTER_TITLE)) != null;
            if (!Waiter.Wait(windowsBacktester))
            {
                throw new Exception("Not appear window Backtester");
            }

            Waiter.WaitingCondition dialogBacktestStrategyExist = () =>
                (dialogBacktestStrategy = backTesterWindow.GetDialog(
                BACKTEST_STRATEGY_DIALOG_TITLE)) != null;
            if (!Waiter.Wait(dialogBacktestStrategyExist))
            {
                throw new Exception("Not appear dialog Backtester");
            }
        }

        private Window FindChildElement(Window window, string nameClass)
        {
            WindowCollection children = window.GetChildren(false);
            foreach (Window child in children)
            {
                if (child.ClassName.Equals(nameClass))
                {
                    return child;
                }
                Window returnObject = FindChildElement(child, nameClass);
                if (returnObject != null)
                {
                    return returnObject;
                }
            }
            return null;
        }

        public void SettingStrategies(string nameStrategy)
        {
            NameStrategy(nameStrategy);

            AccountParametrs();

            StrategyParameters(5, 35, 3);

            ListBoxCurrancy();

            if (dialogBacktestStrategy[START_BUTTON].Enabled)
            {
                dialogBacktestStrategy[START_BUTTON].Click();
            }
        }

        private void ListBoxCurrancy()
        {
            using (EditBox find = (EditBox)FindChildElement(
                dialogBacktestStrategy, "Edit")) //not find by ID((((
            {
                find.Text = "U";
            }

            using (ListBox listBox = (ListBox)FindChildElement(
                dialogBacktestStrategy, "ListBox")) //not find by ID((((
            {
                for (int i = 0; i < listBox.ItemCount; i++)
                {
                    foreach (string currancyPair in CurrancyPairs)
                    {
                        if (listBox.GetItem(i).Equals(currancyPair))
                        {
                            listBox.DoubleClickItem(i, InputEventModifiers.LeftButton);
                            Thread.Sleep(500);
                        }
                    }
                }
            }
            
            dialogBacktestStrategy[NEXT_BUTTON].Click();
        }

        private void StrategyParameters(int shortEMAperiods, int longEMAperiods,
            int signalLinePeriods)
        {
            ListBox listBoxParametrs = null;
            Waiter.WaitingCondition findListbox = () =>
            (listBoxParametrs = (ListBox)FindChildElement(
                dialogBacktestStrategy, "ListBox")) != null;
            if (!Waiter.Wait(findListbox))
            {
                throw new Exception("Not appear ListBox Paramters Strategy");
            }

            listBoxParametrs.ClickItem(1, InputEventModifiers.LeftButton);
            listBoxParametrs.StrokeA(shortEMAperiods.ToString());

            listBoxParametrs.ClickItem(2, InputEventModifiers.LeftButton);
            listBoxParametrs.StrokeA(longEMAperiods.ToString());

            listBoxParametrs.ClickItem(3, InputEventModifiers.LeftButton);
            listBoxParametrs.StrokeA(signalLinePeriods.ToString());
            listBoxParametrs.Dispose();
            dialogBacktestStrategy[NEXT_BUTTON].Click();
        }

        private void AccountParametrs()
        {
            //NOT FIND BY ID
            // AccountCurancyComboBox(ACCOUNT_CURAENCY_USD);
            //AccountLotSizeComboBox(ACCOUNT_LOT_SIZE_1K);
            //MaintenceTypeComboBox(MAINTENANCE_TYPE_Y_ACCOUNT);
            dialogBacktestStrategy[NEXT_BUTTON].Click();
        }


        private void NameStrategy(string nameStrategy)
        {
            FindStrategy(nameStrategy);
            using (ListView viewNameStrategy = (ListView)
                dialogBacktestStrategy.FindChild("SysListView32", "0", true))
            {
                viewNameStrategy.ClickCell(0, 0, InputEventModifiers.LeftButton);
            }
            dialogBacktestStrategy[NEXT_BUTTON].Click();
        }

        private void FindStrategy(string nameFindStrategy)
        {
            using (EditBox editAmount = (EditBox)dialogBacktestStrategy[FIND_EDIT_ID])
            {
                editAmount.Text = nameFindStrategy;
                TestContext.Out.WriteLine("\r\nEdit Find - " + nameFindStrategy);
                Thread.Sleep(1000);  //Time for find
            }
        }

        private void AccountCurancyComboBox(string currancy)
        {
            CarcasComboBox(ACCOUNT_CURAENCY_COMBOBOX_ID, currancy);
            TestContext.Out.WriteLine("\r\nAccount currancy select - " + currancy);
        }

        private void AccountLotSizeComboBox(string lotSize)
        {
            CarcasComboBox(ACCOUNT_CURAENCY_COMBOBOX_ID, lotSize);
            TestContext.Out.WriteLine("\r\nAccount lot size select - " + lotSize);
        }

        private void MaintenceTypeComboBox(string type)
        {
            CarcasComboBox(ACCOUNT_CURAENCY_COMBOBOX_ID, type);
            TestContext.Out.WriteLine("\r\nMaintence type select - " + type);
        }

        private void CarcasComboBox(int idComboBox, string selectText)
        {
            using (ComboBox select = (ComboBox)dialogBacktestStrategy[idComboBox])
            {
                select.SelectItem(selectText);
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (dialogBacktestStrategy != null)
                    {
                        dialogBacktestStrategy.Dispose();
                    }
                    if (backTesterWindow != null)
                    {
                        backTesterWindow.Dispose();
                    }
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

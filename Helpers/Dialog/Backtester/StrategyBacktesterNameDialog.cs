﻿using NUnit.Framework;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Configuration;
using System.IO;

namespace TSAutotests
{
    class StrategyBacktesterNameDialog : IDisposable
    {
        private const string WINDOW_STRATEGY_BACKTESTER_TITLE =
            "Strategy Backtester - ";
        private BacktesterWindow backTesterWindow;


        public StrategyBacktesterNameDialog(string nameStrategy)
        {
            Waiter.WaitingCondition windowsBacktester = () =>
                (backTesterWindow = TradingStation.FindBacktesterWindow(
                WINDOW_STRATEGY_BACKTESTER_TITLE + nameStrategy)) != null;
            if (!Waiter.Wait(windowsBacktester))
            {
                throw new Exception("Not appear window Backtester");
            }
        }


        public void StatisticTab()
        {
            string nameFile = WorkFileForPathBrowser();
            string forPathInBrowser = nameFile.Remove(nameFile.Length - 4, 4);


            Window testValue = FindChildElement(backTesterWindow,
                "Internet Explorer_Server");
        }

        public string WorkFileForPathBrowser()
        {
            DateTime dataForCompare = new DateTime(1990, 1, 1);
            string fileName = "";

            string pathBeforeInstallTS =
                ConfigurationManager.AppSettings["pathBeforeInstallTS"];
            string pathToNameCloud = pathBeforeInstallTS + "\\BacktesterProjects\\TEMP ";
            DirectoryInfo dirInfo = new DirectoryInfo(pathToNameCloud);
            foreach (FileInfo file in dirInfo.GetFiles())
            {
                if (dataForCompare < Convert.ToDateTime(file.CreationTime))
                {
                    dataForCompare = Convert.ToDateTime(file.CreationTime);
                    fileName = file.Name;
                }
            }

            return fileName;
        }

        private Window FindChildElement(Window window, string nameClass)
        {
            WindowCollection children = window.GetChildren(false);
            foreach (Window child in children)
            {
                if (child.ClassName.Equals(nameClass))
                {
                    if (child.Enabled)
                    {
                        return child;
                    }
                }
                Window returnObject = FindChildElement(child, nameClass);
                if (returnObject != null)
                {
                    if (child.Enabled)
                    {
                        return returnObject;
                    }
                }
            }
            return null;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (backTesterWindow != null)
                    {
                        backTesterWindow.Dispose();
                    }
                }
                disposedValue = true;
            }
#endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

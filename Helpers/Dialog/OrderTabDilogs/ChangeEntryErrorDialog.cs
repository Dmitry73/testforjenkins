﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using NUnit.Framework;

namespace TSAutotests.CEE
{
    class ChangeEntryErrorDialog : IDisposable
    {
        private const int LABEL_FOR_CHECK_ID = 1001;
        private const string OK_BUTTON = "OK";
        private const string TITLE_DIALOG = "FXCM Trading Station Desktop";
        private const string ERROR_RATE_LABLE_TEXT = "You are either trying to place an order " +
            "too far away from the current market price, or you are trying to " +
            "place a Stop or Limit order on the wrong side of the market price. " +
            "Please change the rate or the type of order.";
        private const string ERROR_RATE_LE_SE_LABLE_TEXT = 
            "The Stop or Limit order you're entering is incorrect.";
        private const string ERROR_RATE_RE_LABLE_TEXT =
            "The Range Entry rate must be";

        private Dialog dialogError;

        public ChangeEntryErrorDialog(TradingStationWindow application)
        {
            Waiter.WaitingCondition condition = () => (dialogError = 
                application.GetDialog(TITLE_DIALOG)) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("window with an error did not appear");
            }
            else
            {
                TestContext.Out.WriteLine("\r\nDialog Error Change Entry Order appeared");
            }
        }

        public bool CheckErrorRate()
        {
            bool result = false;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Equals(ERROR_RATE_LABLE_TEXT))
            {
                TestContext.Out.WriteLine("\r\nCorrect error type");
                result = true;
            }
            PressOK();
            return result;
        }

        public bool CheckErrorRateLESE()
        {
            bool result = false;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Contains
                (ERROR_RATE_LE_SE_LABLE_TEXT))
            {
                TestContext.Out.WriteLine("\r\nCorrect error type");
                result = true;
            }
            PressOK();
            return result;
        }

        public bool CheckErrorRateRE()
        {
            bool result = false;
            if (dialogError[LABEL_FOR_CHECK_ID].Text.Contains
                (ERROR_RATE_RE_LABLE_TEXT))
            {
                TestContext.Out.WriteLine("\r\nCorrect error type");
                result = true;
            }
            PressOK();
            return result;
        }


        
        public bool EmergenceDialogError()
        {
            if (dialogError != null)
            {
                PressOK();
                return true;
            }
            PressOK();
            return false;
        }

        private void PressOK()
        {
            if (dialogError[OK_BUTTON] == null)
                throw new Exception("No OK button found");

            dialogError[OK_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialogError.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                TestContext.Out.WriteLine("\r\nNot close window with error");
                Assert.Fail();
            }
            else
            {
                TestContext.Out.WriteLine
                    ("\r\nPress button OK and dialog Change Entry Error dialog disappear");
            }

            //TO-DO: static analizer warn "Expresion 'dialogErrpr[OK_BUTTON]!=null' is always true". Check it
            if (dialogError[OK_BUTTON]!=null)
            {
                dialogError[OK_BUTTON].Click();
            }


        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogError.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}


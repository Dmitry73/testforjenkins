﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;
using TSAutotests.CEE;

namespace TSAutotests
{
    class StopLimitOrderDialog : IDisposable
    {
        private const string TITLE_DIALOG = "Stop/Limit Order";
        private const string OK_BUTTON = "OK";
        private const string CANCEL_BUTTON = "Cancel";
        private const int STOP_CHECK_BOX_ID = 49409;
        private const int IN_PIPS_PRIMARY_CHECK_BOX_ID = 49412;
        private const int LIMIT_CHECK_BOX_ID = 49413;
        private const int IN_PIPS_SECONDARY_CHECK_BOX_ID = 49416;
        private const int STOP_RATE_ID = 49410;
        private const int LIMIT_RATE_ID = 49414;
        private const bool ON = true;
        private const bool OFF = false;

        private Dialog dialogStopLimit;

        public StopLimitOrderDialog(TradingStationWindow application)
        {
            dialogStopLimit = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
        }

        public bool StopLimitPeggedTrailingOrder()
        {
            StopCheckBox(ON);
            LimitCheckBox(ON);
            int i = ReturnLimitRate().Length;
            bool result = ReturnLimitRate().Length < 7 &&
                ReturnStopRate().Length < 7 &&
                !dialogStopLimit[IN_PIPS_PRIMARY_CHECK_BOX_ID].Enabled &&
                !dialogStopLimit[IN_PIPS_SECONDARY_CHECK_BOX_ID].Enabled;
            PressCancel();
            return result;
        }


        public void EditStopLimitSecondaryInPip(double valueStop, double valueLimit)
        {
            StopCheckBox(ON);
            LimitCheckBox(ON);
            InstallStopRate(valueStop);
            InstallLimitRate(valueLimit);
            PressOK();
        }


        private void StopCheckBox(bool onOff)
        {
            CarcasCheckBox(STOP_CHECK_BOX_ID, onOff);
        }

        private void LimitCheckBox(bool onOff)
        {
            CarcasCheckBox(LIMIT_CHECK_BOX_ID, onOff);
        }

        private void PrimaryInPips(bool onOff)
        {
            CarcasCheckBox(IN_PIPS_PRIMARY_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox primary In pips " + 
                (onOff ? "ON" : "OFF"));
        }

        private void SecondaryInPips(bool onOff)
        {
            CarcasCheckBox(IN_PIPS_PRIMARY_CHECK_BOX_ID, onOff);
            TestContext.Out.WriteLine("\r\nCheckBox secondary In pips " +
                (onOff ? "ON" : "OFF"));
        }

        private void InstallStopRate(double valueStop)
        {
            using (EditBox editRate = (EditBox)dialogStopLimit[STOP_RATE_ID])
            {
                editRate.Text = (valueStop * 10).ToString(); //*10 HACK
            }            
        }

        private void InstallLimitRate(double valueLimit)
        {
            using (EditBox editRate = (EditBox)dialogStopLimit[LIMIT_RATE_ID])
            {
                editRate.Text = (valueLimit * 10).ToString(); //*10 HACK
            }
        }

        private string ReturnStopRate()
        {
            return CarcasReturnValueFromEdit(STOP_RATE_ID);
        }

        private string ReturnLimitRate()
        {
            return CarcasReturnValueFromEdit(LIMIT_RATE_ID);
        }

        private string CarcasReturnValueFromEdit(int ID)
        {
            using (EditBox editRate = (EditBox)dialogStopLimit[ID])
            {
                string returnValue = editRate.Text;
                return returnValue;
            }
        }

        private void CarcasCheckBox(int idCheckBox, bool onOff)
        {
            using (CheckBox carcas = (CheckBox)dialogStopLimit[idCheckBox])
            {
                if (onOff == true)
                {
                    if (carcas.Checked == 0) { carcas.Click(); }
                }
                else
                {
                    if (carcas.Checked == 1) { carcas.Click(); }
                }
            }
        }

        private void PressOK()
        {
            dialogStopLimit[OK_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialogStopLimit.Exists == false;
            if (!Waiter.Wait(condition))
            {
                dialogStopLimit[OK_BUTTON].Click();
                Waiter.WaitingCondition conditionSecondChance = () => 
                    dialogStopLimit.Exists == false;
                if (!Waiter.Wait(conditionSecondChance))
                {
                    TestContext.Out.WriteLine("\r\nNot close window before press OK");
                    Assert.Fail();
                }
                else
                {
                    TestContext.Out.WriteLine(
                    "\r\nPress button OK and dialog Stop/Limit Order disapear");
                }
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nPress button OK and dialog Stop/Limit Order disapear");
            }
        }

        private void PressCancel()
        {
            dialogStopLimit[CANCEL_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialogStopLimit.Exists == false;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not close window OK dialog");
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nPress button Cancel and dialog Stop/Limit Order disapear");
            }
        }

        #region IDisposable 
        protected bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogStopLimit.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

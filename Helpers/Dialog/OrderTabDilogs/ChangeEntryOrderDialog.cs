﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;
using TSAutotests.CEE;

namespace TSAutotests
{
    class ChangeEntryOrderDialog : IDisposable
    {
        private const string TITLE_DIALOG = "Change Entry Order";
        private const string OK_BUTTON = "OK";
        private const string CANCEL_BUTTON = "Cancel";
        private const string ORDER_ID = "Order ID:";
        private const string AMOUNT_K_EDIT = "Amount(K):";
        private const int AMOUNT_K_EDIT_ID = 1001;
        private const int IN_PIP_CHECK_BOX_ID = 45735;
        private const string RATE_EDIT = "Rate:";
        private const string RATE_EDIT_IN_PIP = "Rate (in pips):";
        private const int RATE_EDIT_ID = 5201;
        private const string TRAILING_COMBO_BOX = "Trailing:";
        private const string TIME_IN_FORCE = "Time In Force:";
        private const string ORDER_TYPE_ENTRY = "Entry";
        private const string ORDER_TYPE_RANGE_ENTRY = "Range Entry";
        private const string TIME_IN_FORCE_GTD_VALUE = "GTD";
        private const string TIME_IN_FORCE_GTC_VALUE = "GTC";
        private const int IN_PIPS_CHECK_BOX = 45735;

        private Dialog dialogChangeEntry;

        public ChangeEntryOrderDialog(TradingStationWindow application)
        {
            dialogChangeEntry = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
            if (dialogChangeEntry != null)
            {
                TestContext.Out.WriteLine("\r\nDialog Change Entry Order appeared");
            }
        }


        public bool CheckErrorChangeRate(TradingStationWindow application,
            double rateCoefficient)
        {
            InstallRateEdit(rateCoefficient);
            dialogChangeEntry[OK_BUTTON].Click();
            bool result = false;
            using (var errorDialog = new ChangeEntryErrorDialog(application))
            {
                result = errorDialog.CheckErrorRate();
            }
            PressCancel();
            return result;
        }

        public bool ChangeRatePeggedNoError(TradingStationWindow application,
            string changeRate)
        {
            InstallRateInPipsEdit(changeRate);
            dialogChangeEntry[OK_BUTTON].Click();
            bool result;
            Waiter.WaitingCondition condition = () =>
                dialogChangeEntry.Exists == false;
            if (!Waiter.Wait(condition, 3))
            {
                using (var errorDialog = new ChangeEntryErrorDialog(application))
                {
                    result = !errorDialog.EmergenceDialogError();
                    PressCancel();
                }
            }
            else
            {
                result = true;
                TestContext.Out.WriteLine("\r\nCorrect change rate without error");
            }
            return result;
        }

        public bool DisableAndOffInPip(double rateCoefficient)
        {
            bool result = false;
            InstallRateEdit(rateCoefficient);
            using (CheckBox carcas = (CheckBox)dialogChangeEntry[IN_PIP_CHECK_BOX_ID])
            {
                if (carcas.Checked == 0) { result = true; }
                else { TestContext.Out.WriteLine("checked in pips error"); }
                //result = result && !carcas.Enabled; //not disable check box, but it's hidden
                if (result == false) { TestContext.Out.WriteLine("In Pips enable"); }
                Waiter.WaitingCondition condition = () => dialogChangeEntry["OK"].Enabled;
                if (!Waiter.Wait(condition))
                {
                    throw new Exception("Not enable button OK");
                }
            }
            PressOK();
            return result;
        }

        public double ChangeRate(double rateCoeff)
        {
            double installRate = InstallRateEdit(rateCoeff);
            TestContext.Out.WriteLine("\r\ninstall rate - " + installRate);
            PressOK();
            return installRate;
        }

        public bool DisableTrailing()
        {
            bool result = !dialogChangeEntry[TRAILING_COMBO_BOX].Enabled;
            if (result)
            {
                TestContext.Out.WriteLine("\r\ntrailing in dialog not available");
            }
            PressCancel();
            return result;
        }

        public bool DisableInPip()
        {
            bool result = !dialogChangeEntry[IN_PIP_CHECK_BOX_ID].Enabled;
            return result;
        }

        public bool RangeEntryError(TradingStationWindow application, double rateCoeff)
        {
            InstallRateEdit(rateCoeff);
            dialogChangeEntry[OK_BUTTON].Click();
            bool result = false;
            using (var errorDialog = new ChangeEntryErrorDialog(application))
            {
                result = errorDialog.EmergenceDialogError();
            }
            PressCancel();
            return result;
        }

        public void ChangeAmount(int changeAmount)
        {
            InstallAmount(changeAmount);
            PressOK();
        }

        public void ChangeTrailing(string trailing)
        {
            SelectTrailing(trailing);
            PressOK();
        }

        public bool StopLimitNonPeggedDisableTrailing()
        {
            bool result = !dialogChangeEntry[TRAILING_COMBO_BOX].Enabled;
            TestContext.Out.WriteLine("\r\ntrailing in dialog not available");
            PressCancel();
            return result;
        }

        public void InPipsCheckBoxOnOff(bool onOff, double rateCoeff = -0.01)
        {
            if ((ReturnRate().Length == 7))
            {
                InstallRateEdit(rateCoeff);
            }
            Waiter.WaitingCondition condition = () =>
                dialogChangeEntry[IN_PIPS_CHECK_BOX].Enabled;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Check box In Pips not enabled");
            }
            if (dialogChangeEntry[IN_PIPS_CHECK_BOX].Enabled)
            {
                InPipCheckBox(onOff);
                if (!dialogChangeEntry[OK_BUTTON].Enabled)
                {
                    InstallRateEdit(rateCoeff);
                }
            }
            PressOK();
        }

        public string ReturnValueTrailing()
        {
            return dialogChangeEntry[TRAILING_COMBO_BOX].Text;
        }
        private void InstallAmount(int amount)
        {
            using (EditBox editRange = (EditBox)dialogChangeEntry[AMOUNT_K_EDIT_ID])
            {
                editRange.Text = Convert.ToString(amount);
                TestContext.Out.WriteLine("\r\nInstall amount - " + amount);
            }
        }

        private void SelectTrailing(string trailing)
        {
            CarcasComboBox(TRAILING_COMBO_BOX, trailing);
        }

        private void CarcasComboBox(string idComboBox, string item)
        {
            using (ComboBox select = (ComboBox)dialogChangeEntry[idComboBox])
            {
                select.SelectItem(item);
            }
        }

        public void InPipCheckBox(bool onOff)
        {
            CarcasCheckBox(IN_PIPS_CHECK_BOX, onOff);
            TestContext.Out.WriteLine(
                "\r\nCheck box In pip in dialog Change Entry Order - " +
                (onOff ? "ON" : "OFF"));
        }

        private void CarcasCheckBox(int idCheckBox, bool onOff)
        {
            using (CheckBox carcas = (CheckBox)dialogChangeEntry[idCheckBox])
            {
                if (onOff == true)
                {
                    if (carcas.Checked == 0) { carcas.Click(); }
                }
                else
                {
                    if (carcas.Checked == 1) { carcas.Click(); }
                }
            }
        }

        public void PressCancel()
        {
            dialogChangeEntry[CANCEL_BUTTON].Click();

            Waiter.WaitingCondition condition = () =>
                dialogChangeEntry.Exists == false;
            if (!Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine("\r\nSecond chance - press Cancel");
                dialogChangeEntry[CANCEL_BUTTON].Click();
                Waiter.WaitingCondition conditionSecondChance = () =>
                    dialogChangeEntry.Exists == false;
                if (!Waiter.Wait(conditionSecondChance))
                {
                    throw new Exception("Not close window OK dialog");
                }
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nPress button Cancel and dialog Change Entry Order disapear");
            }
        }

        public void PressOK()
        {
            Waiter.WaitingCondition conditionEnable = () =>
                dialogChangeEntry[OK_BUTTON].Enabled;
            if (!Waiter.Wait(conditionEnable))
            {
                throw new Exception("Not enable OK button");
            }

            dialogChangeEntry[OK_BUTTON].Click();

            Waiter.WaitingCondition conditionClose = () =>
                dialogChangeEntry.Exists == false;
            if (!Waiter.Wait(conditionClose))
            {
                TestContext.Out.WriteLine("\r\nSecond chance - press OK");
                dialogChangeEntry[OK_BUTTON].Click();
                Waiter.WaitingCondition conditionSecondChance = () =>
                    dialogChangeEntry.Exists == false;
                if (!Waiter.Wait(conditionSecondChance))
                {
                    throw new Exception("Not close window OK dialog");
                }
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nPress button OK  and dialog Change Entry Order disapear");
            }
        }

        public string ReturnRate()
        {
            using (EditBox editRate = (EditBox)dialogChangeEntry[RATE_EDIT_ID])
            {
                string returnValue = editRate.Text;
                return returnValue;
            }
        }

        public bool ErrorEditRate(TradingStationWindow application, double
            rateCoefficient)
        {
            bool result = false;
            InstallRateEdit(rateCoefficient);
            dialogChangeEntry[OK_BUTTON].Click();
            using (var errorDialog = new ChangeEntryErrorDialog(application))
            {
                result = errorDialog.CheckErrorRateLESE();
            }
            PressCancel();
            return result;
        }

        public bool ErrorEditRateRE(TradingStationWindow application, double
            rateCoefficient)
        {
            bool result = false;
            InstallRateEdit(rateCoefficient);
            dialogChangeEntry[OK_BUTTON].Click();
            using (var errorDialog = new ChangeEntryErrorDialog(application))
            {
                result = errorDialog.CheckErrorRateRE();
            }
            PressCancel();
            return result;
        }

        public bool ErrorEditRateREInPip(TradingStationWindow application, double
           rateCoefficient)
        {
            bool result = false;
            InPipCheckBox(false);
            Waiter.WaitingCondition conditionNonPegged = () => ReturnRate().Length == 7;
            Waiter.Wait(conditionNonPegged);
            InstallRateEdit(rateCoefficient);
            InPipCheckBox(true);
            Waiter.WaitingCondition conditionPegged = () => ReturnRate().Length < 7;
            Waiter.Wait(conditionPegged);
            dialogChangeEntry[OK_BUTTON].Click();
            using (var errorDialog = new ChangeEntryErrorDialog(application))
            {
                result = errorDialog.CheckErrorRateRE();
            }
            PressCancel();
            return result;
        }

        public bool ErrorEditRateInPip(TradingStationWindow application, double
           rateCoefficient)
        {
            bool result = false;
            InPipCheckBox(false);
            Waiter.WaitingCondition conditionNonPegged = () => ReturnRate().Length == 7;
            Waiter.Wait(conditionNonPegged);
            InstallRateEdit(rateCoefficient);
            InPipCheckBox(true);
            Waiter.WaitingCondition conditionPegged = () => ReturnRate().Length < 7;
            Waiter.Wait(conditionPegged);
            dialogChangeEntry[OK_BUTTON].Click();
            using (var errorDialog = new ChangeEntryErrorDialog(application))
            {
                result = errorDialog.CheckErrorRateLESE();
            }
            PressCancel();
            return result;
        }
        private double InstallRateEdit(double rateCoefficient)
        {
            using (EditBox editRate = (EditBox)dialogChangeEntry[RATE_EDIT])
            {
                double installValue = double.Parse(editRate.Text) +
                    double.Parse(editRate.Text) * rateCoefficient;
                installValue = MathHelper.RoundingDependingOnSignsAfterComma(installValue);
                editRate.Text = Convert.ToString(installValue);
                TestContext.Out.WriteLine(
                    "\r\nChange rate in dialog Change Entry Order on rate - " + installValue);
                return installValue;
            }
        }

        private void InstallRateInPipsEdit(string installRate)
        {
            using (EditBox editRate = (EditBox)dialogChangeEntry[RATE_EDIT_IN_PIP])
            {
                editRate.Text = installRate;
                TestContext.Out.WriteLine(
                  "\r\nChange rate in pips in dialog Change Entry Order on rate - "
                  + installRate);
            }
        }
        #region IDisposable 
        protected bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogChangeEntry.Dispose();
                }
                disposedValue = true;
            }
#endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

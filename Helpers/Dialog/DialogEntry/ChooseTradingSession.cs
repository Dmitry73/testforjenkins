﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;

namespace TSAutotests.Login
{
    class ChooseTradingSession : IDisposable
    {
        private Dialog dialogSession;
        private const string TITLE_DIALOG = "Choose Trading Session";
        private const string OK_BUTTON = "OK";

        public ChooseTradingSession(TradingStationWindow application)
        {
            dialogSession = Waiter.WaitDialogAppear(application, TITLE_DIALOG);
        }

        public void SelectNameDialog (string nameSession)
        {
            using (ListView sessionSymbolView = (dialogSession.FindChild("SysListView32",
                ".*", true)) as ListView)
            {
                for (int i = 0; i < sessionSymbolView.RowCount; i++)
                {
                    string valueRow = sessionSymbolView.CellValue(i, 0);
                    if (nameSession.Equals(valueRow))
                    {
                        sessionSymbolView.ClickCell(i, 0, InputEventModifiers.LeftButton);
                        break;
                    }
                }
            }
            PressOK();
        }

        private void PressOK()
        {
            dialogSession[OK_BUTTON].Click();

            Waiter.WaitingCondition condition = () => dialogSession.Exists == false;
            if (!Waiter.Wait(condition, 5))
            {
                TestContext.Out.WriteLine(
                    "\r\nNot close window with error");
                Assert.Fail();
            }
        }


        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialogSession.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}


﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;


namespace TSAutotests.Login
{
    class LoginDialog : IDisposable
    {
        private const string LOGIN_EDIT = "Login:";
        private const string PASSWORD_EDIT = "Password:";
        private const int CONECTION_COMBOBOX_EDIT_ID = 42101;
        private const string DIALOG_WINDOW_TITLE = "Login";
        private const string LOGIN_BUTTON = "Login";
        private const string PLEASE_WAIT_WINDOW_TITLE = "Please wait...";
        private const string LOGIN_ERROR_WINDOW_TITLE = "FXCM Trading Station";

        private Dialog dialog;

        public LoginDialog(TradingStationWindow application)
        {
            dialog = Waiter.WaitDialogAppear(application, DIALOG_WINDOW_TITLE);
            if (dialog == null)
            {
                var menuLogin = new ApplicationMenuHelper();
                menuLogin.PressLogin(application);
                dialog = Waiter.WaitDialogAppear(application, DIALOG_WINDOW_TITLE);
                if (dialog == null)
                {
                    throw new Exception("not find window login");
                }
                else
                {
                    TestContext.Out.WriteLine("\r\nDialog login appeared");
                }
            }
        }

        public bool Login(string login, string password, string ConnectionType, 
            TradingStationWindow application, int timeout = 10)
        {
            using (ComboBox selectConnection = (ComboBox)dialog[CONECTION_COMBOBOX_EDIT_ID])
            {
                selectConnection.SelectItem(ConnectionType);
                dialog[LOGIN_EDIT].Text = login;
                dialog[PASSWORD_EDIT].StrokeA(password);

                dialog[LOGIN_BUTTON].Click();

                if (WaitLoginProccesing(timeout, application))
                {
                    TestContext.Out.WriteLine("\r\nPress button Login and dialog Login disappear");
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private static bool WaitLoginProccesing(int timeout, 
            TradingStationWindow application)
        {
            DateTime localDate = DateTime.Now;
            Dialog dlg = null;

            using (dlg = Waiter.WaitDialogAppear(application, PLEASE_WAIT_WINDOW_TITLE, timeout))
            {
                if (dlg != null)
                {
                    Waiter.WaitingCondition dialogExistsCondition = () => !dlg.Exists;
                    if (Waiter.Wait(dialogExistsCondition))
                    {
                        TestContext.Out.WriteLine("\r\ndilog wait proccesing disappear");
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return false;
        }

        private bool WaitAccountIsReady(TradingStationWindow application)
        {
            bool result;
            using (var accountsTab = new AccountsTab(application))
            {
                Waiter.WaitingCondition condition = () => accountsTab.CheckTabIsReady();
                result = Waiter.Wait(condition, 20);
            }
            return result;
        }

        private static bool CheckLogon(TradingStationWindow application)
        {
            using (Dialog dlgInCorrect = application.GetDialog(LOGIN_ERROR_WINDOW_TITLE))
            {
                if (dlgInCorrect == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    dialog.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;
using System;
using System.Collections.Generic;


namespace TSAutotests
{
    class SimpleDealingRatesTab : IDisposable
    {
        private const string TAB_GRID = "Rates";
        private const string TAB_TITLE = "Simple Dealing Rates";
        private const int SYMBOL_COLUMN = 0;
        private const int SELL_COLUMN = 1;
        private const int BUY_COLUMN = 2;
        private const int PIP_COST_COLUMN = 10;
        private const string CREATE_MARKET_ORDER_POPUP_MENU = "Create Market Order...";
        private const string CREATE_MARKET_ORDER_TITLE = "Create Market Order";
        private const string ERROR_NO_TRADABLE_PRICE = "There is no tradable price.";
        private const string OK_BUTTON = "OK";
        private const int LABEL_ERROR = 1001;

        private TradingStationSimpleView simpleDealingRatesView;
        private TradingStationTab simpleDealingRatesTabClick;

        public SimpleDealingRatesTab(TradingStationWindow application)
        {
            Waiter.WaitingCondition condition = () => (simpleDealingRatesView = 
                application.FindGridView(TradingStationViewType.Rates)) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("zero value rate in tab Simple Dealing Rates");
            }
            simpleDealingRatesTabClick = application.FindTab(TAB_TITLE);
        }

        public bool PressBuyOrSellOnCurrancyPair(string symbol, BuySellEnum buySell, 
            TradingStationWindow application)
        {
            simpleDealingRatesTabClick.Click();

            int row = FindOnSymbol(symbol);

            if (buySell==BuySellEnum.BUY)
            {
                simpleDealingRatesView.Grid.ClickCell(row, BUY_COLUMN, 
                    InputEventModifiers.LeftButton);
            }
            else
            {
                simpleDealingRatesView.Grid.ClickCell(row, SELL_COLUMN, 
                    InputEventModifiers.LeftButton);
            }

            return NoTradeblePriceError(application, symbol);
        }

        private bool NoTradeblePriceError(TradingStationWindow application,
            string symbol)
        {
            Dialog dialogTradeblePrice = null;
            Waiter.WaitingCondition conditionTradeblePrice = () =>
                (dialogTradeblePrice = application.GetDialog(
                    CREATE_MARKET_ORDER_TITLE)) != null;
            if (!Waiter.Wait(conditionTradeblePrice, 2))
            {
                if (dialogTradeblePrice != null)
                {
                    using (dialogTradeblePrice)
                    {
                        if (dialogTradeblePrice[LABEL_ERROR].Text.Contains(
                            ERROR_NO_TRADABLE_PRICE))
                        {
                            TestContext.Out.WriteLine(
                              "\r\nApear ERROR - There is no tradeble price for symbol - "
                                  + symbol);
                            dialogTradeblePrice[OK_BUTTON].Click();
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
            }
            return true;
        }

        public void PressCurrancyPair(string symbol)
        {
            simpleDealingRatesTabClick.Click();
            int row = FindOnSymbol(symbol);
            simpleDealingRatesView.Grid.ClickCell(row, SYMBOL_COLUMN, 
                InputEventModifiers.LeftButton);
        }

        public bool CheckPresencSymbol(List<string> currancyPairs)
        {
            foreach (string currancyPair in currancyPairs)
            {
                if (FindOnSymbol(currancyPair) == -1)
                {
                    return false;
                }
            }
            return true;
        }

        public int FindOnSymbol(string symbol)
        {
            for (int i = 0; i < simpleDealingRatesView.Grid.RowCount; i++)
            {
                if (symbol.Equals(simpleDealingRatesView.Grid.CellValue(i, 
                    SYMBOL_COLUMN)))
                    return i;
            }
            return -1;
        }

        public double GetRate(string symbol, BuySellEnum buySell)
        {
            int row = FindOnSymbol(symbol);

            String sellRateString = simpleDealingRatesView.Grid.CellValue(row, 
                SELL_COLUMN);
            String buyRateString = simpleDealingRatesView.Grid.CellValue(row, 
                BUY_COLUMN);

            double sellRate = Convert.ToDouble(sellRateString);
            double buyRate = Convert.ToDouble(buyRateString);

            return buySell == BuySellEnum.BUY ? buyRate : sellRate;
        }

        protected static WindowMenuItem findSubMenuItemByName(WindowMenu menu, 
            String name)
        {
            WindowMenuItem result = null;
            foreach (WindowMenuItem item in menu)
            {
                if (item.Text.Equals(name))
                {
                    result = item;
                    break;
                }
            }
            return result;
        }

        #region return Value
        public double ReturnPipCostForCurrancyPair(string symbol)
        {
            int row = FindOnSymbol(symbol);
            return double.Parse(simpleDealingRatesView.Grid.CellValue(row, 
                PIP_COST_COLUMN));
        }

        public int ReturnRowCount()
        {
            return simpleDealingRatesView.Grid.RowCount;
        }

        public string ReturnValueBuySellSymbol (string symbol, BuySellEnum buySell)
        {
            int row = FindOnSymbol(symbol);
            if (buySell == BuySellEnum.BUY)
            {
                return simpleDealingRatesView.Grid.CellValue(row, BUY_COLUMN);
            }
            else
            {
                return simpleDealingRatesView.Grid.CellValue(row, SELL_COLUMN);
            }
        }
        #endregion

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    simpleDealingRatesView.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

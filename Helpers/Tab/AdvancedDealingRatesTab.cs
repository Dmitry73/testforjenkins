﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;

namespace TSAutotests
{
    class AdvancedDealingRatesTab : IDisposable
    {
        private const string TAB_TITLE = "Advanced Dealing Rates";

        private AdvancedDealingRatesWindow advancedDealingTabWindow;
        private TradingStationTab advancedTabClick;

        public AdvancedDealingRatesTab(TradingStationWindow application)
        {
            Waiter.WaitingCondition condition = () => (advancedDealingTabWindow
                = application.GetAdvancedDealingRates()) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("zero value rate in tab Advanced Dealing Rates");
            }
            advancedTabClick = application.FindTab(TAB_TITLE);
        }

        //TODO how to work with him
        private void Test()
        {

        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    advancedDealingTabWindow.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

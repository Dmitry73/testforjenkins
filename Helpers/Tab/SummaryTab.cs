﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Windows;

namespace TSAutotests
{
    class SummaryTab : IDisposable
    {
        private const string TAB_TITLE = "Summary";
        private const string CLOSE_TO_BUY_POP_UP_MENU = "Close to Buy...";
        private const string CLOSE_TO_SELL_POP_UP_MENU = "Close to Sell...";
        private const string CLOSE_POSITION_POP_UP_MENU = "Close Position...";
        private const string CLOSE_ALL_POSITION_POP_UP_MENU = "Close All Positions...";
        private const string CLOSE_ALL_FOR_SYMBOL_POP_UP_MENU = "Close All for Symbol...";
        private const int  SYMBOL_COLUMN = 0;

        private ListView openPositionsView;
        private TradingStationTab tabClickSummary;

        public SummaryTab (TradingStationWindow application)
        {
            //ApplicationMenuHelper menuHelper = new ApplicationMenuHelper(application);
            //menuHelper.PressViewOpenPositions();

            openPositionsView = (ListView)application.FindChild("SysListView32", 
                TAB_TITLE, true);

            tabClickSummary = application.FindTab(TAB_TITLE);
            tabClickSummary.Click();

        }

        #region Close Position
        public bool ClosePositionToBuySellMenu(TradingStationWindow application,
            string symbol, string orderType, string timeInForce, BuySellEnum buySell)
        {
            int rowItem = FindOnSymbolWaitingPosition(symbol);
            var menuItem = new ApplicationMenuHelper();

            openPositionsView.ClickCell(rowItem, 0, InputEventModifiers.LeftButton);

            if (buySell == BuySellEnum.BUY)
            {
                menuItem.PressTradingSummaryCloseToBuy(application);
            }
            else
            {
                menuItem.PressTradingSummaryCloseToSell(application);
            }

            return ClosePositionToBuySell(application, symbol, orderType, timeInForce);
        }

        public bool ClosePositionToBuySellPopupMenu(TradingStationWindow application,
            string symbol, string orderType, string timeInForce, BuySellEnum buySell)
        {
            int rowItem = FindOnSymbolWaitingPosition(symbol);
            var popupMenu = new PopupMenuHelper();

            if (buySell == BuySellEnum.BUY)
            {
                popupMenu.SummaryPopupMenuItem(application, CLOSE_TO_BUY_POP_UP_MENU,
                    rowItem);
            }
            else
            {
                popupMenu.SummaryPopupMenuItem(application, CLOSE_TO_SELL_POP_UP_MENU,
                    rowItem);
            }

            return ClosePositionToBuySell(application, symbol, orderType, timeInForce);
        }

        private bool ClosePositionToBuySell(TradingStationWindow application, 
            string symbol, string orderType, string timeInForce)
        {
            bool result = false;
            OpenPositionEntity openPositionEntity;
            using (var openPosition = new OpenPositionsTab(application))
            {
                openPositionEntity = openPosition.ValueOpenPositionBySymbol(symbol);
            }

            using (var closePositions = new ClosePositionsDialog(application))
            {
                closePositions.InstallValue(orderType, timeInForce);
                closePositions.PressOK(application);
            }

            using (var closePosition = new ClosedPositionsTab(application))
            {
                result = DisapearPosition(symbol) &&
                closePosition.FindOnTicketCheckOpenPriceAndSymbol(openPositionEntity.Ticket,
                symbol, openPositionEntity.PriceOpen, openPositionEntity.Amount);
            }

            return result;
        }

        public bool CloseAllPositionsMenu(TradingStationWindow application,
           string symbolOne, string symbolTwo, string orderType, string timeInForce)
        {
            OpenPositionEntity openPositionEntity1, openPositionEntity2;
            using (var openPositionTab = new OpenPositionsTab(application))
            {
                openPositionEntity1 = openPositionTab.ValueOpenPositionBySymbol(symbolOne);
                openPositionEntity2 = openPositionTab.ValueOpenPositionBySymbol(symbolTwo);
            }

            openPositionsView.ClickCell(openPositionEntity1.RowItem, 1,
                InputEventModifiers.LeftButton);
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressTradingSummaryCloseToAllPositions(application);
            return CloseAllPositions(application, symbolOne, symbolTwo, orderType,
                timeInForce, openPositionEntity1, openPositionEntity2);
        }

        public bool CloseAllPositionsPopupMenu(TradingStationWindow application,
            string symbolOne, string symbolTwo, string orderType, string timeInForce)
        {
            OpenPositionEntity openPositionEntity1, openPositionEntity2;
            using (var openPositionTab = new OpenPositionsTab(application))
            {
                openPositionEntity1 = openPositionTab.ValueOpenPositionBySymbol(symbolOne);
                openPositionEntity2 = openPositionTab.ValueOpenPositionBySymbol(symbolTwo);
            }

            var popupMenu = new PopupMenuHelper();
            popupMenu.SummaryPopupMenuItem(application, CLOSE_ALL_POSITION_POP_UP_MENU,
                 openPositionEntity1.RowItem);

            return CloseAllPositions(application, symbolOne, symbolTwo, orderType,
                timeInForce, openPositionEntity1, openPositionEntity2);
        }


        private bool CloseAllPositions(TradingStationWindow application,
            string symbolOne, string symbolTwo, string orderType, string
            timeInForce, OpenPositionEntity openPositionEntity1,
            OpenPositionEntity openPositionEntity2)
        {
            using (var closeAllPosition = new CloseAllPositionsDialog(application))
            {
                closeAllPosition.InstallValue(orderType, timeInForce);
                closeAllPosition.PressOK(application);
            }

            bool result = false;
            using (var closePosition = new ClosedPositionsTab(application))
            {
                result = DisapearPosition(openPositionEntity1.Ticket)
                    && closePosition.FindOnTicketCheckOpenPriceAndSymbol(
                        openPositionEntity1.Ticket, symbolOne, 
                        openPositionEntity1.PriceOpen, openPositionEntity1.Amount)
                    && DisapearPosition(openPositionEntity2.Ticket)
                    && closePosition.FindOnTicketCheckOpenPriceAndSymbol(
                        openPositionEntity2.Ticket, symbolTwo, 
                        openPositionEntity2.PriceOpen, openPositionEntity2.Amount);
            }
            return result;
        }

        public bool CloseAllForSymbolMenu(TradingStationWindow application, string symbol, string orderType,
            string timeInForce)
        {
            OpenPositionEntity openPositionEntity;
            using (var openPositionTab = new OpenPositionsTab(application))
            {
                openPositionEntity = openPositionTab.ValueOpenPositionBySymbol(symbol);
            }
            tabClickSummary.Click();
            openPositionsView.ClickCell(openPositionEntity.RowItem, 1, InputEventModifiers.LeftButton);
            openPositionsView.ClickCell(openPositionEntity.RowItem, 1, InputEventModifiers.LeftButton);
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressOpenPositionCloseAllforSymbol(application);

            return CloseAllForSymbol(application, symbol, orderType, timeInForce,
                openPositionEntity);
        }

        public bool CloseAllForSymbolPopupMenu(TradingStationWindow application, string symbol, string orderType,
            string timeInForce)
        {
            OpenPositionEntity openPositionEntity;
            using (var openPositionTab = new OpenPositionsTab(application))
            {
                openPositionEntity = openPositionTab.ValueOpenPositionBySymbol(symbol);
            }

            var popupMenu = new PopupMenuHelper();
            popupMenu.SummaryPopupMenuItem(application, CLOSE_ALL_FOR_SYMBOL_POP_UP_MENU,
                 openPositionEntity.RowItem);

            return CloseAllForSymbol(application, symbol, orderType, timeInForce,
                openPositionEntity);
        }

        private bool CloseAllForSymbol(TradingStationWindow application, string symbol, 
            string orderType, string timeInForce, OpenPositionEntity openPositionEntity)
        {
            using (var closeAllForSymbol = new CloseAllPositionsForSymbol(application))
            {
                closeAllForSymbol.InstallValue(orderType, timeInForce, symbol);
                closeAllForSymbol.PressOK(application);
            }
            bool result = false;
            using (var closePosition = new ClosedPositionsTab(application))
            {
                result = DisapearPosition(openPositionEntity.Ticket) &&
                    closePosition.FindOnTicketCheckOpenPriceAndSymbol(openPositionEntity.Ticket,
                    symbol, openPositionEntity.PriceOpen, openPositionEntity.Amount);
            }
            return result;
        }
        #endregion

        #region Secondary Functions
        private int FindOnSymbolWaitingPosition(string symbol)
        {
            int rowItem = -1;
            Waiter.WaitingCondition findOpenPostion = () => 
                (rowItem = FindInGridByColumn(symbol, SYMBOL_COLUMN)) != -1;
            if (!Waiter.Wait(findOpenPostion))
            {
                throw new Exception("not find open position");
            }
            return rowItem;
        }

        private bool DisapearPosition(string symbol)
        {
            Waiter.WaitingCondition findOpenPostion = () => 
                FindInGridByColumn(symbol, SYMBOL_COLUMN) == -1;
            if (!Waiter.Wait(findOpenPostion))
            {
                throw new Exception("not find open position");
            }
            return true;
        }

        private int FindInGridByColumn(string symbolByFind, int column)
        {
            for (int i = 0; i < openPositionsView.RowCount; i++)
            {
                if (openPositionsView.CellValue(i, column).Equals(symbolByFind))
                {
                    return i;
                }
            }
            return -1;
        }
        #endregion

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    openPositionsView.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;

namespace TSAutotests
{
    class AccountsTab : IDisposable
    {
        private const string ACCOUNTS_TAB_TITLE = "Account";

        private ListView accountsView;

        public AccountsTab(TradingStationWindow application)
        {
            TradingStationTab accountsTab = null;
            if (application.FindTab(ACCOUNTS_TAB_TITLE) == null)
            {
                //var applicationMenu = new ApplicationMenuHelper();
                //applicationMenu.PressViewAccounts(application);
                Waiter.WaitingCondition condition = () => (application.FindTab(ACCOUNTS_TAB_TITLE)) != null;
                if (!Waiter.Wait(condition))
                    throw new Exception("Tab Orders isn't found");
            }
            accountsTab = application.FindTab(ACCOUNTS_TAB_TITLE);
            accountsTab.Click();

            accountsView = (ListView)application.FindChild("SysListView32", ACCOUNTS_TAB_TITLE, true);
        }

        public void AccountHaveMoney ()
        {
            for (int i = 0; i < accountsView.RowCount; i++)
            {
           
                    }
        }

        public bool CheckTabIsReady()
        {
            string i = accountsView.CellValue(0, 9);
            bool b = !accountsView.CellValue(0, 9).Equals("");
            return !accountsView.CellValue(0, 9).Equals("");  //HACK no ability to track count of columns
        }

        #region IDisposable 
        protected bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    accountsView.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Threading;

namespace TSAutotests
{
    class ClosedPositionsTab : IDisposable
    {
        private const string TAB_TITLE = "History";
        private const int TICKET_COLUMN = 0;
        private const int SYMBOL_COLUMN = 2;
        private const int AMOUNT_COLUMN = 3;
        private const int OPEN_COLUMN = 5;


        private ListView closePositionView;

        public ClosedPositionsTab(TradingStationWindow application)
        {
            //ApplicationMenuHelper menuHelper = new ApplicationMenuHelper(application);
            //menuHelper.PressViewOpenPositions();
            Thread.Sleep(300); //TODO
            closePositionView = (ListView)application.FindChild("SysListView32", TAB_TITLE, true);
        }


        public bool FindOnTicketCheckOpenPriceAndSymbol(string ticket, string symbol, string priceOpen, string amount)
        {
            int rowItem = -1;
            Waiter.WaitingCondition findOpenPostion = () => (rowItem = FindInGridByColumn(ticket, TICKET_COLUMN)) != -1;
            if (!Waiter.Wait(findOpenPostion))
            {
                throw new Exception("not find close position");
            }
            bool result = false;
            result = closePositionView.CellValue(rowItem, SYMBOL_COLUMN).Equals(symbol)
                && closePositionView.CellValue(rowItem, AMOUNT_COLUMN).Equals(amount)
                && closePositionView.CellValue(rowItem, OPEN_COLUMN).Equals(priceOpen);

            return result;
        }


        private int FindInGridByColumn(string symbolByFind, int column)
        {
            for (int i = 0; i < closePositionView.RowCount; i++)
            {
                if (closePositionView.CellValue(i, column).Equals(symbolByFind))
                {
                    return i;
                }
            }
            return -1;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    closePositionView.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

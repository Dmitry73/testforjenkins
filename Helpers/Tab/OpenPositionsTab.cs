﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using NUnit.Framework;
using System.Threading;

namespace TSAutotests
{
    class OpenPositionsTab : IDisposable
    {
        private const string TAB_TITLE = "Trades";
        private const int TICKET_COLUMN = 0;
        private const int ACCOUNT_COLUMN = 1;
        private const int SYMBOL_COLUMN = 2;
        private const int USD_MR_COLUMN = 3;
        private const int AMMOUNT_K_COLUMN = 4;
        private const int SELL_BUY_COLUMN = 5;
        private const int OPEN_COLUMN = 6;
        private const int CLOSE_COLUMN = 7;
        private const int STOP_COLUMN = 8;
        private const int STOP_MOVE_COLUMN = 9;
        private const int LIMIT_COLUMN = 10;
        private const int P_L_COLUMN = 11;

        private const string CLOSE_POSITION_POP_UP_MENU = "Close Position...";
        private const string CLOSE_ALL_POSITION_POP_UP_MENU = "Close All Positions...";
        private const string CLOSE_ALL_FOR_SYMBOL_POP_UP_MENU = "Close All for Symbol...";
        private const string REFRESH_POP_UP_MENU = "Refresh";
        private const string OPEN_POSITION_TAB = "Open Positions";


        private ListView openPositionsView;

        public OpenPositionsTab(TradingStationWindow application)
        {
            //ApplicationMenuHelper menuHelper = new ApplicationMenuHelper(application);
            //menuHelper.PressViewOpenPositions();
            Thread.Sleep(300); //TODO
            ClickOpenPosition(application);
            openPositionsView = (ListView)application.FindChild("SysListView32", TAB_TITLE, true);

        }

        public int OpenPositionsCount
        {
            get
            {
                return openPositionsView.RowCount;
            }
        }

        public void ClickOpenPosition(TradingStationWindow application)
        {
            TradingStationTab openPosition = application.FindTab(OPEN_POSITION_TAB);
            openPosition.Click();
        }


        public void CloseAllOpenPosition(TradingStationWindow application)
        {
            if (openPositionsView.RowCount > 0)
            {
                var applicationMenu = new ApplicationMenuHelper();
                applicationMenu.PressOpenPositionCloseAllPositions(application);
                using (var closeAllPositionsDialog = new CloseAllPositionsDialog(application))
                {
                    closeAllPositionsDialog.PressOK(application);
                }
            }
        }

        public OpenPositionEntity ValueOpenPositionBySymbol(string symbol)
        {
            int rowItem = FindOnSymbolWaitingOpenPosition(symbol);
            string priceOpen = openPositionsView.CellValue(rowItem, OPEN_COLUMN);
            string amount = openPositionsView.CellValue(rowItem, AMMOUNT_K_COLUMN);
            string ticket = openPositionsView.CellValue(rowItem, TICKET_COLUMN);
            string tradeType = openPositionsView.CellValue(rowItem, SELL_BUY_COLUMN);
            var openPositionEntity = new OpenPositionEntity(ticket, tradeType,
                priceOpen, amount, rowItem);
            return openPositionEntity;
        }

        #region Close Position
        public bool ClosePositionMenu(TradingStationWindow application, string symbol, string orderType,
            string timeInForce)
        {
            var openPositionEntity = ValueOpenPositionBySymbol(symbol);

            openPositionsView.ClickCell(openPositionEntity.RowItem, 1, InputEventModifiers.LeftButton);

            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressOpenPositionClosePosition(application);

            return ClosePosition(application, symbol, orderType, timeInForce, openPositionEntity);
        }

        public bool ClosePositionPopupMenu(TradingStationWindow application, string symbol, string orderType,
            string timeInForce)
        {
            var openPositionEntity = ValueOpenPositionBySymbol(symbol);

            var popupMenu = new PopupMenuHelper();
            popupMenu.OpenPositionPopUpMenuItem(application, CLOSE_POSITION_POP_UP_MENU,
                openPositionEntity.RowItem);

            return ClosePosition(application, symbol, orderType, timeInForce, openPositionEntity);
        }

        private bool ClosePosition(TradingStationWindow application, string symbol, string orderType,
            string timeInForce, OpenPositionEntity openPositionEntity)
        {
            using (var closePosition = new ClosePositionDialog(application))
            {
                closePosition.InstallValue(orderType, timeInForce);
                closePosition.PressOK(application);
            }

            bool result = false;
            using (var closePositionTab = new ClosedPositionsTab(application))
            {
                result = DisapearPosition(openPositionEntity.Ticket) &&
                    closePositionTab.FindOnTicketCheckOpenPriceAndSymbol(openPositionEntity.Ticket,
                    symbol, openPositionEntity.PriceOpen, openPositionEntity.Amount);
            }
            return result;
        }


        public bool  CloseAllForSymbolMenu (TradingStationWindow application, 
            string symbol, string orderType, string timeInForce)
        {
            var openPositionEntity = ValueOpenPositionBySymbol(symbol);

            openPositionsView.ClickCell(openPositionEntity.RowItem, 1, InputEventModifiers.LeftButton);
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressOpenPositionCloseAllforSymbol(application);
            return CloseAllForSymbol(application, symbol, orderType, timeInForce,
                openPositionEntity);
        }

        public bool CloseAllForSymbolPopupMenu(TradingStationWindow application,
            string symbol, string orderType, string timeInForce)
        {
            var openPositionEntity = ValueOpenPositionBySymbol(symbol);

            var popupMenu = new PopupMenuHelper();
            popupMenu.OpenPositionPopUpMenuItem(application, CLOSE_ALL_FOR_SYMBOL_POP_UP_MENU,
                openPositionEntity.RowItem);

            return CloseAllForSymbol(application, symbol, orderType, timeInForce,
                openPositionEntity);
        }



        private bool CloseAllForSymbol(TradingStationWindow application, string symbol, string orderType,
            string timeInForce, OpenPositionEntity openPositionEntity)
        {
            

            using (var closeAllForSymbol = new CloseAllPositionsForSymbol(application))
            {
                closeAllForSymbol.InstallValue(orderType, timeInForce, symbol);
                closeAllForSymbol.PressOK(application);
            }
            bool result = false;
            using (var closePosition = new ClosedPositionsTab(application))
            {
                result = DisapearPosition(openPositionEntity.Ticket) &&
                    closePosition.FindOnTicketCheckOpenPriceAndSymbol(openPositionEntity.Ticket,
                    symbol, openPositionEntity.PriceOpen, openPositionEntity.Amount);
            }
            return result;
        }



        public bool CloseAllPositionMenu(TradingStationWindow application, string symbolOne,
            string symbolTwo, string orderType, string timeInForce)
        {
            var openPositionEntity1 = ValueOpenPositionBySymbol(symbolOne);
            var openPositionEntity2 = ValueOpenPositionBySymbol(symbolTwo);

            openPositionsView.ClickCell(openPositionEntity1.RowItem, 1, InputEventModifiers.LeftButton);
            var applicationMenu = new ApplicationMenuHelper();
            applicationMenu.PressOpenPositionCloseAllPositions(application);

            return CloseAllPosition(application, symbolOne, symbolTwo, orderType,
                timeInForce, openPositionEntity1, openPositionEntity2);
        }

        public bool CloseAllPositionPopupMenu(TradingStationWindow application, string symbolOne,
            string symbolTwo, string orderType, string timeInForce)
        {
            var openPositionEntity1 = ValueOpenPositionBySymbol(symbolOne);
            var openPositionEntity2 = ValueOpenPositionBySymbol(symbolTwo);

            var popupMenu = new PopupMenuHelper();
            popupMenu.OpenPositionPopUpMenuItem(application, CLOSE_ALL_POSITION_POP_UP_MENU,
                openPositionEntity1.RowItem);

            return CloseAllPosition(application, symbolOne, symbolTwo, orderType,
                timeInForce, openPositionEntity1, openPositionEntity2);
        }



        private bool CloseAllPosition(TradingStationWindow application, string symbolOne, 
            string symbolTwo, string orderType, string timeInForce,
            OpenPositionEntity openPositionEntity1, OpenPositionEntity openPositionEntity2)
        {
            using (var closeAllPosition = new CloseAllPositionsDialog(application))
            {
                closeAllPosition.InstallValue(orderType, timeInForce);
                closeAllPosition.PressOK(application);
            }

            bool result = false;
            using (var closePosition = new ClosedPositionsTab(application))
            {
                result = DisapearPosition(openPositionEntity1.Ticket)
                    && closePosition.FindOnTicketCheckOpenPriceAndSymbol(openPositionEntity1.Ticket,
                    symbolOne, openPositionEntity1.PriceOpen, openPositionEntity1.Amount)
                    && DisapearPosition(openPositionEntity2.Ticket)
                    && closePosition.FindOnTicketCheckOpenPriceAndSymbol(openPositionEntity2.Ticket,
                    symbolTwo, openPositionEntity2.PriceOpen, openPositionEntity2.Amount);
            }
            return result;
        }
        #endregion

        public bool CheckCreateMarketOrderDefault(string symbol, string amtKValue, BuySellEnum buyOrSell, string accountValue)
        {
            bool result = false;
            int rowItem = FindOnSymbolWaitingOpenPosition(symbol);
            result = CheckBuySell(buyOrSell, rowItem) &&
                CheckAccount(accountValue, rowItem) &&
                CheckAmount(amtKValue, rowItem) &&
                CheckForEmptyColumsStopLimit(rowItem);
            return result;
        }

        public bool CheckCreateMarketOrderStopLimit(string symbol, string amtKValue, 
            BuySellEnum buyOrSell, string accountValue, double stopRange, 
            double limitRange)
        {
            bool result = false;
            int rowItem = FindOnSymbolWaitingOpenPosition(symbol);
            result = CheckColumnsStopLimit(stopRange, limitRange, rowItem) &&
                CheckBuySell(buyOrSell, rowItem) &&
                CheckAccount(accountValue, rowItem) &&
                CheckAmount(amtKValue, rowItem);
            return result;
        }

        public bool CheckCreateMarketOrderLimitPip(string symbol, string amtKValue, BuySellEnum buyOrSell, string accountValue,
            double pipLimit)
        {
            bool result = false;
            int rowItem = FindOnSymbolWaitingOpenPosition(symbol);
            result = CheckLimitExposedPip(pipLimit, buyOrSell, rowItem) &&
                CheckBuySell(buyOrSell, rowItem) &&
                CheckAccount(accountValue, rowItem) &&
                CheckAmount(amtKValue, rowItem);
            return result;
        }

        #region function check
        public bool CheckLimitExposedPip(double pipLimit, BuySellEnum buyOrSell, int rowItem)
        {
            bool result = false;
            double openPosition = double.Parse(openPositionsView.CellValue(rowItem, OPEN_COLUMN));
            double limit = double.Parse(openPositionsView.CellValue(rowItem, LIMIT_COLUMN));
            int signBeforeComma = 1;
            double valueForCheck;

            for (int i = MathHelper.SignsBeforeComma(openPosition); i > 0; i--)
            {
                signBeforeComma = signBeforeComma * 10;
            }
            double oneStepPip = 0.00001 * signBeforeComma;

            if (buyOrSell == BuySellEnum.BUY)
            {
                valueForCheck = openPosition + oneStepPip * pipLimit;
            }
            else
            {
                valueForCheck = openPosition - oneStepPip * pipLimit;
            }
            result = Math.Round(valueForCheck, MathHelper.RoundOnAfterSign(valueForCheck))
                == Math.Round(limit, MathHelper.RoundOnAfterSign(valueForCheck));
            if (result == false)
            {
                TestContext.Out.WriteLine("\r\nlimit = " + limit + " compared value = " + valueForCheck);
                throw new Exception("Error in check limit (in pip)");
            }
            else
            {
                TestContext.Out.WriteLine("\r\nCheck limit exposed in pip completed successfully");
            }
            return result;
        }

        private bool CheckBuySell(BuySellEnum buyOrSell, int rowItem)
        {
            bool equalBuyOrSell = false;
            string checkResult;

            if (buyOrSell == BuySellEnum.BUY)
            {
                equalBuyOrSell = "B" == openPositionsView.CellValue(rowItem, SELL_BUY_COLUMN);
                checkResult = openPositionsView.CellValue(rowItem, SELL_BUY_COLUMN);
            }
            else
            {
                equalBuyOrSell = "S" == openPositionsView.CellValue(rowItem, SELL_BUY_COLUMN);
                checkResult = openPositionsView.CellValue(rowItem, SELL_BUY_COLUMN);
            }
            if (equalBuyOrSell == false)
            {
                TestContext.Out.WriteLine("\r\nCheck Buy or Sell it should be showed - " + checkResult);
                throw new Exception("Error in check direction price");
            }
            else
            {
                TestContext.Out.WriteLine("\r\nCheck buy or sell completed successfully");
            }
            return equalBuyOrSell;
        }
        public bool CheckForEmptyColumsStopLimit(int rowItem)
        {
            if ((openPositionsView.CellValue(rowItem, STOP_COLUMN) == "") && (openPositionsView.CellValue(rowItem, STOP_MOVE_COLUMN) == "") &&
                (openPositionsView.CellValue(rowItem, LIMIT_COLUMN) == ""))
            {
                TestContext.Out.WriteLine("\r\ncell stop and limit not empty");
                return true;
            }
            return false;
        }
        private bool CheckColumnsStopLimit(double stopRange, double limitRange, int rowItem)
        {
            string valueStopFromCell = openPositionsView.CellValue(rowItem, STOP_COLUMN).TrimEnd('0');
            string valueLimitFromCell = openPositionsView.CellValue(rowItem, LIMIT_COLUMN).TrimEnd('0');
            if (valueStopFromCell.Equals(stopRange.ToString()) && (valueLimitFromCell.Equals(limitRange.ToString())))
            {
                TestContext.Out.WriteLine("\r\nCheck cell stop and limit completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\nvalueStopFromCell = " + valueStopFromCell + " compared value = " + stopRange +
                "\r\nvalueLimitFromCell = " + valueLimitFromCell + " compared value = " + limitRange);
            throw new Exception("Error in check stop and limit");
        }

        private bool CheckAccount(string accountValue, int rowItem)
        {
            if (openPositionsView.CellValue(rowItem, ACCOUNT_COLUMN) == accountValue)
            {
                TestContext.Out.WriteLine("\r\nCheck account completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\naccaunt it should be - " + accountValue +
                ", but in position - " + (openPositionsView.CellValue(rowItem, ACCOUNT_COLUMN)));
            throw new Exception("Error in check account");
        }
        private bool CheckAmount(string amtKValue, int rowItem)
        {
            string valueInCell;
            Waiter.WaitingCondition condition = () =>
            {
                valueInCell = openPositionsView.CellValue(rowItem, AMMOUNT_K_COLUMN).Replace(" ", "");
                return valueInCell.Equals(amtKValue);
            };
            if (Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine("\r\nCheck amount completed successfully");
                return true;
            }

            TestContext.Out.WriteLine("\r\nammount should be - " + amtKValue +
                ", but in position - " + (openPositionsView.CellValue(rowItem, AMMOUNT_K_COLUMN)));
            throw new Exception("Error in check Amount");
        }
        #endregion
        #region Secondary Functions
        public int FindOnSymbolWaitingOpenPosition(string symbol)
        {
            int rowItem = -1;
            Waiter.WaitingCondition findOpenPostion = () => (rowItem = FindInGridByColumn(symbol, SYMBOL_COLUMN)) != -1;
            if (!Waiter.Wait(findOpenPostion))
            {
                throw new Exception("not find open position");
            }
            return rowItem;
        }

        private bool DisapearPosition(string ticket)
        {
            Waiter.WaitingCondition findOpenPostion = () => FindInGridByColumn(ticket, SYMBOL_COLUMN) == -1;
            if (!Waiter.Wait(findOpenPostion))
            {
                throw new Exception("not find open position");
            }
            return true;
        }

        private int FindInGridByColumn(string symbolByFind, int column)
        {
            for (int i = 0; i < openPositionsView.RowCount; i++)
            {
                if (openPositionsView.CellValue(i, column).Equals(symbolByFind))
                {
                    return i;
                }
            }
            return -1;
        }

        public bool ExistanceOpenPositionOnCurrancyPair(string symbol)
        {
            for (int i = 0; i < openPositionsView.RowCount; i++)
            {
                if (openPositionsView.CellValue(i, SYMBOL_COLUMN).Equals(symbol))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion
        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    openPositionsView.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

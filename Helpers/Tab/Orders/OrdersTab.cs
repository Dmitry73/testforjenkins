﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using System.Runtime.InteropServices;

namespace TSAutotests
{
    class OrdersTab : IDisposable
    {
        private const string ORDER_TAB = "Orders";
        private const string REMOVE_ORDER_POPUPMENU = "Remove Order";
        private const int ID_COLUMN = 0;
        private const int ACCOUNT_COLUMN = 1;
        private const int TYPE_COLUMN = 2;
        private const int SYMBOL_COLUMN = 4;
        private const int AMMOUNT_COLUMN = 5;
        private const int SELL_COLUMN = 6;
        private const int BUY_COLUMN = 7;
        private const int RANGE_COLUMN = 8;
        private const int TRAILING_COLUMN = 9;
        private const int STOP_COLUMN = 10;
        private const int LIMIT_COLUMN = 11;
        private const int EXPIRE_DATE_COLUMN = 12; //без trailing
        private const string ORDER_TYPE_ENTRY = "Entry";
        private const string ORDER_TYPE_RANGE_ENTRY = "Range Entry";


        public TradingStationComplexView ordersView;

        public OrdersTab(TradingStationWindow application)
        {
            TradingStationTab ordersTab = null;
            //if (application.FindTab(ORDER_TAB) == null)
            //{
            //    var applicationMenu = new ApplicationMenuHelper(application);
            //    applicationMenu.PressViewOrders();
            //    Waiter.WaitingCondition condition = () => (application.FindTab(
            //        ORDER_TAB)) != null;
            //    if (!Waiter.Wait(condition))
            //        throw new Exception("tab Orders isn't found");
            //}
            Thread.Sleep(300); //TODO
            ordersTab = application.FindTab(ORDER_TAB);
            ordersTab.Click();

            ordersView = application.GetOrdersView();
        }

        private ListView FindChildElement(Window window, string nameClass)
        {
            WindowCollection children = window.GetChildren(false);
            foreach (Window child in children)
            {
                    TestContext.Out.WriteLine("\r\nchild.ClassName - " +
                        child.ClassName);
            }
            return null;
        }

        public void DeleteAllOrders(TradingStationWindow application)
        {
            var applicationMenu = new ApplicationMenuHelper();

            FindChildElement(ordersView.Grid, "SysListView32");
                

                //TestContext.Out.WriteLine("\r\nlistView.ClassName - " +
                //    listView.ClassName);

                //if (ordersView.Grid.GetGroup(i).Name.Contains("OTO "))
                //{
                //    while (!ordersView.Grid.GetGroup(i).Name.Equals("OTO"))
                //    {
                //        TestContext.Out.WriteLine("\r\nordersView.Grid.GetGroup(i).Name - " +
                //            ordersView.Grid.GetGroup(i).Name);

                //        string nameBeforeRemove = ordersView.Grid.GetGroup(i).Name;
                //        var firstNonEmptyOrdersGroup = ordersView.Grid.GetGroup(i);
                //        firstNonEmptyOrdersGroup.Grid.SendLeftMouseButtonDown(0, 0,
                //            InputEventModifiers.None);
                //        firstNonEmptyOrdersGroup.Grid.SendLeftMouseButtonUp(0, 0,
                //            InputEventModifiers.None);
                //        applicationMenu.PressRemoveOrder(application);
                //        YesNoDialog.PressYes("FXCM Trading Station Desktop", application);
                //        Thread.Sleep(1500);  //HACK
                //        Waiter.WaitingCondition condition = () =>
                //            !ordersView.Grid.GetGroup(i).Name.Equals(nameBeforeRemove);
                //        if (!Waiter.Wait(condition, 30))
                //        {
                //            throw new Exception("Can't close order");
                //        }
                //    }
                //}
        }

        public int GetAllOrdersCount()
        {
            int count = 0;
            for (int i = 0; i < ordersView.Grid.Count; i++)
            {
                count += ordersView.Grid.GetGroup(i).Grid.RowCount;

            }
            return count;
        }



        #region Check Value Open Orders
        public bool CheckEntryOrderDefault(string symbol, string accountValue,
            BuySellEnum buyOrSell, int amount, double price, double
            rateCoefficient, string orderType)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType);
            return result;
        }

        public bool CheckStopLimitInPip(OrderGroups orderType, string symbol,
            double stopRate, double limitRate)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);
            double stopComapare = double.Parse(orderGroup.Grid.CellValue(
                rowItem, STOP_COLUMN));
            double limitComapare = double.Parse(orderGroup.Grid.CellValue(
                rowItem, LIMIT_COLUMN));

            result = (stopComapare == stopRate) && (limitComapare == limitRate);

            return result;
        }

        public bool CheckEntryOrderWithStopLimit(string symbol, string
            accountValue, BuySellEnum buyOrSell, int amount, double price,
            double rateCoefficient, double stopRateInstall, double
            limitRateInstall, string orderType)
        {
            bool result = false;

            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType) &&
                     CheckColumnsStopLimit(orderGroup, stopRateInstall,
                     limitRateInstall, rowItem);
            return result;
        }

        public bool CheckEntryOrderWithStopLimitInPip(string symbol, string
            accountValue, BuySellEnum buyOrSell, int amount, double price,
            double rateCoefficient, string stopPip, string limitPip,
            string orderType)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType) &&
                     CheckColumnStopLimitInPip(orderGroup, stopPip, limitPip,
                     rowItem);
            return result;
        }

        public bool CheckEntryOrderWithRangeOrderType(string symbol, string
            accountValue, BuySellEnum buyOrSell, int amount, double price,
            double rateCoefficient, string range, string orderType)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType) &&
                CheckRange(orderGroup, rowItem, range);
            return result;
        }

        public bool CheckEntryOrderWithRangeOrderTypeGTD(string symbol, string
            accountValue, BuySellEnum buyOrSell, int amount, double price,
            double rateCoefficient, string range, string orderType, string dateTime)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType) &&
                CheckRange(orderGroup, rowItem, range) &&
                CheckExpireDate(orderGroup, dateTime, rowItem);
            return result;
        }

        public bool CheckEntryOrderGTD(string symbol, string accountValue,
            BuySellEnum buyOrSell, int amount, double price, double
            rateCoefficient, string dateTime, string orderType)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType) &&
                CheckExpireDate(orderGroup, dateTime, rowItem);
            return result;
        }

        public bool CheckEntryOrderRangeEntryStopLimit(string symbol, string
            accountValue, BuySellEnum buyOrSell, int amount, double price,
            double rateCoefficient, string range, string orderType, double
            stopRateInstall, double limitRateInstall)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroups.Entry);
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);

            result = GeneralCheck(orderGroup, rowItem, symbol, accountValue,
                buyOrSell, amount, price, rateCoefficient, orderType) &&
                CheckRange(orderGroup, rowItem, range) &&
                CheckColumnsStopLimit(orderGroup, stopRateInstall,
                limitRateInstall, rowItem);
            return result;
        }

        public bool CheckDataOrder(string dataTime, OrderGroups group,
            string primarySymbol, string secondarySymbol)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(group);
            int rowItem1 = SearchOrderOnSymbol(orderGroup, primarySymbol);
            int rowItem2 = SearchOrderOnSymbol(orderGroup, secondarySymbol);

            result = CheckExpireDate(orderGroup, dataTime, rowItem1) &&
                CheckExpireDate(orderGroup, dataTime, rowItem2);

            return result;
        }


        #endregion

        #region Check Contingent Order
        public bool CheckRate(string symbol, OrderGroups orderType,
            BuySellEnum buySell, double rate, bool takeAccountIfThen = false)
        {
            bool result = false;
            if (orderType == OrderGroups.If_Then || orderType ==
                OrderGroups.If_Then_OCO)
            {
                buySell = BuySellEnum.SELL;
            }
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbolWaitEmergenceBuySell(orderGroup,
                symbol, buySell, takeAccountIfThen);
            result = ExhibitedPrice(orderGroup, rowItem, buySell, rate);
            return result;
        }

        public bool CCOCheckOrderType(string symbol, OrderGroups orderType,
            BuySellEnum buySell, double rateCoefficient, string
            orderTypeCurrancyPair)
        {
            bool result = false;
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);
            result = BuyOrSellTheOrderType(orderGroup, rowItem, buySell,
                rateCoefficient, orderTypeCurrancyPair);
            return result;
        }

        private OrderGroups OrderGroupTab(OrderGroups orderType)
        {
            if (orderType == OrderGroups.If_Then)
            {
                orderType = OrderGroups.OTO;
            }
            if (orderType == OrderGroups.If_Then_OCO)
            {
                orderType = OrderGroups.OTOCO;
            }
            return orderType;
        }

        public void PressRightButtonOnCell(OrderGroups orderType, string symbol)
        {
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);
            orderGroup.Grid.ClickCell(rowItem, 1, InputEventModifiers.RightButton);
        }

        public void PressRightButtonOnCellBySymbolAndBuySell
            (OrderGroups orderType, string symbol, BuySellEnum buySell, bool
            takeAccountIfThen)
        {
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbolWaitEmergenceBuySell(orderGroup,
                symbol, buySell, takeAccountIfThen);
            orderGroup.Grid.ClickCell(rowItem, 1, InputEventModifiers.RightButton);
        }

        public bool CheckAmmount(string symbol, int amount, OrderGroups orderType)
        {
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);
            return Amount(orderGroup, rowItem, amount);
        }

        public bool CheckTrailing(string symbol, OrderGroups orderType,
            BuySellEnum buySell, bool takeAccountIfThen)
        {
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbolWaitEmergenceBuySell(orderGroup,
                symbol, buySell, takeAccountIfThen);
            Waiter.WaitingCondition condition = () =>
            orderGroup.Grid.CellValue(rowItem, TRAILING_COLUMN).Equals("") == false;
            if (Waiter.Wait(condition, 5))
            {
                return true;
            }
            TestContext.Out.WriteLine("\r\n error in trailing, value trailing  = " +
                orderGroup.Grid.CellValue(rowItem, TRAILING_COLUMN));
            return false;
        }

        public string ReturnRate(string symbol, BuySellEnum buySell,
            OrderGroups orderType, bool takeAccountIfThen = false)
        {
            TradingStationGridGroup.Group orderGroup =
                WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbolWaitEmergenceBuySell(orderGroup,
                symbol, buySell, takeAccountIfThen);
            if (BuySellEnum.BUY == buySell)
            {
                return orderGroup.Grid.CellValue(rowItem, BUY_COLUMN);
            }
            else
            {
                return orderGroup.Grid.CellValue(rowItem, SELL_COLUMN);
            }
        }


        #endregion

        #region Secondary function
        private int SearchOrderOnSymbolWaitEmergence(TradingStationGridGroup.Group
            orderGroup, string symbol)
        {
            int row = -1;
            Waiter.WaitingCondition condition = () =>
            (row = SearchOrderOnSymbol(orderGroup, symbol)) != -1;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found order by ");
            }
            return row;
        }

        private int SearchOrderOnSymbol(TradingStationGridGroup.Group
            orderGroup, string symbol)
        {
            for (int rowItem = 0; rowItem < orderGroup.Grid.RowCount; rowItem++)
            {
                string orderInstrument = orderGroup.Grid.CellValue(rowItem, SYMBOL_COLUMN);
                if (!orderInstrument.Equals(symbol))
                    continue;
                return rowItem;
            }
            return -1;
        }

        private int SearchOrderOnSymbolWaitEmergenceBuySell(
            TradingStationGridGroup.Group orderGroup, string symbol,
            BuySellEnum buySell, bool takeAccountIfThen)
        {
            int row = -1;
            Waiter.WaitingCondition condition = () =>
            (row = SearchOrderOnSymbolAndBuySell(orderGroup, symbol, buySell,
                takeAccountIfThen)) != -1;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found order by ");
            }
            return row;
        }

        private int SearchOrderOnSymbolAndBuySell(TradingStationGridGroup.Group
            orderGroup, string symbol, BuySellEnum buySell, bool takeAccountIfThen)
        {
            for (int rowItem = 0; rowItem < orderGroup.Grid.RowCount; rowItem++)
            {
                string orderInstrument = orderGroup.Grid.CellValue(rowItem, SYMBOL_COLUMN);
                if (!orderInstrument.Equals(symbol))
                    continue;
                if ((BuySellEnum.BUY == buySell) &&
                    orderGroup.Grid.CellValue(rowItem, BUY_COLUMN).Equals(""))
                {
                    continue;
                }
                if ((BuySellEnum.SELL == buySell) &&
                    orderGroup.Grid.CellValue(rowItem, SELL_COLUMN).Equals(""))
                {
                    continue;
                }

                if (!takeAccountIfThen)
                {
                    if (orderGroup.Grid.CellValue(rowItem,
                        AMMOUNT_COLUMN).Equals("If-Then"))
                    {
                        continue;
                    }
                }

                return rowItem;
            }
            return -1;
        }
        private TradingStationGridGroup.Group WaitingEmergenceOrder(
            OrderGroups group)
        {
            TradingStationGridGroup.Group orderGroup = null;
            Waiter.WaitingCondition condition = () => (orderGroup =
                FindOrderGroup(group)) != null;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("not found order");
            }
            return orderGroup;
        }

        public TradingStationGridGroup.Group FindFirstNonEmptyOrdersGroup()
        {
            for (int i = 0; i < ordersView.Grid.Count; i++)
            {
                var currentGroup = ordersView.Grid.GetGroup(i);
                if (currentGroup.Grid.RowCount > 0)
                {
                    return currentGroup;
                }
            }
            return null;
        }

        public List<String> GetOrdersFromGroup(OrderGroups orderGroupName)
        {
            List<string> ret = new List<string>();
            TradingStationGridGroup.Group orderGroup = FindOrderGroup(orderGroupName);
            if (orderGroup == null)
                return ret;

            for (int j = 0; j < orderGroup.Grid.RowCount; j++)
            {
                ret.Add(orderGroup.Grid.CellValue(j, ID_COLUMN));
            }
            return ret;
        }

        private TradingStationGridGroup.Group FindOrderGroup(OrderGroups
            orderGroupName)
        {
            for (int i = 0; i < ordersView.Grid.Count; i++)
            {
                TradingStationGridGroup.Group orderGroup = ordersView.Grid.GetGroup(i);
                if (orderGroup.Name.IndexOf(
                    OrderGroupDictionary.orderGroupNames[orderGroupName]) == -1)
                    continue;
                return orderGroup;
            }
            return null;
        }

        public string GetOrderId(List<string> excludeOrders, OrderGroups
            orderGroupName, string instrument, BuySellEnum buySell, double
            orderRate)
        {
            TradingStationGridGroup.Group orderGroup =
                FindOrderGroup(orderGroupName);
            if (orderGroup == null)
                return null;
            for (int i = 0; i < orderGroup.Grid.RowCount; i++)
            {
                string orderId = orderGroup.Grid.CellValue(i, ID_COLUMN);
                if (excludeOrders.Contains(orderId))
                    continue;
                string orderInstrument = orderGroup.Grid.CellValue(i, SYMBOL_COLUMN);
                if (!orderInstrument.Equals(instrument))
                    continue;

                return orderId;
            }
            return null;
        }

        public bool CheckOrderExists(string orderId, OrderGroups orderGroupName)
        {
            TradingStationGridGroup.Group orderGroup = FindOrderGroup(orderGroupName);
            if (orderGroup == null)
                return false;
            for (int i = 0; i < orderGroup.Grid.RowCount; i++)
            {
                if (orderGroup.Grid.CellValue(i, ID_COLUMN) == orderId)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region return value
        public string ReturnIdOrder(OrderGroups orderType, string symbol)
        {
            TradingStationGridGroup.Group orderGroup =
               WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbol(orderGroup, symbol);
            return orderGroup.Grid.CellValue(rowItem, ID_COLUMN);
        }
        #endregion

        #region Mini Check
        public bool CheckExistanceOrderByID(string compareID, string symbol,
            OrderGroups orderType)
        {
            TradingStationGridGroup.Group orderGroup =
               WaitingEmergenceOrder(OrderGroupTab(orderType));
            int rowItem = SearchOrderOnSymbolWaitEmergence(orderGroup, symbol);
            if (orderGroup.Grid.CellValue(rowItem, ID_COLUMN).Equals(compareID))
            {
                TestContext.Out.WriteLine("\r\nCheck order ID completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\nValue in Cell - " +
                orderGroup.Grid.CellValue(rowItem, ID_COLUMN) + "Compare value - " +
                compareID);
            return false;
        }

        private bool CheckRange(TradingStationGridGroup.Group orderGroup, int
            rowItem, string range)
        {
            if (range.Equals(orderGroup.Grid.CellValue(rowItem, RANGE_COLUMN)))
            {
                TestContext.Out.WriteLine("\r\nCheck range completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\nrange = " + range + " compared value = "
                + orderGroup.Grid.CellValue(rowItem, RANGE_COLUMN));
            throw new Exception("Error in check Range");
        }

        private bool GeneralCheck(TradingStationGridGroup.Group orderGroup,
            int rowItem, string symbol, string accountValue,
            BuySellEnum buyOrSell, int ammount, double price, double
            rateCoefficient, string orderType)
        {
            return CheckAccount(orderGroup, rowItem, accountValue) &&
                     BuyOrSellTheOrderType(orderGroup, rowItem, buyOrSell,
                     rateCoefficient, orderType) &&
                     Amount(orderGroup, rowItem, ammount) &&
                     ExhibitedPrice(orderGroup, rowItem, buyOrSell, price);
        }

        private bool CheckColumnStopLimitInPip(TradingStationGridGroup.Group
            orderGroup, string pipStop, string pipLimit, int rowItem)
        {
            Waiter.WaitingCondition condition = () => (
            orderGroup.Grid.CellValue(rowItem, STOP_COLUMN)) != "";
            if (!Waiter.Wait(condition, 5))
            {
                throw new Exception("Did not find a stop at the order");
            }
            string pipStopInTabOrder =
                orderGroup.Grid.CellValue(rowItem, STOP_COLUMN);
            string pipLimitInTabOrder =
                orderGroup.Grid.CellValue(rowItem, LIMIT_COLUMN);
            if (pipStopInTabOrder.Equals(pipStop) &&
                pipLimitInTabOrder.Equals(pipLimit))
            {
                TestContext.Out.WriteLine(
                    "\r\nCheck stop and limit in pip successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\npipStopInTabOrder = " +
                pipStopInTabOrder + " compared value = " + pipStop +
                "\r\npipLimitInTabOrder = " + pipLimitInTabOrder +
                " compared value = " + pipLimit);

            throw new Exception("Error in check limit and stop");
        }

        private bool CheckExpireDate(TradingStationGridGroup.Group orderGroup,
            string dataTime, int rowItem)
        {
            //TODO
            string Test = orderGroup.Grid.CellValue(rowItem, EXPIRE_DATE_COLUMN);

            if (dataTime == orderGroup.Grid.CellValue(rowItem, EXPIRE_DATE_COLUMN))
            {
                TestContext.Out.WriteLine("\r\nCheck expire date completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\ndateTime = " + dataTime + " compared value = " +
                orderGroup.Grid.CellValue(rowItem, EXPIRE_DATE_COLUMN));
            throw new Exception("Error in check Expire Date");
        }

        private bool CheckColumnsStopLimit(TradingStationGridGroup.Group
            orderGroup, double stopRange, double limitRange, int rowItem)
        {
            Waiter.WaitingCondition condition = () => (
                orderGroup.Grid.CellValue(rowItem, STOP_COLUMN)) != "";
            if (!Waiter.Wait(condition, 5))
            {
                throw new Exception("Did not find a stop at the order");
            }

            string valueStopFromCell = orderGroup.Grid.CellValue(rowItem,
                STOP_COLUMN).TrimEnd('0');
            string valueLimitFromCell = orderGroup.Grid.CellValue(rowItem,
                LIMIT_COLUMN).TrimEnd('0');
            if (valueStopFromCell.Equals(stopRange.ToString()) &&
                valueLimitFromCell.Equals(limitRange.ToString()))
            {
                TestContext.Out.WriteLine(
                    "\r\nCheck stop and limit completed successfully");
                return true;
            }

            TestContext.Out.WriteLine("\r\nvalueStopFromCell = "
                + valueStopFromCell + " compared value = " +
                Math.Round(stopRange, MathHelper.RoundOnAfterSign(stopRange)) +
                 "\r\nvalueLimitFromCell = " + valueLimitFromCell +
                 " compared value = " + Math.Round(limitRange,
                 MathHelper.RoundOnAfterSign(limitRange)));

            throw new Exception("Error in check limit and stop");
        }


        private bool ExhibitedPrice(TradingStationGridGroup.Group orderGroup,
            int rowItem, BuySellEnum buyOrSell, double price)
        {
            bool result = false;
            string comparePrice;
            if (buyOrSell == BuySellEnum.BUY)
            {
                Waiter.WaitingCondition condition = () => (result =
                    orderGroup.Grid.CellValue(rowItem, BUY_COLUMN).TrimEnd('0').
                    Equals(price.ToString()));
                Waiter.Wait(condition, 5);
                comparePrice = orderGroup.Grid.CellValue(rowItem,
                    BUY_COLUMN).TrimEnd('0');
            }
            else
            {
                Waiter.WaitingCondition condition = () => (result =
                    orderGroup.Grid.CellValue(rowItem, SELL_COLUMN).TrimEnd('0').
                    Equals(price.ToString()));
                Waiter.Wait(condition, 5);
                comparePrice = orderGroup.Grid.CellValue(rowItem,
                    SELL_COLUMN).TrimEnd('0');
            }

            if (result == false)
            {
                TestContext.Out.WriteLine("\r\nprice = " + price +
                    " compared value = " + comparePrice);
                throw new Exception("Error in check exhibited price");
            }
            else
            {
                TestContext.Out.WriteLine(
                    "\r\nCheck exhibited price completed successfully");
            }

            return result;
        }

        private bool CheckAccount(TradingStationGridGroup.Group orderGroup,
            int rowItem, string accountValue)
        {
            if (orderGroup.Grid.CellValue(rowItem, ACCOUNT_COLUMN) == accountValue)
            {
                TestContext.Out.WriteLine(
                    "\r\nCheck account create order completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\naccaunt it should be - " +
                accountValue + ", but in order TAB - " +
                orderGroup.Grid.CellValue(rowItem, ACCOUNT_COLUMN));
            throw new Exception("Error in check account");
        }

        private bool BuyOrSellTheOrderType(TradingStationGridGroup.Group
            orderGroup, int rowItem, BuySellEnum buyOrSell, double
            rateCoefficient, string orderTypeCurrancyPair)
        {
            bool equalBuyOrSell = false;
            if (orderTypeCurrancyPair == ORDER_TYPE_ENTRY)
            {
                if (rateCoefficient > 0)
                {
                    if (buyOrSell == BuySellEnum.BUY)
                    {
                        equalBuyOrSell = "SE" ==
                            orderGroup.Grid.CellValue(rowItem, TYPE_COLUMN);
                    }
                    else
                    {
                        equalBuyOrSell = "LE" ==
                            orderGroup.Grid.CellValue(rowItem, TYPE_COLUMN);
                    }
                }
                else
                {
                    if (buyOrSell == BuySellEnum.BUY)
                    {
                        equalBuyOrSell = "LE" ==
                            orderGroup.Grid.CellValue(rowItem, TYPE_COLUMN);
                    }
                    else
                    {
                        equalBuyOrSell = "SE" ==
                            orderGroup.Grid.CellValue(rowItem, TYPE_COLUMN);
                    }
                }
            }
            else
            {
                if (orderTypeCurrancyPair == ORDER_TYPE_RANGE_ENTRY)
                {
                    equalBuyOrSell = "RE" ==
                        orderGroup.Grid.CellValue(rowItem, TYPE_COLUMN);
                }
            }

            if (equalBuyOrSell == false)
            {
                TestContext.Out.WriteLine("\r\nValue in check cell - " +
                    orderGroup.Grid.CellValue(rowItem, TYPE_COLUMN));
                throw new Exception("Error in check directions price");
            }
            else
            {
                TestContext.Out.WriteLine("\r\nCheck order type completed successfully");
            }
            return equalBuyOrSell;
        }

        private bool Amount(TradingStationGridGroup.Group orderGroup,
            int rowItem, int amount)
        {
            string valueInCell;
            Waiter.WaitingCondition condition = () =>
            {
                valueInCell = orderGroup.Grid.CellValue(rowItem,
                    AMMOUNT_COLUMN).Replace(" ", "");
                return valueInCell.Equals(amount.ToString());
            };
            if (Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine("\r\nCheck amount completed successfully");
                return true;
            }
            TestContext.Out.WriteLine("\r\namount given - " + amount + " compare value - " +
                orderGroup.Grid.CellValue(rowItem, AMMOUNT_COLUMN) == amount.ToString());
            throw new Exception("Error in check amount. Value in cell = " +
                orderGroup.Grid.CellValue(rowItem, AMMOUNT_COLUMN));
        }
        #endregion

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    ordersView.Dispose();
                }
                disposedValue = true;
            }
#endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Threading;
using NUnit.Framework;

namespace TSAutotests
{
    class StoredStrategiesTab : IDisposable
    {
        private const string TAB_TITLE_CLICK = "Stored Strategies";
        private TradingStationTab storedStrategyTabClick;
        public ListView storedStrategiesView;
        private const string TAB_TITLE = "CloudStrategies";
        private const string NEW_STRATEGY_POPUP = "New Strategy...";
        private const string BACKTEST_STRATEGY_POPUP = "Backtest Strategy...";
        private const string REMOVE_STRATEGY_POPUP = "Remove Strategy";
        private const string READY_STRATEGY = "Ready";
        private const int COLUMN_STATUS = 0;
        private const int COLUMN_NAME = 1;
        private const int COLUMN_STRATEGY = 2;
        private const int COLUMN_UPLOAD_DATE = 3;
        private const int COLUMN_VERIFICATION_DATE = 4;

        public StoredStrategiesTab(TradingStationWindow application)
        {
            storedStrategiesView = (ListView)application.FindChild(
                "SysListView32", TAB_TITLE, true);
            ClickStoredStrategyTab(application);
        }

        private void ClickStoredStrategyTab(TradingStationWindow application)
        {
            storedStrategyTabClick = application.FindTab(TAB_TITLE_CLICK);
            storedStrategyTabClick.Click();
        }

        public void BacktestStrategyNoParam(TradingStationWindow application,
            string nameStrategy)
        {
            OpenPopup(application, nameStrategy);
            using (var backtest = new BacktestStrategyNameDialog(application,
                nameStrategy))
            {
                backtest.NoParamatrsTest();
            }
            using (var dashboard = new BacktestingDashboardTab(application))
            {
                dashboard.SearchStrategyWaitEmergance(nameStrategy);
            }
        }

        public bool DownloadStrategy(TradingStationWindow application,
            string nameStrategy)
        {
            bool result = false;
            string nameStrategySelect;
            var popUpMenu = new PopupMenuHelper();

            popUpMenu.StoredStrategyPopupMenuItem(application,
                NEW_STRATEGY_POPUP, 0, 1);
            using (var newStrategyDialog = new NewStrategyDialog(application))
            {
                nameStrategySelect =
                    newStrategyDialog.TestRunStrategies(nameStrategy);
            }
            Waiter.WaitingCondition condition = () =>
              storedStrategiesView.CellValue(0, COLUMN_STATUS).Equals(READY_STRATEGY);
            if (!Waiter.Wait(condition, 120))
            {
                throw new Exception("Strategy don't download");
            }
            else
            {
                result = true;
            }

            result = result && nameStrategySelect.Contains(
                storedStrategiesView.CellValue(0, COLUMN_STRATEGY));

            return result;
        }

        public bool CheckExistStrategy(string nameStrategy)
        {
            Waiter.WaitingCondition condition = () =>
                FindStrategyName(nameStrategy) != -1;
            if (!Waiter.Wait(condition, 2))
            {
                TestContext.Out.WriteLine("\r\nStrategy delete");
                return false;
            }
            else
            {
                TestContext.Out.WriteLine("\r\nStrategy not delete");
                return true;
            }
        }

        public void RemoveAllStrategies(TradingStationWindow application)
        {
            var popUpMenu = new PopupMenuHelper();
            while (storedStrategiesView.RowCount > 0)
            {
                int countBegin = storedStrategiesView.RowCount;
                popUpMenu.StoredStrategyPopupMenuItem(application,
                    REMOVE_STRATEGY_POPUP, 0, 1);
                Waiter.WaitingCondition condition = () =>
                    storedStrategiesView.RowCount != countBegin;
                if (!Waiter.Wait(condition, 20))
                {
                    throw new Exception("Can't close strategy");
                }
            }
        }

        public bool RemoveNameStrategies(TradingStationWindow
            application, string nameStrategy)
        {
            var popUpMenu = new PopupMenuHelper();

            int rowStrategies = SearchStrategyWaitEmergance(nameStrategy);
            popUpMenu.StoredStrategyPopupMenuItem(application, 
                REMOVE_STRATEGY_POPUP, rowStrategies);
            Waiter.WaitingCondition condition = () =>
                (FindStrategyName(nameStrategy)) == -1;
            if (!Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine("\r\nStrategy not delete");
                return false;
            }
            else
            {
                TestContext.Out.WriteLine("\r\nStrategy delete");
                return true;
            }
        }

        public bool BacktestStrategyStartAndCheck(TradingStationWindow
            application, string nameStrategy)
        {
            OpenPopup(application, nameStrategy);

            bool result = false;
            using (var strategy = new BacktestStrategyNameDialog(
                application, nameStrategy))
            {
                result = strategy.RunAndCheckDefault(application, nameStrategy);
            }
            return result;
        }

        public bool BacktestStrategyStartParametrAndCheck(TradingStationWindow
           application, string nameStrategy, DateTime fromDate, DateTime toDate,
            int instrumentIndex)
        {
            OpenPopup(application, nameStrategy);

            bool result = false;
            using (var strategy = new BacktestStrategyNameDialog(
                application, nameStrategy))
            {
                result = strategy.RunAndCheckParametr(application, nameStrategy,
                    fromDate, toDate, instrumentIndex);
            }
            return result;
        }

        private void OpenPopup(TradingStationWindow application, string
            nameStrategy)
        {
            var popUpMenu = new PopupMenuHelper();
            int rowStrategy = SearchStrategyWaitEmergance(nameStrategy);
            popUpMenu.StoredStrategyPopupMenuItem(application,
                BACKTEST_STRATEGY_POPUP, rowStrategy, 1);
        }

        private int SearchStrategyWaitEmergance(string nameStrategy)
        {
            int row = -1;
            Waiter.WaitingCondition condition = () =>
                (row = FindStrategyName(nameStrategy)) != -1;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found strategy ");
            }
            return row;
        }

        private int FindStrategyName(string nameStrategy)
        {
            for (int i = 0; i < storedStrategiesView.RowCount; i++)
            {
                if (storedStrategiesView.CellValue(i, COLUMN_STRATEGY).Contains(
                    nameStrategy))
                {
                    return i;
                }
            }
            return -1;
        }

        public bool CheckNotExistStrategies()
        {
            Waiter.WaitingCondition condition = () =>
                storedStrategiesView.RowCount == 0;
            if (!Waiter.Wait(condition))
            {
                TestContext.Out.WriteLine("\r\nStrategies exist");
                return false;
            }
            else
            {
                TestContext.Out.WriteLine("\r\nCorrect strategies not exist");
                return true;
            }
        }
        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    storedStrategiesView.Dispose();
                }
                disposedValue = true;
            }
#endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}

﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Threading;
using NUnit.Framework;

namespace TSAutotests
{
    class BacktestingDashboardTab : IDisposable
    {
        private const string TAB_TITLE = "CloudBacktests";
        private const int COLUMN_STATUS = 0;
        private const int COLUMN_NAME = 1;
        private const int COLUMN_STRATEGY = 2;
        private const int COLUMN_INSTRUMENT = 3;
        private const int COLUMN_SINCE = 4;
        private const int COLUMN_TILL = 5;
        private const int COLUMN_STARTED = 6;
        private const int COLUMN_FINISHED = 7;
        private const int COLUMN_P_L = 8;
        private const int COLUMN_TRADES = 9;
        private const int COLUMN_VISIBALITY = 10;
        private const string REMOVE_STRATEGY_POPUP = "Remove";
        private const string SHOW_STATISTIC_POPUP = "Show Statistics...";
        private const string END_CALCULATION_ON_STRAEGY = "Done";

        private const string TAB_TITLE_CLICK = "Backtesting Dashboard";
        private TradingStationTab backtestingDashboardTabClick;
        public ListView backtestingDashboardView;

        public BacktestingDashboardTab(TradingStationWindow application)
        {
            backtestingDashboardView = (ListView)application.FindChild(
                "SysListView32", TAB_TITLE, true);
            BacktestingDashboardTabClick(application);
        }

        private void BacktestingDashboardTabClick(TradingStationWindow
            application)
        {
            backtestingDashboardTabClick = application.FindTab(TAB_TITLE_CLICK);
            backtestingDashboardTabClick.Click();
        }

        public bool NoStrategiesInTab()
        {
            Waiter.WaitingCondition condition = () =>
               backtestingDashboardView.RowCount == 0;
            if (!Waiter.Wait(condition, 10))
            {
                TestContext.Out.WriteLine("\r\nStrategies are present");
                return false;
            }
            else
            {
                return true;
            }
        }


        public bool CheckStrategy(string nameStrategy, string instrument, 
            string fromSince, string toTill)
        {
            int rowStrategy = SearchStrategyWaitEmergance(nameStrategy);
            bool result = backtestingDashboardView.CellValue(rowStrategy,
                COLUMN_INSTRUMENT).Equals(instrument);
            result = result && backtestingDashboardView.CellValue(rowStrategy,
                COLUMN_SINCE).Equals(fromSince);

            result = result && backtestingDashboardView.CellValue(rowStrategy,
                COLUMN_TILL).Equals(toTill);
            return result;
        }

        public void WaitDownloadStrategies(int rowStrategies)
        {
            Waiter.WaitingCondition condition = () =>
                backtestingDashboardView.CellValue(rowStrategies,
                COLUMN_STATUS).Equals(END_CALCULATION_ON_STRAEGY);
            if (!Waiter.Wait(condition, 60))
            {
                throw new Exception("The strategy data is not calculated");
            }
        } 

        public void ShowStatisticInStrategyBackName(TradingStationWindow 
            application, string nameStrategy)
        {
            int rowStrategy = SearchStrategyWaitEmergance(nameStrategy);
            var popUpMenu = new PopupMenuHelper();
            popUpMenu.BacktestingDashboardPopupMenuItem(application,
                SHOW_STATISTIC_POPUP, rowStrategy, 1);
            using (var strategyBacktesterName = new
                StrategyBacktesterNameDialog(nameStrategy))
            {
                strategyBacktesterName.StatisticTab();
            }
        }

        public void RemoveAllStrategies(TradingStationWindow application)
        {
            var popUpMenu = new PopupMenuHelper();
            while (backtestingDashboardView.RowCount > 0)
            {
                int countBegin = backtestingDashboardView.RowCount;
                popUpMenu.BacktestingDashboardPopupMenuItem(application,
                    REMOVE_STRATEGY_POPUP, 0, 1);
                Waiter.WaitingCondition condition = () =>
                    backtestingDashboardView.RowCount != countBegin;
                if (!Waiter.Wait(condition, 20))
                {
                    throw new Exception("Can't close strategy");
                }
            }
        }

        public bool RemoveStrategyName(TradingStationWindow application, 
            string nameStrategy)
        {
            int row = SearchStrategyWaitEmergance(nameStrategy);
            int countBefore = backtestingDashboardView.RowCount;
            var popUpMenu = new PopupMenuHelper();
            popUpMenu.BacktestingDashboardPopupMenuItem(application,
                REMOVE_STRATEGY_POPUP, row);
            Waiter.WaitingCondition condition = () =>
                   backtestingDashboardView.RowCount < countBefore;
            if (!Waiter.Wait(condition, 20))
            {
                TestContext.Out.WriteLine("\r\nNot delete strategy - " + 
                    nameStrategy);
                return false;
            }
            else
            {
                TestContext.Out.WriteLine("\r\nDelete strategy - " + 
                    nameStrategy);
                return true;
            }
        }

        public int SearchStrategyWaitEmergance(string nameStrategy)
        {
            int row = -1;
            Waiter.WaitingCondition condition = () =>
            (row = FindStrategyName(nameStrategy)) != -1;
            if (!Waiter.Wait(condition))
            {
                throw new Exception("Not found strategy ");
            }

            WaitDownloadStrategies(row);

            return row;
        }

        public int FindStrategyName(string nameStrategy)
        {
            for (int i = 0; i < backtestingDashboardView.RowCount; i++)
            {
                if (backtestingDashboardView.CellValue(i, 
                    COLUMN_STRATEGY).Contains(
                    nameStrategy))
                {
                    return i;
                }
            }
            return -1;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
#if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    backtestingDashboardView.Dispose();
                }
                disposedValue = true;
            }
#endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}


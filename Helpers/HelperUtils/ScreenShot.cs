﻿using System.Drawing;
using System.Drawing.Imaging; 
using System.Windows.Forms;

namespace TSAutotests
{
    public class ScreenShot
    {
        public Bitmap GetScreenShot(Screen currentScreen)
        {
            Bitmap bmpScreenShot = new Bitmap(currentScreen.Bounds.Width, currentScreen.Bounds.Height, PixelFormat.Format32bppArgb);
            Graphics gScreenShot = Graphics.FromImage(bmpScreenShot);
            gScreenShot.CopyFromScreen(currentScreen.Bounds.X, currentScreen.Bounds.Y, 0, 0, currentScreen.Bounds.Size, CopyPixelOperation.SourceCopy);

            return bmpScreenShot;
        }

        public void SaveScreenShot(string file, ImageFormat ifo)
        {
            try { GetScreenShot(GetPrimaryScreen()).Save(file, ifo); }
            catch { }
        }

        public static Screen GetPrimaryScreen() { return Screen.PrimaryScreen; }
    }
}
﻿using System;

namespace TSAutotests
{
    class TimeoutMonitor
    {
        private DateTime endTime;

        public TimeoutMonitor(int timeout = 10)
        {
            endTime = DateTime.Now.AddSeconds(timeout);
        }

        public bool CheckTimeout()
        {
            return (endTime < DateTime.Now);
        }
    }
}

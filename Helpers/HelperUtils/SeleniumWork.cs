﻿//using gehtsoft.applicationcontroller;
//using gehtsoft.tscontroller;
//using NUnit.Framework;
//using OpenQA.Selenium;
//using OpenQA.Selenium.Support.UI;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Threading;

//namespace TSAutotests
//{
//    class SeleniumWork
//    {
//        private IWebDriver Browser;

//        private const string XPATH_STRATEGY_NAME =
//            "//tr[.//td[text()='Strategy']]//td[2]";
//        private const string XPATH_INSTRUMENT =
//            "//tr[.//td[text()='Instrument']]//td[2]";
//        private const string XPATH_TOTAL_PROFIT_LOSS =
//            "//tr[.//td[text()='Total profit/loss']]//td[2]";
//        private const string XPATH_TOTAL_PROFIT_OF_ALL_TRADES =
//            "//tr[.//td[text()='Total profit of all trades']]//td[2]";
//        private const string XPATH_TOTAL_LOSS_OF_ALL_TRADES =
//           "//tr[.//td[text()='Total loss of all trades']]//td[2]";
//        private const string XPATH_FINAL_BALANCE =
//            "//tr[.//td[text()='Final balance']]//td[2]";
//        private const string XPATH_FINAL_EQUITY =
//            "//tr[.//td[text()='Final equity']]//td[2]";
//        private const string XPATH_TOTAL_AMOUNT_TRADED =
//            "//tr[.//td[text()='Total amount traded']]//td[2]";
//        private const string XPATH_MAXIMAL_PROFIT_PER_TRADE =
//            "//tr[.//td[text()='Maximal profit per trade']]//td[2]";
//        private const string XPATH_LONGEST_PROFITABLE_TRADES_SEQUENCE =
//            "//tr[.//td[text()='Longest profitable trades sequence']]//td[2]";
//        private const string XPATH_MOST_PROFITABLE_TRADES_SEQUENCE =
//            "//tr[.//td[text()='Most profitable trades sequence']]//td[2]";
//        private const string XPATH_LEAST_PROFITABLE_TRADES_SEQUENCE =
//            "//tr[.//td[text()='Least profitable trades sequence']]//td[2]";

//        Dictionary<string, string> returnedValue =
//            new Dictionary<string, string>();

//        public SeleniumWork()
//        {
//            Browser = new OpenQA.Selenium.Chrome.ChromeDriver();
//            Browser.Manage().Window.Maximize();
//        }

//        public Dictionary<string, string> ReturnedValue(string
//            nameCloudStrategies)
//        {
//            Browser.Navigate().GoToUrl("http://fxcodebase.com/btview/#/view/"
//               + nameCloudStrategies);
//            FunctionWait();
//            Thread.Sleep(2000); //TODO

//            string strategyName = Browser.FindElement(By.XPath(
//                XPATH_STRATEGY_NAME)).Text;
//            returnedValue.Add("Strategy", strategyName);

//            string instrument = Browser.FindElement(By.XPath(
//                XPATH_INSTRUMENT)).Text;
//            returnedValue.Add("Instrument", instrument);

//            string totalProfitLoss = Browser.FindElement(By.XPath(
//                XPATH_TOTAL_PROFIT_LOSS)).Text;
//            returnedValue.Add("Total profit/loss", totalProfitLoss);

//            string totalProfitLossOfTrades = Browser.FindElement(By.XPath(
//                XPATH_TOTAL_LOSS_OF_ALL_TRADES)).Text;
//            returnedValue.Add("Total profit of all trades",
//                totalProfitLossOfTrades);

//            string totalLossOfAllTrades = Browser.FindElement(By.XPath(
//                XPATH_TOTAL_LOSS_OF_ALL_TRADES)).Text;
//            returnedValue.Add("Total loss of all trades", totalLossOfAllTrades);

//            string finalBalance = Browser.FindElement(By.XPath(
//                XPATH_FINAL_BALANCE)).Text;
//            returnedValue.Add("Final balance", finalBalance);

//            string fianlEquity = Browser.FindElement(By.XPath(
//                XPATH_FINAL_EQUITY)).Text;
//            returnedValue.Add("Final equity", fianlEquity);


//            string totalAmountTrades = Browser.FindElement(By.XPath(
//                XPATH_TOTAL_AMOUNT_TRADED)).Text;
//            returnedValue.Add("Total amount traded", totalAmountTrades);

//            string maximalProfitPerTrade = Browser.FindElement(By.XPath(
//                XPATH_MAXIMAL_PROFIT_PER_TRADE)).Text;
//            returnedValue.Add("Maximal profit per trade", maximalProfitPerTrade);

//            string longestProfitableSequnce= Browser.FindElement(By.XPath(
//                XPATH_LONGEST_PROFITABLE_TRADES_SEQUENCE)).Text;
//            returnedValue.Add("Longest profitable trades", longestProfitableSequnce);

//            string mostProfitableSequnce = Browser.FindElement(By.XPath(
//                XPATH_MOST_PROFITABLE_TRADES_SEQUENCE)).Text;
//            returnedValue.Add("Most profitable trades", mostProfitableSequnce);

//            string leastProfitableSequnce = Browser.FindElement(By.XPath(
//                XPATH_LEAST_PROFITABLE_TRADES_SEQUENCE)).Text;
//            returnedValue.Add("Least profitable trades", leastProfitableSequnce);

//            Browser.Close();
//            return returnedValue;
//        }

//        private void FunctionWait()
//        {
//            WebDriverWait wait = new WebDriverWait(Browser, new TimeSpan(0, 0, 10));
//            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.ClassName("ng-binding")));
//        }
//    }
//}

﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using NUnit.Framework;
using System;
using System.Threading;

namespace TSAutotests
{
    static class Waiter
    {
        public delegate bool WaitingCondition();
        public delegate IDisposable DisposableWaitCondition();

        public static bool Wait(WaitingCondition condition, int timeout = 20)
        {
            var monitor = new TimeoutMonitor(timeout);
            while (!monitor.CheckTimeout())
            {
                if (condition())
                {
                    return true;
                }
                Thread.Sleep(100); 
            }
            return false;
        }

        public static bool DisposableWait(DisposableWaitCondition 
            disposeCondition, int timeout = 20)
        {
            WaitingCondition condition = () => DisposeConditionExecutor(
                disposeCondition);
            return Wait(condition, timeout);
        }

        public static Dialog WaitDialogAppear(TradingStationWindow app, string title, 
            int timeout = 20)
        {
            DisposableWaitCondition disposableCondition = () => app.GetDialog(
                title);
            if (DisposableWait(disposableCondition, timeout))
            {
                TestContext.Out.WriteLine("\r\nDialog - " + title + " appear");
                return app.GetDialog(title);
            }
            else
            {
                return null;
            }
        }

        private static bool DisposeConditionExecutor(DisposableWaitCondition
            condition)
        {
            using (IDisposable disposable = condition())
            {
                if (disposable != null)
                {
                    return true;
                }
            }
            return false;
        }

    }
}

﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading;

namespace TSAutotests
{
    class CollectorLog
    {
        private const string PATH_FOLDER_CRASH = @"C:\Program Files (x86)\Candleworks\FXTS2\crash";
        private const string PATH_FOLDER_LOG = @"C:\Program Files (x86)\Candleworks\FXTS2\log";
        private const string PATH_TO_STORE_ERROR = @".\Logs";

        public void ArchivingLogsFail()
        {
            if (!Directory.Exists(PATH_TO_STORE_ERROR))
            {
                Directory.CreateDirectory(PATH_TO_STORE_ERROR);
            }

            ZipFile.CreateFromDirectory(PATH_FOLDER_LOG, "LogsTS.zip");
            Thread.Sleep(1000); //time on arhiving
        }
    }
}

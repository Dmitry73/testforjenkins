﻿using System;

namespace TSAutotests
{
    static class MathHelper
    {
        public static int SignsBeforeComma(double rate)
        {
            int i = 0;
            while (rate > 0.3)  //HACK There is no correlation of currencies below number "0.3"
            {
                rate = rate / 10;
                i++;
            }
            return i;
        }

        public static int RoundOnAfterSign(double rate)
        {
            int roundBefore = 6 - SignsBeforeComma(rate);
            return roundBefore;
        }

        public static double RoundingDependingOnSignsAfterComma(double rate)
        {
            rate = Math.Round(rate, 6 - SignsBeforeComma(rate));
            return rate;
        }

        public static double MinimumBorderForComparingDouble (double rateForDeterminant)
        {
            int signBeforeComma = 1;

            for (int i = SignsBeforeComma(rateForDeterminant); i > 0; i--)
            {
                signBeforeComma = signBeforeComma * 10;
            }
            double resultForCompare = 0.000001 * signBeforeComma;
            return resultForCompare;
        }
    }
}

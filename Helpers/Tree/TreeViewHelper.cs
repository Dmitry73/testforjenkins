﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;

namespace TSAutotests
{
    class TreeViewHelper : IDisposable
    {
        private const string MY_WATCHLISTS_BRANCH_BORDER = "My Watchlists";
        private TreeView treeView = null;

        public TreeViewHelper(Window window, string titleRegex)
        {
            Waiter.WaitingCondition condition = () => window.FindChild("SysTreeView32", titleRegex, true) != null;
            if (!Waiter.Wait(condition, 20))
                throw new Exception("Can't find tree");
            treeView = window.FindChild("SysTreeView32", titleRegex, true) as TreeView;
            if (treeView == null)
                throw new Exception("Can't find tree");
        }

        public TreeViewItem FindElement(string title)
        {
            TreeViewItem item = treeView.RootItem;
            return Scan(item, title);
        }


        public bool CompareElementTree(string compareValue)
        {
            TreeViewItem item = treeView.RootItem;
            bool checkFunction = true;
            CompareItemInTreeAll(item, compareValue, out checkFunction);
            return checkFunction;
        }

        public bool NegativSearchSymbolList()
        {
            TreeViewItem item = treeView.RootItem;
            if (item.Text.Equals(MY_WATCHLISTS_BRANCH_BORDER))
            {
                return true;
            }
            return false;
        }

        private TreeViewItem CompareItemInTreeAll( TreeViewItem item, string compareValue, out bool checkFunction)
        {
            checkFunction = true;
            while (item != null)
            {
                if (item.Text.Equals(MY_WATCHLISTS_BRANCH_BORDER))
                {
                    break;
                }

                if ((item.Text.Length == 7) && (item.Text.IndexOf('/') == 3))
                {
                    if (!item.Text.Contains(compareValue))
                    {
                        checkFunction = false;
                        throw new Exception("The currency pair does not satisfy the search conditions");
                    }
                }

                TreeViewItem found = CompareItemInTreeAll(item.FirstChild, compareValue, out checkFunction);
                if (found != null)
                    return found;
                item = item.Next;
            }
            return null;
        }


        private TreeViewItem Scan(TreeViewItem item, string title)
        {
            while (item != null)
            {
                if (item.Text == title)
                    return item;
                TreeViewItem found = Scan(item.FirstChild, title);
                if (found != null)
                    return found;
                item = item.Next;
            }
            return null;
        }

        #region IDisposable 
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            #if !NO_DISPOSE
            if (!disposedValue)
            {
                if (disposing)
                {
                    treeView.Dispose();
                }
                disposedValue = true;
            }
            #endif
        }

        public void Dispose()
        {
            Dispose(true);
        }
#endregion
    }
}

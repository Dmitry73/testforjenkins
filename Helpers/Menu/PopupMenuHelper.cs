﻿using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System;
using NUnit.Framework;
using System.Threading;

namespace TSAutotests
{
    class PopupMenuHelper : MenuHelper
    {
        private const string CREATE_CONTINGENT_ORDER_POPUP_MENU =
            "Create Contingent Order...";
        private const string CREATE_MARKET_ORDER_POPUP_MENU = "Create Market Order...";

        private const string OPEN_POSITION_TAB = "Open Position";
        private const string SUMMARY_TAB = "Summary";
        private const string SIMPLE_DEALING_RATES_TAB = "Simple Dealing Rates";
        private const string ORDERS_TAB = "Orders";

        public void OpenPositionPopUpMenuItem(TradingStationWindow application,
            string titleMenu, int row, int column = 1)
        {
            TradingStationTab tabOpenPositionClick = application.FindTab(
                OPEN_POSITION_TAB);
            tabOpenPositionClick.Click();

            using (TradingStationSimpleView openPositionView =
                application.FindGridView(TradingStationViewType.Trades))
            {
                openPositionView.Grid.ClickCell(row, column, 
                    InputEventModifiers.RightButton);
            }
            MenuItem(application, titleMenu);
        }



        public void SummaryPopupMenuItem(TradingStationWindow application,
            string titleMenu, int row, int column = 1)
        {
            TradingStationTab tabClickSummary = application.FindTab(SUMMARY_TAB);
            tabClickSummary.Click();

            using (TradingStationSimpleView SummaryView =
                application.FindGridView(TradingStationViewType.Summary))
            {
                SummaryView.Grid.ClickCell(row, column, InputEventModifiers.RightButton);
            }
            MenuItem(application, titleMenu);
        }

        public void SimpleDealingRatesPopupMenuItem(TradingStationWindow 
            application, string titleMenu, int row, int column = 1)
        {
            TradingStationTab simpleDealingRatesTabClick;
            using (TradingStationSimpleView SimpleDealingRatesView =
                application.FindGridView(TradingStationViewType.Rates))
            {
                simpleDealingRatesTabClick = application.FindTab(
                    SIMPLE_DEALING_RATES_TAB);
                simpleDealingRatesTabClick.Click();
                SimpleDealingRatesView.Grid.ClickCell(row, column, 
                    InputEventModifiers.RightButton);
            }
            MenuItem(application, titleMenu);
        }

        public void StoredStrategyPopupMenuItem(TradingStationWindow
            application, string titleMenu, int row, int column = 1)
        {
            using (var storedStrategiesTab = new StoredStrategiesTab(application))
            {
                while (application.GetPopupWindow() == null)
                {
                    storedStrategiesTab.storedStrategiesView.ClickCell(row, column,
                        InputEventModifiers.RightButton);
                    Thread.Sleep(500);
                }

            }
            MenuItem(application, titleMenu);
        }

        public void BacktestingDashboardPopupMenuItem(TradingStationWindow
                application, string titleMenu, int row, int column = 1)
        {
            using (var backtestingDashboard = new BacktestingDashboardTab
                (application))
            {
                while (application.GetPopupWindow() == null)
                {
                    backtestingDashboard.backtestingDashboardView.ClickCell(row,
                    column, InputEventModifiers.RightButton);
                }
            }
            MenuItem(application, titleMenu);
        }

        public void OrdersPopupMenuItem(TradingStationWindow application,
            string titleMenu, OrderGroups orderType, string symbol)
        {
            TradingStationTab ordersTabClick;
            using (TradingStationSimpleView OrdersView =
                application.FindGridView(TradingStationViewType.Orders))
            {
                ordersTabClick = application.FindTab(ORDERS_TAB);
                ordersTabClick.Click();
                using (var rightClick = new OrdersTab(application))
                {
                    rightClick.PressRightButtonOnCell(orderType, symbol);
                }
                MenuItem(application, titleMenu);
            }
        }

        public void OrdersPopupMenuItemSymbolAndBuySell(TradingStationWindow
            application, string titleMenu, OrderGroups orderType, string symbol,
            BuySellEnum buySell, bool takeAccountIfThen = false)
        {
            TradingStationTab ordersTabClick;
            using (TradingStationSimpleView OrdersView =
                application.FindGridView(TradingStationViewType.Orders))
            {
                ordersTabClick = application.FindTab(ORDERS_TAB);
                ordersTabClick.Click();
                using (var rightClick = new OrdersTab(application))
                {
                    rightClick.PressRightButtonOnCellBySymbolAndBuySell(
                        orderType, symbol, buySell, takeAccountIfThen);
                }
                MenuItem(application, titleMenu);
            }
        }


        private void MenuItem(TradingStationWindow application, string titleMenu)
        {
            Window window = null;
            Waiter.WaitingCondition dialogExistsCondition = () =>
            (window = application.GetPopupWindow()) != null;
            using (window)
            {
                if (!Waiter.Wait(dialogExistsCondition))
                {
                    throw new Exception("Not found Popup windows");
                }

                WindowMenu popupMenu = window.GetMenu();

                WindowMenuItem menuItem = findSubMenuItemByName(popupMenu, 
                    titleMenu);
                if (menuItem != null)
                {
                    application.SendCommand(menuItem.Command);
                    TestContext.Out.WriteLine("\r\nPress items Popup menu - " + 
                        titleMenu);
                }
                window.SendClose();
            }
        }
    }
}

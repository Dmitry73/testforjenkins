﻿using System;
using gehtsoft.applicationcontroller;
using gehtsoft.tscontroller;
using System.Collections.Generic;
using NUnit.Framework;
using System.Threading;

namespace TSAutotests
{
    class ApplicationMenuHelper : MenuHelper
    {
        private const int DEFAULT_LAYOUT_COMMAND = 0x0000b501;
        private const int TRADING_SETTING_COMMAND = 0x0000b675;
        private const int BACKTEST_STRATEGY_COMMAND = 0x0000b82d;
        private const int NEW_SIMULATION_MODE_COMMAND = 0x0000a80f;
        private const int CREATE_CONTINGENT_ORDER_COMMAND = 0x0000d712;
        private const int CREATE_MARKET_ORDER_COMMAND = 0x0000abee;
        private const int CREATE_ENTRY_ORDER_COMMAND = 0x0000abed;
        private const int EXIT_COMMAND = 0x00009c56;
        private const int LOGIN_COMMAND = 0x0000a411;
        private const int CLOSE_ALL_FOR_SYMBOL_COMMAND = 0x0000c129;
        private const int CLOSE_POSITIONS_COMMAND = 0x0000c04e;
        private const int CLOSE_ALL_POSITIONS_COMMAND = 0x0000b71a;
        private const int CLOSE_TO_BUY_COMMAND = 0x0000b719;
        private const int REMOVE_ORDER_COMMAND = 0x0000abf6;
        private const int CLOSE_TO_SELL_COMMAND = 0x0000b718;
        private const int MANAGE_SYMBOL_SYBSRIPTION_COMMAND = 0x0000abef;
        private const int CLOSE_POSITION_COMMAND = 0x0000abfc;
        private const int LOGOUT_COMMAND = 0x0000a412;

        public ApplicationMenuHelper()
        {

        }

        private void PressMenuItem(TradingStationWindow application, 
            List<string> address, bool checkable = false,
            bool setChecked = true)
        {
            WindowMenu menu = application.GetMenu();
            WindowMenuItem item = null;
            string lineForOut ="";
            foreach (var itemTitle in address)
            {
                if (item == null)
                {
                    item = findSubMenuItemByName(menu, itemTitle);
                }
                else
                {
                    item = findSubMenuItemByName(item.Submenu, itemTitle);
                }
                lineForOut = lineForOut + "=>" + itemTitle;
            }
            if (item == null)
            {
                throw new Exception("Wrong menu address");
            }
            int itemCommand = item.Command;
            if (!checkable)
            {
                menu.Window.SendCommand(itemCommand);
            }
            else
            {
                if (!item.Checked == setChecked)
                {
                    menu.Window.SendCommand(itemCommand);
                }
            }
            lineForOut = lineForOut.Trim(new Char[] { '=', '>' });
            TestContext.Out.WriteLine("\r\nPress items menu - " + lineForOut);
        }

        #region use command menu
        public void PressLogin(TradingStationWindow application)
        {
            application.SendCommand(LOGIN_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Login...");
        }

        public void PressSystemLogout(TradingStationWindow application)
        {
            application.SendCommand(LOGOUT_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Logout");
        }

        public void PressRemoveOrder(TradingStationWindow application)
        {
            application.SendCommand(REMOVE_ORDER_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Remove Order");
        }

        public void PressOpenCreateContingentOrder(TradingStationWindow application)
        {
            application.SendCommand(CREATE_CONTINGENT_ORDER_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Create Contingent Order...");
        }

        public void PressViewDefault(TradingStationWindow application)
        {
            application.SendCommand(DEFAULT_LAYOUT_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Default Layout");
        }

        public void PressSystemExit(TradingStationWindow application)
        {
            application.SendCommand(EXIT_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Exit");
        }

        public void PressTradingOrdersCreateEntryOrder(TradingStationWindow 
            application)
        {
            application.SendCommand(CREATE_ENTRY_ORDER_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Create Entry Order...");
        }

        public void PressTradingDealingRatesMarketOrder(TradingStationWindow 
            application)
        {
            application.SendCommand(CREATE_MARKET_ORDER_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Create Market Order..");
        }

        public void PressTradingSummaryCloseToSell(TradingStationWindow
            application)
        {
            application.SendCommand(CLOSE_TO_SELL_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Close to Sell...");
        }

        public void PressTradingSummaryCloseToBuy(TradingStationWindow 
            application)
        {
            application.SendCommand(CLOSE_TO_BUY_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Close to Buy...");
        }

        public void PressTradingSummaryCloseToAllPositions(TradingStationWindow 
            application)
        {
            application.SendCommand(CLOSE_ALL_POSITIONS_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Close All Positions...");

        }

        public void PressOpenPositionCloseAllPositions(TradingStationWindow 
            application)
        {
            application.SendCommand(CLOSE_ALL_POSITIONS_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Close Positions...");

        }

        public void PressOpenPositionClosePosition(TradingStationWindow 
            application)
        {
            application.SendCommand(CLOSE_POSITION_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Close Position...");
        }

        public void PressOpenPositionCloseAllforSymbol(TradingStationWindow 
            application)
        {
            application.SendCommand(CLOSE_ALL_FOR_SYMBOL_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Close All for Symbol...");
            
        }

        public void PressAlertAndTradingBacktestStrategy(TradingStationWindow 
            application)
        {
            application.SendCommand(BACKTEST_STRATEGY_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Backtest Strategy...");
        }

        public void PressSystemSimulationModeNew(TradingStationWindow application)
        {
            application.SendCommand(NEW_SIMULATION_MODE_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => New Simulation Mode");
        }

        public void PressTradingDealingRatesSettings(TradingStationWindow 
            application)
        {
            application.SendCommand(TRADING_SETTING_COMMAND);
            TestContext.Out.WriteLine("\r\nPress item menu => Trading Settings...");
        }

        public void PressTradingDealingRatesManageSymbol(TradingStationWindow
            application)
        {
            application.SendCommand(MANAGE_SYMBOL_SYBSRIPTION_COMMAND);
            TestContext.Out.WriteLine(
                "\r\nPress item menu => Manage Symbol Subscription...");
        }
        #endregion

        #region not use
        public void PressTradingAccountsReports(TradingStationWindow application)
        {
            PressMenuItem(application, 
                new List<string> { "Trading", "Accounts", "Reports..." });
        }

        public void PressSystemOptions(TradingStationWindow application)
        {
            PressMenuItem(application, 
                new List<string> { "System", "Options..." });
        }
        #endregion

        #region checked menu
        public void PressViewSymbolList(TradingStationWindow application, 
            bool setChecked = true)
        {
            PressMenuItem(application,
                new List<string> { "View", "Symbol List" }, true, setChecked);
        }

        public void PressViewOpenPositions(TradingStationWindow application,
            bool setChecked = true)
        {
            PressMenuItem(application, 
                new List<string> { "View", "Open Positions" }, true, setChecked);
        }

        public void PressViewAccounts(TradingStationWindow application,
            bool setChecked = true)
        {
            PressMenuItem(application, new List<string> { "View", "Accounts" }, 
                true, setChecked);
        }

        public void PressViewOrders(TradingStationWindow application,
            bool setChecked = true)
        {
            PressMenuItem(application, new List<string> { "View", "Orders" },
                true, setChecked);
        }

        public void PressViewSummary(TradingStationWindow application,
            bool setChecked = true)
        {
            PressMenuItem(application, new List<string> { "View", "Summary" },
                true, setChecked);
        }

        public void PressAlertAndTradingStrategiesCloudStrategies(
            TradingStationWindow application, bool setChecked = true)
        {
            PressMenuItem(application,
                new List<string> {"Alerts and Trading Automation",
                "Strategies Cloud", "Strategies" });
        }
        #endregion
    }
}
